<?php

// LC Admin Home
Breadcrumbs::for('lc-admin', function ($trail) {
    $trail->push('Home', route('lc-admin.dashboard'));
});

// LC Admin Activity Log
Breadcrumbs::for('lc-admin.activity-log', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Activity log', route('lc-admin.activity-log'));
});

// LC Admin Home > Users
Breadcrumbs::for('lc-admin.users', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Users', route('lc-admin.users.index'));
});

// LC Admin Home > Users > Create
Breadcrumbs::for('lc-admin.users.create', function ($trail) {
    $trail->parent('lc-admin.users');
    $trail->push('Create', route('lc-admin.users.create'));
});

// LC Admin Home > Users > Edit
Breadcrumbs::for('lc-admin.users.edit', function ($trail, $user) {
    $trail->parent('lc-admin.users');
    $trail->push($user->email, route('lc-admin.users.edit', $user->id));
});

// LC Admin Roles
Breadcrumbs::for('lc-admin.roles', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Roles', route('lc-admin.roles.index'));
});

// LC Admin Permissions
Breadcrumbs::for('lc-admin.permissions', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Permissions', route('lc-admin.permissions.index'));
});

// Home > Blog
Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push('Blog', route('blog'));
});

// Home > Blog > Create
Breadcrumbs::for('lc-admin.blogs.create', function ($trail) {
    $trail->parent('lc-admin.blogs');
    $trail->push('Create', route('lc-admin.blogs.create'));
});

// Home > Groups
Breadcrumbs::for('lc-admin.groups', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Groups', route('lc-admin.groups.index'));
});

// Home > Groups > Create
Breadcrumbs::for('lc-admin.groups.create', function ($trail) {
    $trail->parent('lc-admin.groups');
    $trail->push('Create', route('lc-admin.groups.create'));
});

// Home > Groups > Edit
Breadcrumbs::for('lc-admin.groups.edit', function ($trail, $group) {
    $trail->parent('lc-admin.groups');
    $trail->push($group->name, route('lc-admin.groups.edit', $group->id));
});

// Home > Pharmacies
Breadcrumbs::for('lc-admin.pharmacies', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Pharmacies', route('lc-admin.pharmacies.index'));
});

// Home > Pharmacies > Create
Breadcrumbs::for('lc-admin.pharmacies.create', function ($trail) {
    $trail->parent('lc-admin.pharmacies');
    $trail->push('Create', route('lc-admin.pharmacies.create'));
});

// Home > Pharmacies > Edit
Breadcrumbs::for('lc-admin.pharmacies.edit', function ($trail, $pharmacy) {
    $trail->parent('lc-admin.pharmacies');
    $trail->push($pharmacy->name, route('lc-admin.pharmacies.edit', $pharmacy));
});

// Home > UserPharmacies > Create
Breadcrumbs::for('lc-admin.user-pharmacies.create', function ($trail) {
    $trail->parent('lc-admin.pharmacies');
    $trail->push('Add Pharmacy to User', route('lc-admin.user-pharmacies.create'));
});

// Home > Employees
Breadcrumbs::for('lc-admin.employees', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Employees', route('lc-admin.employees.index'));
});

// LC Admin Home > Employees > Create
Breadcrumbs::for('lc-admin.employees.create', function ($trail) {
    $trail->parent('lc-admin.employees');
    $trail->push('New Employee', route('lc-admin.employees.create'));
});

// Home > Employee > Edit
Breadcrumbs::for('lc-admin.employees.edit', function ($trail, $employee) {
    $trail->parent('lc-admin.employees');
    $trail->push($employee->first_name . ' ' . $employee->last_name, route('lc-admin.employees.edit', $employee->id));
});

// Home > Drugs
Breadcrumbs::for('lc-admin.drugs', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Products', route('lc-admin.drugs.index'));
});

// Home > Drugs > Create
Breadcrumbs::for('lc-admin.drugs.create', function ($trail) {
    $trail->parent('lc-admin.drugs');
    $trail->push('Create', route('lc-admin.drugs.create'));
});

// Home > Sliders
Breadcrumbs::for('lc-admin.sliders', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Sliders', route('lc-admin.sliders.index'));
});

// Home > Sliders > Create
Breadcrumbs::for('lc-admin.sliders.create', function ($trail) {
    $trail->parent('lc-admin.sliders');
    $trail->push('Create', route('lc-admin.sliders.create'));
});

// Home > Sliders > Edit
Breadcrumbs::for('lc-admin.sliders.edit', function ($trail, $slider) {
    $trail->parent('lc-admin.sliders');
    $trail->push('#ID ' . $slider->id, route('lc-admin.sliders.edit', $slider->id));
});

// Home > Blogs
Breadcrumbs::for('lc-admin.blogs', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Blogs', route('lc-admin.blogs.index'));
});

// Home > Job Board
Breadcrumbs::for('lc-admin.job-board', function ($trail) {
    $trail->parent('lc-admin');
    $trail->push('Job Board', route('lc-admin.job-board.index'));
});

// Home > Job Board > Create
Breadcrumbs::for('lc-admin.job-board.create', function ($trail) {
    $trail->parent('lc-admin.job-board');
    $trail->push('Create', route('lc-admin.job-board.create'));
});

// Home > Job Board > Edit
Breadcrumbs::for('lc-admin.job-board.edit', function ($trail, $jobBoard) {
    $trail->parent('lc-admin.job-board');
    $trail->push('#ID ' . $jobBoard->id, route('lc-admin.job-board.edit', $jobBoard->id));
});
