<?php

Auth::routes();
Route::get('/', 'PageController@index')->name('index');
Route::get('contact', 'PageController@contact')->name('contact');
Route::get('about-us', 'PageController@aboutUs')->name('about_us');
Route::get('advertising', 'PageController@advertising')->name('advertising');
Route::get('faq', 'PageController@faq')->name('faq');
Route::get('privacy', 'PageController@privacy')->name('privacy');

Route::get('blogs', 'PageController@blogs')->name('blogs');
Route::get('blog/{slug}', 'PageController@blogPost')->name('blog_post');

Route::get('apoteke', 'PageController@pharmacies')->name('pharmacies');
Route::get('a/{slug}', 'PageController@pharmacy')->name('pharmacy');

Route::get('proizvodi', 'PageController@products')->name('products');
Route::get('p/{slug}', 'PageController@product')->name('product');

Route::get('profil/{slug}', 'PageController@employee')->name('employee');
Route::get('search', 'PageController@search')->name('search');


Route::get('home', 'HomeController@index');

Route::get('lc-admin', 'Laracode\AdminController@index')->name('lc-admin');

Route::group(['as' => 'lc-admin.', 'prefix' => 'lc-admin', 'middleware' => ['auth', 'permission:admin panel']], function () {

    Route::resource('groups', 'GroupController')->middleware('permission:group read');

    Route::resource('pharmacies', 'PharmacyController')->middleware('permission:pharmacy read');

    Route::post('drugs/import', 'DrugController@import')->name('drugs.import');
    Route::resource('drugs', 'DrugController')->middleware('permission:drug read');
    Route::resource('drug-pharmacy', 'DrugPharmacyController')
        ->parameters(['drug-pharmacies' => 'drug'])
        ->middleware('permission:drug pharmacy read');

    Route::resource('employees', 'EmployeeController')->middleware('permission:employee read');

    Route::resource('job-board', 'JobBoardController')->middleware('permission:job board read');

    Route::resource('group-pharmacies', 'GroupPharmaciesController')->parameters([
        'group-pharmacies' => 'group'
    ])->except(['index', 'create', 'store']);

    Route::resource('user-pharmacies', 'UserPharmaciesController')->parameters([
        'user-pharmacies' => 'user'
    ]);

    Route::resource('sliders', 'SliderController')->middleware('role:super admin');

    Route::resource('blogs', 'BlogController')->middleware('permission:blog read');

    Route::post('upload/{path}', 'ImageController@store')->name('images.store');
});

Route::group(['as' => 'lc-admin.', 'prefix' => 'lc-admin', 'middleware' => ['auth', 'permission:admin panel'], 'namespace' => 'Laracode'], function () {

    Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::resource('roles', 'RoleController', ['except' => ['show', 'edit']]);
    Route::resource('permissions', 'PermissionController', ['except' => ['show', 'edit']]);

    Route::resource('users', 'UserController')->middleware('permission:super admin');

    Route::get('languages', 'LanguageController@index')->name('languages');
    Route::post('languages/{name}', 'LanguageController@store')->name('store-language');
    Route::get('set-language/{language}', 'LanguageController@update')->name('set-language');

    Route::get('activity-log', 'AdminController@activityLog')->name('activity-log');

    Route::post('newsletter', 'NewsletterController@store')->name('store-newsletter');
});
