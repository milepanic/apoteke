<?php

use App\BusinessHour;
use App\Pharmacy;
use Illuminate\Database\Seeder;

class PharmaciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Pharmacy::class, 50)->create()->each(function ($pharmacy) {
            $businessHours = factory(BusinessHour::class)->make();

            $pharmacy->businessHours()->save($businessHours);
        });
    }
}
