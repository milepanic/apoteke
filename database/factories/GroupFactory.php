<?php

use App\Group;
use Faker\Generator as Faker;

$factory->define(Group::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'logo' => $faker->word,
        'description' => $faker->paragraph,
    ];
});
