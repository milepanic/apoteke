<?php

use App\BusinessHour;
use Faker\Generator as Faker;

$factory->define(BusinessHour::class, function (Faker $faker) {
    return [
        'monday' => ['08:00', '16:00'],
        'tuesday' => ['08:00', '16:00'],
        'wednesday' => ['08:00', '16:00'],
        'thursday' => ['08:00', '16:00'],
        'friday' => ['08:00', '16:00'],
        'saturday' => ['08:00', '16:00'],
    ];
});
