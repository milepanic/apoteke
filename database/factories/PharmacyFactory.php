<?php

use App\Pharmacy;
use Faker\Generator as Faker;

$factory->define(Pharmacy::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'slug' => $faker->unique()->word,
        'address' => $faker->streetAddress,
        'visible' => $faker->boolean,
        'cover_image' => 'https://pharmacytimes.s3.amazonaws.com/v1_media/SafeImages/Webdam%20Assets/pharmacy1.jpg',
        'thumbnail' => 'https://image.freepik.com/free-vector/medical-pharmacy-logo_7888-26.jpg',
        'location_id' => rand(1, 113),
        'longitude' => $faker->longitude($min = -90, $max = 90),
        'latitude' => $faker->latitude(-90, 90),
    ];
});
