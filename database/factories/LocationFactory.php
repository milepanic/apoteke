<?php

use App\Location;
use Faker\Generator as Faker;

$factory->define(Location::class, function (Faker $faker) {
    return [
        'name' => $faker->cityPrefix,
        'longitude' => $faker->longitude($min = -90, $max = 90),
        'latitude' => $faker->latitude(-90, 90),
    ];
});
