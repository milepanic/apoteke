<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('published')->default(false);
            $table->string('types');
            $table->integer('priority')->default(0);
            $table->string('background')->nullable();
            $table->string('name')->nullable();
            $table->text('text')->nullable();
            $table->string('url')->nullable();
            $table->text('buttons')->nullable();
            $table->string('style_name')->nullable();
            $table->string('style_value')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
