<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugsPharmaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_pharmacy', function (Blueprint $table) {
            $table->unsignedInteger('drug_id');
            $table->unsignedInteger('pharmacy_id');
            $table->decimal('price', 6, 2);
            $table->boolean('sale')->default(false);
            $table->dateTime('promoted_until')->nullable();

            $table->foreign('drug_id')->references('id')->on('drugs');
            $table->foreign('pharmacy_id')->references('id')->on('pharmacies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs_pharmacies');
    }
}
