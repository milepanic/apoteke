<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePharmaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('emails')->nullable();
            $table->text('phone_numbers')->nullable();
            $table->text('fax_numbers')->nullable();
            // $table->text('contact');
            $table->text('about_us')->nullable();
            $table->string('web_page')->nullable();
            $table->string('social_pages')->nullable();
            $table->text('documents')->nullable();
            // $table->string('working_time');
            $table->boolean('visible');
            $table->string('cover_image');
            $table->string('thumbnail');
            $table->text('gallery')->nullable();
            $table->unsignedInteger('location_id');
            $table->string('address');
            $table->string('longitude');
            $table->string('latitude');
            $table->text('admin_report')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('location_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacies');
    }
}
