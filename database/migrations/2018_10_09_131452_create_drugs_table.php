<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('substance')->nullable();
            $table->string('name');
            $table->string('manufacturer')->nullable();
            $table->string('shape')->nullable();
            $table->text('dosage')->nullable();
            $table->text('package')->nullable();
            $table->boolean('issuing')->nullable()->default(false); // sa ili bez recepta
            $table->string('description')->nullable(); // pdf
            $table->string('instruction')->nullable(); // pdf
            $table->string('thumbnail')->nullable();
            $table->boolean('official')->default(false); // da li je rucno dodato od strane korisnika
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs');
    }
}
