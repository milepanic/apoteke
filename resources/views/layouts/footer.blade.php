
    <!--page footer-->
    <footer id="main-footer" class="site-footer clearfix">
        <div class="container">
            <div class="row">


                <!--about widget-->
                <div class="col-md-3 col-sm-6">
                    <section  class="widget animated fadeInLeft">
                        <h3 class="title">O nama</h3>
                        <div class="textwidget">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                            nisl ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </section>
                </div>


                <!--general services-->
                <div class="col-md-3 col-sm-6  ">
                    <section class="widget animated fadeInLeft ae-animation-fadeInLeft">
                        <h3 class="title">Partneri</h3>
                        <ul>
                            <li>
                                <a href="#">Lorem ipsum</a>
                            </li>
                            <li>
                                <a href="#">Lorem ipsum</a>
                            </li>
                            <li>
                                <a href="#">Lorem ipsum</a>
                            </li>
                            <li>
                                <a href="#">Lorem ipsum</a>
                            </li>
                            <li>
                                <a href="#">Lorem ipsum</a>
                            </li>
                        </ul>
                    </section>
                </div>

                <div class="clearfix visible-sm"></div>

                <!--recent posts-->
                <div class="col-md-3 col-sm-6  ">
                    <section  class="widget animated fadeInLeft">
                        <h3 class="title">Stranice</h3>                     
                        <ul id="twitter_update_list">                         
                            <li>
                                <a href="#">Uslovi korištenja</a>
                            </li>
                            <li>
                                <a href="{{ route('privacy') }}">Privatnost</a>
                            </li>
                            <li>
                                <a href="{{ route('faq') }}">Često postavljana pitanja</a>
                            </li>               
                        </ul>
                    </section>
                </div>
                <!--subscription form-->
                <div class="col-md-3 col-sm-6  ">
                    <section  class="widget animated fadeInLeft">
                        <h3 class="title">Subscribe!</h3>
                        <div class="textwidget">Subscribe to my blog for updates</div>
                        <div>

                            <form name="subs-form" id="subs_form" class="subs-form" method="post" action="subscription-handler.php">


                                <label for="subs-name">Name:</label>
                                <input name="name" id="subs-name" class="required" type="text" title="* Please provide your name" /><br/>

                                <label for="subs-email">Email:</label>
                                <input name="email" class="required email" id="subs-email" type="text" title="* Please provide a valid email address" /><br/>

                                <input type="hidden" name="action" value="send_message">
                                <input type="hidden" name="nonce" value="">

                                <input id="subs-form-submit" name="Submit" type="submit" value="SUBMIT" />
                                <img src="{{ asset('theme/apoteke/images/loader3.gif') }}" id="subs-loader" alt="Loading...">
                                <div id="subs-error-container"></div>
                                <div id="subs-response-container"></div>

                            </form>

                        </div>

                    </section>
                </div>
            </div>
            <div class="footer-bottom animated fadeInDown clearfix">
                <div class="row">
                    <div class="col-sm-7">
                        <p>&copy; Copyright 2018. All Rights Reserved by apoteke.ba</p>
                    </div>
                    <!--footer social icons-->
                    <div class="col-sm-5 clearfix">
                        <ul class="footer-social-nav">
                            <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_blank" href="#"><i class="fa fa-youtube"></i></a></li>
                            <li><a target="_blank" href="skype:skypeusername?add"><i class="fa fa-skype"></i></a></li>
                            <li><a target="_blank" href="#"><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>