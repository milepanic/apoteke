<header id="header">
    <div class="container">

        <!-- Website Logo -->
        <div class="logo clearfix">
            <a href="{{ route('index') }}">
                <img src="{{ asset('theme/apoteke/images/logo.png') }}" alt="Medicalpress">
            </a>
        </div>

        <!-- Main Navigation -->
        <nav class="main-menu">
            <ul class="header-nav clearfix" id="menu-main-menu">
            <li class="{{ strpos(Route::currentRouteName(), 'index') === 0 ? 'current-menu-item' : null }} page_item">
                <a href="{{ route('index') }}">Početna</a>
            </li>
            <li class="{{ strpos(Route::currentRouteName(), 'pharmacies') === 0 ? 'current-menu-item' : null }}">
                <a href="{{ route('pharmacies') }}">Lista apoteka</a>
                <ul>
                    <li>
                        <a href="doctors-four-columns.html">Bijeljina</a>
                    </li>
                    <li>
                        <a href="doctors-three-columns.html">Banja Luka</a>
                    </li>
                    <li>
                        <a href="doctors-two-columns.html">Sarajevo</a>
                    </li>
                </ul>
            </li>
            <li class="{{ strpos(Route::currentRouteName(), 'products') === 0 ? 'current-menu-item' : null }}">
                <a href="{{ route('products') }}">Lijekovi i proizvodi</a>
            </li>

            <li class="{{ strpos(Route::currentRouteName(), 'blogs') === 0 ? 'current-menu-item' : null }}">
                <a href="{{ route('blogs') }}">Blog</a>
                <ul>
                    <li>
                        <a href="#">Zdravlje</a>
                    </li>
                    <li>
                        <a href="#">Prehrana</a>
                    </li>
                    <li>
                        <a href="#">Ljepota</a>
                    </li>
                    <li>
                        <a href="#">Mame i bebe</a>
                    </li>
                    <li>
                        <a href="#">Zivotni stil</a>
                    </li>
                </ul>
            </li>
            <li class="{{ strpos(Route::currentRouteName(), 'advertising') === 0 ? 'current-menu-item' : null }}">
                <a href="{{ route('advertising') }}">Oglašavanje</a>
            </li>
            <li class="{{ strpos(Route::currentRouteName(), 'about_us') === 0 ? 'current-menu-item' : null }}">
                <a href="{{ route('about_us') }}">O nama</a>
            </li>
            <li class="{{ strpos(Route::currentRouteName(), 'contact') === 0 ? 'current-menu-item' : null }}">
                <a href="{{ route('contact') }}">Kontakt</a>
            </li>
        </ul>
    </nav>
    <div id="responsive-menu-container"></div>
</div>
</header>