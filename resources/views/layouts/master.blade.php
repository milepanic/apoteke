<!DOCTYPE html>
<html lang="en-US"><!--<![endif]-->
<head>
    <!-- META TAGS -->
    <meta charset="UTF-8">

    <!-- Title -->
    <title>@yield('title', 'APOTEKE.BA')</title>

    <!-- Define a view port to mobile devices to use - telling the browser to assume that
    the page is as wide as the device (width=device-width)
    and setting the initial page zoom level to be 1 (initial-scale=1.0) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>

    <!-- favicon -->
    <link rel="shortcut icon" href="images/favicon-new.png">

    <!-- Google Web Font -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,100,500,600,700,800,900,300,200" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- bootstrap Style Sheet (caution ! - Do not edit this stylesheet) -->
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/bootstrap.css') }}" type="text/css" media="all">
    <!-- Flexslider stylesheet -->
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/flexslider.css') }}" type="text/css" media="all">
    <!-- Animations stylesheet -->
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/animations.css') }}" type="text/css" media="all">
    <!-- Awesome Font stylesheet -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/font-awesome.css') }}" type="text/css" media="all">
    <!-- Datepciker stylesheet -->
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/datepicker.css') }}" type="text/css" media="all">
    <!-- Swipebox stylesheet -->
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/swipebox.css') }}" type="text/css" media="all">
    <!-- meanmenu stylesheet -->
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/meanmenu.css') }}" type="text/css" media="all">
    <!-- Include the site main stylesheet -->
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/main.css') }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/style.css') }}" type="text/css" media="all">
    <!-- Include the site responsive  stylesheet -->
    <link rel="stylesheet" href="{{ asset('theme/apoteke/css/custom-responsive.css') }}" type="text/css" media="all">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    @yield('styles')
</head>
<body>

<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="<php echo 'http://www.google.com/chromeframe/?redirect=true'; ?>">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->


@include('layouts.header')


@yield('content')


@include('layouts.footer')

    <a href="#top" id="scroll-top"></a>

    <script type='text/javascript' id='quick-js'></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery-2.2.3.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/bootstrap.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.flexslider-min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.swipebox.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.isotope.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.appear.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.ui.core.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.ui.datepicker.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.validate.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.form.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.autosize.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.meanmenu.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery.velocity.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery-twitterFetcher.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/respond.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/apoteke/js/custom.js') }}"></script>

    @yield('external-js')
    @yield('scripts')

</body>
</html>
