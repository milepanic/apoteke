@extends('layouts.master')
@section('title', 'Oglašavanje | APOTEKE.BA')

@section('content')

    <div class="banner clearfix"></div>
    
    <!--main part-->
    <div class="advertising-page">
        <div class="container">
            <h2 class="inner-title">PROMOTIVNI PAKETI</h2>
            
            <div class="intro-paragraph">
                <p>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla tempore nam totam at, 
                    ipsum delectus cumque vel nisi rerum, cupiditate eveniet quos vero ratione hic? 
                    Aspernatur minima officia odio dolorem?
                </p>
            </div>
            <div class="row ">
                <div class="col-xs-12 col-sm-6  col-md-4  ">
                    <article class="packet-item">
                        <header>
                            <h4>BASIC</h4>
                        </header>
                        <ul>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                        </ul>
                        <footer>
                            <a href="https://www.apoteke.me/mne/contact" class="readmore">Poručite</a>
                        </footer>
                    </article>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 ">
                    <article class="packet-item">
                        <header>
                            <h4>PREMIUM</h4>
                        </header>
                        <ul>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                        </ul>
                        <footer>
                            <a href="https://www.apoteke.me/mne/contact" class="readmore">Poručite</a>
                        </footer>
                    </article>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-0">
                    <article class="packet-item">
                        <header>
                            <h4>DE LUX</h4>
                        </header>
                        <ul>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                            <li>lorem ipsum</li>
                        </ul>
                        <footer>
                            <a href="https://www.apoteke.me/mne/contact" class="readmore">Poručite</a>
                        </footer>
                    </article>
                </div>
            </div>
            <div class="container pack-intro">
            <h3>Napomene</h3>
            <hr>
            <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                 Alias, ipsa recusandae error voluptate architecto maiores fuga
                  accusamus maxime corrupti quasi sed numquam voluptates. 
                Non ipsa accusamus dolor quae exercitationem voluptas.
            </p>
            <p>
                * Definisanje cijena za većinu usluga iz DE LUX dodataka se sprovodi na direktan upit klijenta s’obzirom da 
                ista zavisi od velikog broja faktora.
            </p>
        </div>
        </div> 
    </div>
   

@endsection