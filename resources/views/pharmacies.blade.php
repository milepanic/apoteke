@extends('layouts.master')
@section('title', 'Lista apoteka | APOTEKE.BA')

@section('content')
    <div class="banner clearfix"></div>
    <!-- main part -->
    <div class="container search-page">
        <div class="row">
            <!-- first left part -->
            <div class="col-md-3 col-sm-4 col-xs-5">
                <div class="row">
                    <div class="col-md-12 map">
                        <a href="#">
                            <img src="{{ asset('theme/apoteke/images/search/map-link.png') }}" alt="map-link">
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter-search">
                            <div>
                                <input type="submit" id="search-submit" value="">
                                <input type="text" value="" name="s" id="search-text" placeholder="Search">  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form class="panel panel-primary">
                            <h5 class="panel-heading">Gradovi:</h5>
                            <input type="checkbox" id="city1" name="city-1" value="city1">
                            <label for="city1">Bijeljina</label>
                            <input type="checkbox" id="City2" name="city-2" value="City2" >
                            <label for="City2">Banja Luka</label>
                            <input type="checkbox" id="City3" name="city-3" value="City3" >
                            <label for="City3">City3</label>
                            <input type="checkbox" id="city4" name="city-4" value="Strawberry">
                            <label for="city4">City4</label>
                            <input type="checkbox" id="city5" name="city-5" value="blackberry" >
                            <label for="city5">City5</label>
                            <input type="checkbox" id="city6" name="city-6" value="bbb">
                            <label for="city6">City6</label>
                            <div id="more" hidden>
                                <input type="checkbox" id="city7" name="city-7" value="7777" >
                                <label for="city7">City7</label>
                                <input type="checkbox" id="city8" name="city-8" value="88888">
                                <label for="city8">City8</label>
                            </div>
                            <span class="readmore" id="myBtn" style="display: block;">Prikaži sve</span>
                            <hr>
                            <h5 class="panel-heading">Apoteke:</h5>
                            <input type="checkbox" id="fruit9" name="fruit-9" value="Apot1">
                            <label for="fruit9">Rosic</label>
                            <input type="checkbox" id="apot2" name="apot-2" value="apot2" >
                            <label for="apot2">Apot2</label>
                            <input type="checkbox" id="fruit3" name="fruit-3" value="Cherry" >
                            <label for="fruit3">Apot3</label>
                            <input type="checkbox" id="fruit4" name="fruit-4" value="Strawberry">
                            <label for="fruit4">Apot4</label>
                            <input type="checkbox" id="fruit5" name="fruit-5" value="blackberry" >
                            <label for="fruit5">Apot5</label>
                            <input type="checkbox" id="fruit6" name="fruit-6" value="bbb">
                            <label for="fruit6">Apot6</label>
                            <div id="moreApot" hidden>
                                <input type="checkbox" id="fruit7" name="fruit-7" value="7777" >
                                <label for="fruit7">Apot7</label>
                                <input type="checkbox" id="fruit8" name="fruit-8" value="88888">
                                <label for="fruit8">Apot8</label>
                            </div>
                            <span class="readmore" id="apotMyBtn" style="display: block;">Prikaži sve</span>
                            <hr>
                            <div class="widget widget_categories">
                                <h5 class="title panel-heading">Kategorije</h5>
                                <ul class="ul-categories">
                                    <li><a href="#" title="View all posts filed under Environment">Zdravlje</a></li>
                                    <li><a href="#" title="View all posts filed under Health Basics">Prehrana</a></li>
                                    <li><a href="#" title="View all posts filed under Lifestyle">Ljepota</a></li>
                                    <li><a href="#" title="View all posts filed under Motivation">Mame i bebe</a></li>
                                </ul>
                            </div>
                            <div class="switch-box">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <span class="switch">Trenutno otvoreno</span>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="switch">
                                            <input type="checkbox" id="checkbox1">
                                            <label for="checkbox1"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-12">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <section class="widget ads-item-wrap">
                            <h5 class="title">Reklame</h5>
                            <div class="ads">
                                <a href="https://www.prirodnolecenje.com/sremus-kapi-ramslin/" title="sremus-kapi">
                                    <img src="{{ asset('theme/apoteke/images/blog/images.jpg') }}" alt="kapiSremus">
                                </a>    
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            
            <!-- firs right part -->
            <div class="col-md-9 col-sm-8 col-xs-7">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="list-header search-results-panel">
                            <h4>Pronadjeno je: {{ $pharmacies->total() }} apoteka </h4>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                Culpa cupiditate, nihil aliquam reprehenderit ex nulla.
                                Aperiam sit sint laudantium ad quo similique at,
                                omnis fuga consequuntur officiis
                            </p>
                        </div>
                    </div>
                </div>
                <!-- second right part -->
                <div class="row">
                	@foreach($pharmacies as $pharmacy)
                    <div class="col-md-12">
                        <div class="search-results discount" id="{{ $loop->first ? 'highlight' : '' }}">
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12 info-pic">
                                    <a href="{{ route('pharmacy', $pharmacy) }}"><img src="{{ file_path($pharmacy->thumbnail) }}" alt="lorempics" class="img-responsive med-inside" ></a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="apoteka-results">
                                        <h3>
                                            <a href="{{ route('pharmacy', $pharmacy) }}">{{ $pharmacy->name }}</a>
                                        </h3>
                                        <a href="">
                                        	<i class="fas fa-map-marker-alt"></i>
                                        	<p>{{ $pharmacy->location->name }}</p>
                                        </a>
                                        <i class="fas fa-briefcase-medical"></i>
                                        <p>kategorije...</p>
                                        <i class="far fa-clock"></i>
                                        <p>radno vrijeme:&nbsp</p>
                                        <p class="open">9:00 do 17:00 (Otvoreno)</p>
                                    </div>
                                </div>
                                @if(rand(0,1))
                                <div class="col-md-3 col-sm-6 col-xs-12 action-pic">
                                    <div class="sale">
                                        <div class="box">
                                            <div class="ribbon red"><span>Popust 10%</span></div>
                                            <a href="#">
                                                <img src="{{ asset('theme/apoteke/images/search/paracetamol.jpg') }}" alt="paracetamol500">
                                            </a>
                                        </div>
                                    </div>
                                </div>
								@elseif(rand(0,1))
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="services">
                                        <h4>Usluge:</h4>
                                        <ul>
                                            <li>Mjrenje krvnog pritiska </li>
                                            <li> Mjrenje šećera u krvi </li>
                                            <li>Savjetovanja</li>
                                        </ul>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                	@endforeach
                </div>
                <!-- pagination -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="pagination-wrap">
							{{ $pharmacies->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $("#myBtn").click(function(e){
                $(this).text(function (index, text) {
                    return (text == 'Prikaži manje' ? 'Prikaži sve' : 'Prikaži manje');
                });
                e.preventDefault();
                $("#more").toggle();
            });
        });
        
        $(document).ready(function(){
            $("#apotMyBtn").click(function(e){
                $(this).text(function (index, text) {
                    return (text == 'Prikaži manje' ? 'Prikaži sve' : 'Prikaži manje');
                });
                e.preventDefault();
                $("#moreApot").toggle();
            });
        });
    </script>
@endsection