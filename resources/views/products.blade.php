@extends('layouts.master')
@section('title', 'Lista proizvoda | APOTEKE.BA')

@section('content')
	<div class="banner clearfix"></div>
	<!-- main part -->
	<div class="container search-page products-page">
		<div class="row">
			<div class="col-md-12">
				<div class="filter-search">
					<div>
						<input type="submit" id="search-submit" value="">
						<input type="text" value="" name="s" id="search-text" placeholder="Search">  
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="products-list search-results-panel">
					<h4>Pronadjeno je: 6 apoteka </h4>
				</div>
			</div>
			<!-- first left part -->
			<div class="col-md-3 col-sm-4 col-xs-5">
				<div class="row">
					<div class="col-md-12">
						<form class="panel panel-primary">
							<h5 class="panel-heading">Gradovi:</h5>
							<input type="checkbox" id="city1" name="city-1" value="city1">
							<label for="city1">Bijeljina</label>
							<input type="checkbox" id="City2" name="city-2" value="City2" >
							<label for="City2">Banja Luka</label>
							<input type="checkbox" id="City3" name="city-3" value="City3" >
							<label for="City3">City3</label>
							<input type="checkbox" id="city4" name="city-4" value="Strawberry">
							<label for="city4">City4</label>
							<input type="checkbox" id="city5" name="city-5" value="blackberry" >
							<label for="city5">City5</label>
							<input type="checkbox" id="city6" name="city-6" value="bbb">
							<label for="city6">City6</label>
							<div id="more" hidden>
								<input type="checkbox" id="city7" name="city-7" value="7777" >
								<label for="city7">City7</label>
								<input type="checkbox" id="city8" name="city-8" value="88888">
								<label for="city8">City8</label>
							</div>
							<span class="readmore" id="myBtn" style="display: block;">Prikaži sve</span>
							<hr>
							<h5 class="panel-heading">Apoteke:</h5>
							<input type="checkbox" id="fruit9" name="fruit-9" value="Apot1">
							<label for="fruit9">Rosic</label>
							<input type="checkbox" id="apot2" name="apot-2" value="apot2" >
							<label for="apot2">Apot2</label>
							<input type="checkbox" id="fruit3" name="fruit-3" value="Cherry" >
							<label for="fruit3">Apot3</label>
							<input type="checkbox" id="fruit4" name="fruit-4" value="Strawberry">
							<label for="fruit4">Apot4</label>
							<input type="checkbox" id="fruit5" name="fruit-5" value="blackberry" >
							<label for="fruit5">Apot5</label>
							<input type="checkbox" id="fruit6" name="fruit-6" value="bbb">
							<label for="fruit6">Apot6</label>
							<div id="moreApot" hidden>
								<input type="checkbox" id="fruit7" name="fruit-7" value="7777" >
								<label for="fruit7">Apot7</label>
								<input type="checkbox" id="fruit8" name="fruit-8" value="88888">
								<label for="fruit8">Apot8</label>
							</div>
							<span class="readmore" id="apotMyBtn" style="display: block;">Prikaži sve</span>
							<hr>
							<div class="widget widget_categories">
								<h5 class="title panel-heading">Kategorije</h5>
								<ul class="ul-categories">
									<li><a href="#" title="View all posts filed under Environment">Zdravlje</a></li>
									<li><a href="#" title="View all posts filed under Health Basics">Prehrana</a></li>
									<li><a href="#" title="View all posts filed under Lifestyle">Ljepota</a></li>
									<li><a href="#" title="View all posts filed under Motivation">Mame i bebe</a></li>
								</ul>
							</div>
							<div class="switch-box">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<span class="">Otvoreno </span>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<div class="switch">
											<input type="checkbox" id="checkbox1">
											<label for="checkbox1"></label>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row ">
					<div class="col-md-12">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<section class="widget ads-item-wrap">
							<h5 class="title">Reklame</h5>
							<div class="ads">
								<a href="https://www.prirodnolecenje.com/sremus-kapi-ramslin/" title="sremus-kapi">
									<img src="images/blog/images.jpg" alt="kapiSremus">
								</a>    
							</div>
						</section>
					</div>
				</div>
			</div>

			<!-- firs right part -->
			<div class="col-md-9 col-sm-8 col-xs-7">
				<!-- first right part -->
				<div class="row">
					<div class="col-md-12">
						<div class="search-results">
							<div class="row">
								<div class="col-md-3 col-sm-12 col-xs-12">
									<a href="#"><img src="images/products/teliska.jpg" alt="teliska"></a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="apoteka-results">
										<h3>
											<a href="#">Teliska AM-H </a>
										</h3>
										<div class="tags">  
											<a href="#">tag1</a>  
											<a href="#">tag2</a>  
											<a href="#">tag3</a>  
											<a href="#">tag4</a>  
											<a href="#">tag5</a>  
											<a href="#">tag6</a> 
										</div> 
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12 price">
									<h3>Cijena:</h3>
									<span>5,50 KM</span>
									<span>-</span>
									<span>6,00 KM</span>
								<div class="action">
									<span>Akcija 10%</span>
								</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="search-results">
							<div class="row">
								<div class="col-md-3 col-sm-12 col-xs-12">
									<a href="#"><img src="images/search/paracetamol.jpg" alt="lorempics"></a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="apoteka-results">
										<h3>
											<a href="#">Paracet 500</a>
										</h3>
										<div class="tags">  
											<a href="#">tag1</a>  
											<a href="#">tag2</a>  
											<a href="#">tag3</a>  
											<a href="#">tag4</a>  
											<a href="#">tag5</a>  
											<a href="#">tag6</a> 
										</div> 
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12 price">
									<h3>Cijena:</h3>
									<span>3,50 KM</span>
									<span>-</span>
									<span>4,80 KM</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="search-results">
							<div class="row">
								<div class="col-md-3 col-sm-12 col-xs-12">
									<a href="#"><img src="images/products/plivit.jpg" alt="lorempics"></a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="apoteka-results">
										<h3>
											<a href="#">Plivit Total</a>
										</h3>
										<div class="tags">  
											<a href="#">tag1</a>  
											<a href="#">tag2</a>  
											<a href="#">tag3</a>  
											<a href="#">tag4</a>  
											<a href="#">tag5</a>  
											<a href="#">tag6</a> 
										</div> 
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12 price">
									<h3>Cijena:</h3>
									<span>7,50 KM</span>
									<span>-</span>
									<span>9,00 KM</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="search-results">
							<div class="row">
								<div class="col-md-3 col-sm-12 col-xs-12">
									<a href="#"><img src="images/products/citicoline.jpg" alt="lorempics"></a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="apoteka-results">
										<h3>
											<a href="#">Citcol-500 </a>
										</h3>
										<div class="tags">  
											<a href="#">tag1</a>  
											<a href="#">tag2</a>  
											<a href="#">tag3</a>  
											<a href="#">tag4</a>  
											<a href="#">tag5</a>  
											<a href="#">tag6</a> 
										</div> 
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12 price">
									<h3>Cijena:</h3>
									<span>12,50 KM</span>
									<span>-</span>
									<span>14,50 KM</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="search-results">
							<div class="row">
								<div class="col-md-3 col-sm-12 col-xs-12">
									<a href="#"><img src="images/products/flupara.jpg" alt="lorempics"></a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="apoteka-results">
										<h3>
											<a href="#">Flupara-500</a>
										</h3>
										<div class="tags">  
											<a href="#">tag1</a>  
											<a href="#">tag2</a>  
											<a href="#">tag3</a>  
											<a href="#">tag4</a>  
											<a href="#">tag5</a>  
											<a href="#">tag6</a> 
										</div> 
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12 price">
									<h3>Cijena:</h3>
									<span>16,00 KM</span>
									<span>-</span>
									<span>19,00 KM</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="search-results">
							<div class="row">
								<div class="col-md-3 col-sm-12 col-xs-12">
									<a href="#"><img src="images/products/spirulina.jpg" alt="lorempics"></a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="apoteka-results">
										<h3>
											<a href="#">Spirulina </a>
										</h3>
										<div class="tags">  
											<a href="#">tag1</a>  
											<a href="#">tag2</a>  
											<a href="#">tag3</a>  
											<a href="#">tag4</a>  
											<a href="#">tag5</a>  
											<a href="#">tag6</a> 
										</div> 
									</div>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12 price">
									<h3>Cijena:</h3>
									<span>15,50 KM</span>
									<span>-</span>
									<span>17,00 KM</span>
								</div>
							</div>
						</div>
					</div>
					
					<!-- pagination -->
					<div class="row">
						<div class="col-md-12">
							<div class="pagination-wrap">
								<ul class="pagination" role="navigation">
									<li class="page-item disabled" aria-disabled="true" aria-label="&amp;laquo; Previous">
										<span class="page-link" aria-hidden="true">‹</span>
									</li>
									<li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">4</a></li>
									<li class="page-item"><a class="page-link" href="#">5</a></li>
									<li class="page-item"><a class="page-link" href="#">6</a></li>
									<li class="page-item"><a class="page-link" href="#">7</a></li>
									<li class="page-item"><a class="page-link" href="#">8</a></li>
									<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>                                                           
									<li class="page-item"><a class="page-link" href="#">50</a></li>
									<li class="page-item"><a class="page-link" href="#">51</a></li>         
									<li class="page-item">
										<a class="page-link" href="#" rel="next" aria-label="Next &amp;raquo;">›</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection