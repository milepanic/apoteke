		<div class="mr-auto">
			<h3 class="m-subheader__title m-subheader__title--separator">@yield ('title', 'Administration')</h3>	
			@foreach ($breadcrumbs as $breadcrumb)	
			@if (count($breadcrumbs))	
			<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
				@if($breadcrumb->url && $breadcrumb->title == 'Home')
				<li class="m-nav__item m-nav__item--home">
					<a href="{{ $breadcrumb->url }}" class="m-nav__link m-nav__link--icon">
						<i class="m-nav__link-icon la la-home"></i>
					</a>
				</li>
				@elseif($breadcrumb->url)
				<li class="m-nav__separator">-</li>
				<li class="m-nav__item">
					<a href="{{ $breadcrumb->url }}" class="m-nav__link">
						<span class="m-nav__link-text">{{ $breadcrumb->title }}</span>
					</a>
				</li>
				@elseif($breadcrumb->url && $loop->last)
				<li class="m-nav__item">
					<a href="{{ $breadcrumb->url }}" class="m-nav__link">
						<span class="m-nav__link-text">{{ $breadcrumb->title }}</span>
					</a>
				</li>
				@endif
			</ul>
			@endif
			@endforeach
		</div>

