{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login | Apoteke.ba</title>

    <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/fonts/line-awesome/css/line-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/fonts/open-sans/styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/tether/css/tether.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/common.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/pages/auth.min.css') }}">
</head>
<body>

<div class="ks-page">
    <div class="ks-page-header">
        <a href="{{ url('/') }}" class="ks-logo">
            <img src="{{ asset('theme/apoteke/images/logo.png') }}" alt="Apoteke.ba logo" height="30">
        </a>
    </div>
    <div class="ks-page-content">
        <div class="ks-logo">apoteke.ba</div>

        <div class="card panel panel-default ks-light ks-panel ks-login">
            <div class="card-block">
                <form 
                    action="{{ route('login') }}"
                    method="POST"
                    class="form-container" 
                    aria-label="{{ __('Login') }}"
                >
                    @csrf
                    <h4 class="ks-header">{{ __('Login') }}</h4>
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input 
                                name="email"
                                type="email" 
                                class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" 
                                placeholder="{{ __('Email') }}"
                                value="{{ old('email') }}"
                                required
                                autofocus 
                            >
                            
                            <span class="icon-addon">
                                <span class="la la-at"></span>
                            </span>
                        </div>
                        @if ($errors->has('email'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input 
                                type="password" 
                                name="password"
                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" 
                                placeholder="{{ __('Password') }}"
                            >
                            <span class="icon-addon">
                                <span class="la la-key"></span>
                            </span>
                        </div>
                        @if ($errors->has('password'))
                            <span class="text-danger" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-inline mb-3">
                        <input 
                            type="checkbox" 
                            name="remember" 
                            class="form-check-input" 
                            id="remember" 
                            {{ old('remember') ? 'checked' : '' }}
                        >
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Login</button>
                    </div>
                    <div class="ks-text-center">
                        <a href="{{ route('password.request') }}">{{ __('Forgot your password?') }}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="ks-footer">
        <a href="https://www.laracode.net" target="_blank">
            <img src="{{ asset('laracode-logo.svg') }}" alt="Laracode logo" height="30">
        </a>
        <ul>
            <li>
                <a href="#">{{ __('Privacy Policy') }}</a>
            </li>
            <li>
                <a href="mailto:admin@laracode.net">{{ __('Email us') }}</a>
            </li>
        </ul>
    </div>
</div>

<script src="libs/jquery/jquery.min.js"></script>
<script src="libs/tether/js/tether.min.js"></script>
<script src="libs/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

 --}}
