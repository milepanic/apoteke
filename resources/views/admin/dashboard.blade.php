@extends('admin.layouts.master')
@section('title', 'Dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
<!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            {{ Breadcrumbs::render('lc-admin') }}
        </div>
    </div>
<!-- END: Subheader -->

</div>
@endsection