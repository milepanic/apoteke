@extends('admin.layouts.master')
@section('title', 'Create User')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			{{ Breadcrumbs::render('lc-admin.users.create') }}
		</div>
	</div>
	<!-- END: Subheader -->    
	<div class="m-content">
		<div class="row">
			<div class="col-lg-12">
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__head">
						<div class="m-portlet__head-progress">
							<!-- here can place a progress bar-->
						</div>
						<div class="m-portlet__head-wrapper">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Create new user
									</h3>
								</div>
							</div>
							<div class="m-portlet__head-tools">
								<a href="{{ route('lc-admin.users.index') }}" class="btn btn-secondary m-btn m-btn--icon m-btn--sm m--margin-right-10">
									<span>
										<i class="la la-arrow-left"></i>
										<span>Back</span>
									</span>
								</a>
								<div class="btn-group">
									<button name="save" type="button" class="submit-form btn btn-accent m-btn m-btn--icon m-btn--sm">
										<span>
											<i class="la la-check"></i>
											<span>Save</span>
										</span>
									</button>
									<button type="button" class="btn btn-accent  dropdown-toggle dropdown-toggle-split m-btn m-btn--sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<button name="save-and-new" type="button" class="submit-form dropdown-item submit-form btn m-btn--icon">
											<span>
												<i class="la la-check"></i>
												<span>Save & New</span>
											</span>
										</button>
										<button name="save-and-back" type="button" class="submit-form dropdown-item submit-form btn m-btn--icon">
											<span>
												<i class="la la-check"></i>
												<span>Save & Back</span>
											</span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<form id="user-form" class="m-form m-form--label-align-left- m-form--state" action="{{ route('lc-admin.users.store') }}" method="POST">
							@csrf
							<input type="hidden" name="action">
							<!--begin: Form Body -->
							<div class="m-portlet__body">
								<div class="row">
									<div class="col-xl-8 offset-xl-2">
										<div class="m-form__section m-form__section--first">
											<div class="m-form__heading">
												<h3 class="m-form__heading-title">Account Details</h3>
											</div>
											<div class="form-group m-form__group row {{ $errors->has('name') ? 'has-danger' : null }}">
												<label class="col-xl-3 col-lg-3 col-form-label">* Name:</label>
												<div class="col-xl-9 col-lg-9">
													<input type="text" name="name" class="form-control m-input" value="{{ old('name')}}" placeholder="">
													@if ($errors->has('name'))
													<div class="form-control-feedback">
														{{ $errors->first('name') }}
													</div>
													@else
													<span class="m-form__help">Please enter your first and last names</span>
													@endif
												</div>

											</div>
											<div class="form-group m-form__group row {{ $errors->has('email') ? 'has-danger' : null }}">
												<label class="col-xl-3 col-lg-3 col-form-label">* Email:</label>
												<div class="col-xl-9 col-lg-9">
													<input type="email" name="email" class="form-control m-input" value="{{ old('email') }}" placeholder="">
													@if ($errors->has('email'))
													<div class="form-control-feedback">
														{{ $errors->first('email') }}
													</div>
													@else
													<span class="m-form__help">Please enter user email</span>
													@endif
												</div>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-6 m-form__group-sub {{ $errors->has('username') ? 'has-danger' : null }}">
													<label class="form-control-label">* Username:</label>
													<input type="text" name="username" class="form-control m-input" value="{{ old('username') }}" placeholder="">
													@if ($errors->has('username'))
													<div class="form-control-feedback">
														{{ $errors->first('username') }}
													</div>
													@else
													<span class="m-form__help">Please enter unique username</span>
													@endif
												</div>
												<div class="col-lg-6 m-form__group-sub {{ $errors->has('password') ? 'has-danger' : null }}">
													<label class="form-control-label">* Password:</label>
													<input type="password" name="password" class="form-control m-input" placeholder="">
													@if ($errors->has('password'))
													<div class="form-control-feedback">
														{{ $errors->first('password') }}
													</div>
													@else
													<span class="m-form__help">Please enter minimal 6 characters</span>
													@endif
												</div>
											</div>
										</div>
										<div class="m-separator m-separator--dashed m-separator--lg"></div>
										<div class="m-form__section">
											<div class="m-form__heading">
												<h3 class="m-form__heading-title">Client Settings</h3>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-6 m-form__group-sub {{ $errors->has('roles') ? 'has-danger' : null }}">
													<label class="form-control-label">* Roles:</label>
													<div class="m-checkbox-inline">
														@foreach($roles as $role)
														<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
															<input type="checkbox" name="roles[]" value="{{ $role->id }}" {{ in_array($role->id, old('roles', [])) ? 'checked' : null }}> {{ $role->name }} 
															<span></span>
														</label>
														@endforeach
													</div>
													@if ($errors->has('roles'))
													<div class="form-control-feedback">
														{{ $errors->first('roles') }}
													</div>
													@else
													<span class="m-form__help">Please select user group</span>
													@endif
												</div>
												<div class="col-lg-6 m-form__group-sub {{ $errors->has('permissions') ? 'has-danger' : null }}">
													<label class="form-control-label">* Permissions:</label>
													<div class="m-checkbox-inline">
														@foreach($permissions as $permission)
														<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
															<input type="checkbox" name="permissions[]" value="{{ $permission->id }}" {{ in_array($permission->id, old('permissions', [])) ? 'checked' : null }}> {{ $permission->name }} 
															<span></span>
														</label>
														@endforeach
													</div>
													@if ($errors->has('permissions'))
													<div class="form-control-feedback">
														{{ $errors->first('permissions') }}
													</div>
													@else
													<span class="m-form__help">Please select user permission</span>
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--end::Portlet-->
			</div>
		</div>		        
	</div>
</div>

@endsection
@section('external-js')
<script>
	$('.submit-form').click(function (e) {
		$('input[name=action]').val($(this).attr('name'))
		$('form#user-form').submit();
	})

</script>
@endsection