@extends('admin.layouts.master')
@section('title', 'Users')
@section('external-css')
 <!--begin::Page Vendors Styles -->
<link href="{{ asset('METRONIC/assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" /><!--RTL version:<link href="../../../assets/vendors/custom/datatables/datatables.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            {{ Breadcrumbs::render('lc-admin.users') }}
        </div>
    </div>
<!-- END: Subheader -->             
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-progress">
                <!-- here can place a progress bar-->
            </div>
            <div class="m-portlet__head-wrapper">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ __('Users') }}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <a href="{{ route('lc-admin.users.create') }}" class="btn btn-accent m-btn m-btn--icon m-btn--sm m--margin-right-10">
                        <span>
                            <i class="la la-plus"></i>
                            <span>{{ __('New user') }}</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead>
                    <tr>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('Email') }}</th>
                        <th>{{ __('Roles') }}</th>
                        <th>{{ __('Special Permissions') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->             
</div>

@endsection
@section('external-js')

<script src="{{ asset('js/datatables.bundle.js') }}"></script>
<script src="{{ asset('admin/users-datatable.js') }}"></script>

@endsection