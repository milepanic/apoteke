@extends('lc-admin.layouts.master')
@section('title', __('New Group'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Group') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.groups.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New Group') }}</h5>
                                    <form action="{{ route('lc-admin.groups.store') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">{{ __('Name') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="name"
                                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                    id="name-input"
                                                    placeholder="{{ __('Group name') }}"
                                                    value="{{ old('name') }}"
                                                >
                                                @if($errors->has('name'))
                                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div>
                                            <div class="preview-div col-md-9 mb-3 ml-auto" hidden>
                                                <img class="preview-image img-fluid" src="#">
                                            </div>
                                            <div class="form-group row">
                                                <label for="logo-input" class="col-sm-3 form-control-label">{{ __('Logo') }}</label>
                                                <div class="col-sm-9">
                                                    <label class="btn btn-primary ks-btn-file">
                                                        <span class="la la-cloud-upload ks-icon"></span>
                                                        <span class="ks-text">{{ __('Choose file') }}</span>
                                                        <input class="preview-input" type="file" name="logo">
                                                    </label>
                                                    @if($errors->has('logo'))
                                                        <small class="text-danger">{{ $errors->first('logo') }}</small>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="description-input" class="col-sm-3 form-control-label">{{ __('Description') }}</label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="description"
                                                    class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                                    id="description-input"
                                                    placeholder="{{ __('Group description') }}"
                                                >{{ old('description') }}</textarea>
                                                @if($errors->has('description'))
                                                    <small class="text-danger">{{ $errors->first('description') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="pharmacy-input" class="col-sm-3 form-control-label">{{ __('Pharmacies') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control multiselect2-pharmacies {{ $errors->has('pharmacies') ? 'is-invalid' : '' }}"
                                                    name="pharmacies[]"
                                                    id="pharmacy-input"
                                                    placeholder="{{ __('Select pharmacies') }}"
                                                    multiple
                                                >
                                                    @foreach($pharmacies as $pharmacy)
                                                        <option value="{{ $pharmacy->id }}">{{ $pharmacy->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('pharmacies'))
                                                    <small class="text-danger">{{ $errors->first('pharmacies') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="user-input" class="col-sm-3 form-control-label">{{ __('Users') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control multiselect2-users {{ $errors->has('users') ? 'is-invalid' : '' }}"
                                                    name="users[]"
                                                    id="user-input"
                                                    multiple
                                                >
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->first_name . ' ' . $user->last_name . ', ' . $user->email }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('users'))
                                                    <small class="text-danger">{{ $errors->first('users') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="web-page-input" class="col-sm-3 form-control-label">
                                                {{ __('Web Page') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="web_page"
                                                    class="form-control {{ $errors->has('web_page') ? 'is-invalid' : '' }}"
                                                    id="web-page-input"
                                                    placeholder="{{ __('Group web page') }}"
                                                    value="{{ old('web_page') }}"
                                                >
                                                @if($errors->has('web_page'))
                                                    <small class="text-danger">{{ $errors->first('web_page') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="facebook-input" class="col-sm-3 form-control-label">
                                                {{ __('Facebook') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="facebook"
                                                    class="form-control {{ $errors->has('facebook') ? 'is-invalid' : '' }}"
                                                    id="facebook-input"
                                                    placeholder="{{ __('Group facebook') }}"
                                                    value="{{ old('facebook') }}"
                                                >
                                                @if($errors->has('facebook'))
                                                    <small class="text-danger">{{ $errors->first('facebook') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="instagram-input" class="col-sm-3 form-control-label">
                                                {{ __('Instagram') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="instagram"
                                                    class="form-control {{ $errors->has('instagram') ? 'is-invalid' : '' }}"
                                                    id="instagram-input"
                                                    placeholder="{{ __('Group instagram') }}"
                                                    value="{{ old('instagram') }}"
                                                >
                                                @if($errors->has('instagram'))
                                                    <small class="text-danger">{{ $errors->first('instagram') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="google-input" class="col-sm-3 form-control-label">
                                                {{ __('Google +') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="google"
                                                    class="form-control {{ $errors->has('google') ? 'is-invalid' : '' }}"
                                                    id="google-input"
                                                    placeholder="{{ __('Group google') }}"
                                                    value="{{ old('google') }}"
                                                >
                                                @if($errors->has('google'))
                                                    <small class="text-danger">{{ $errors->first('google') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="linkedin-input" class="col-sm-3 form-control-label">
                                                {{ __('Linkedin') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="linkedin"
                                                    class="form-control {{ $errors->has('linkedin') ? 'is-invalid' : '' }}"
                                                    id="linkedin-input"
                                                    placeholder="{{ __('Group linkedin') }}"
                                                    value="{{ old('linkedin') }}"
                                                >
                                                @if($errors->has('linkedin'))
                                                    <small class="text-danger">{{ $errors->first('linkedin') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="twitter-input" class="col-sm-3 form-control-label">
                                                {{ __('Twitter') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="twitter"
                                                    class="form-control {{ $errors->has('twitter') ? 'is-invalid' : '' }}"
                                                    id="twitter-input"
                                                    placeholder="{{ __('Group twitter') }}"
                                                    value="{{ old('twitter') }}"
                                                >
                                                @if($errors->has('twitter'))
                                                    <small class="text-danger">{{ $errors->first('twitter') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="youtube-input" class="col-sm-3 form-control-label">
                                                {{ __('Youtube') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="youtube"
                                                    class="form-control {{ $errors->has('youtube') ? 'is-invalid' : '' }}"
                                                    id="youtube-input"
                                                    placeholder="{{ __('Group youtube') }}"
                                                    value="{{ old('youtube') }}"
                                                >
                                                @if($errors->has('youtube'))
                                                    <small class="text-danger">{{ $errors->first('youtube') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="pinterest-input" class="col-sm-3 form-control-label">
                                                {{ __('Pinterest') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="pinterest"
                                                    class="form-control {{ $errors->has('pinterest') ? 'is-invalid' : '' }}"
                                                    id="pinterest-input"
                                                    placeholder="{{ __('Group pinterest') }}"
                                                    value="{{ old('pinterest') }}"
                                                >
                                                @if($errors->has('pinterest'))
                                                    <small class="text-danger">{{ $errors->first('pinterest') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                        <button class="float-right btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function() {
    $('.multiselect2-pharmacies').select2({
        placeholder: '{{ __('Select pharmacies') }}'
    });

    $('.multiselect2-users').select2({
        placeholder: '{{ __('Select users') }}'
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.preview-image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".preview-input").change(function() {
        $('.preview-div').attr('hidden', false);
        readURL(this);
    });
});
</script>
@endsection
