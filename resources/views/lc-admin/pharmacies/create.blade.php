@extends('lc-admin.layouts.master')
@section('title', __('New Pharmacy'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Pharmacy') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.pharmacies.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New Pharmacy') }}</h5>
                                    <form action="{{ route('lc-admin.pharmacies.store') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="visible-input" class="col-sm-3 form-control-label">
                                                {{ __('Visible') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <label class="ks-checkbox-slider ks-on-off ks-solid ks-primary">
                                                    <input
                                                        type="checkbox"
                                                        name="visible"
                                                        id="visible-input"
                                                        value="1"
                                                    >
                                                    <span class="ks-indicator"></span>
                                                    <span class="ks-on">On</span>
                                                    <span class="ks-off">Off</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">{{ __('Name') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="name"
                                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                    id="name-input"
                                                    placeholder="{{ __('Pharmacy name') }}"
                                                    value="{{ old('name') }}"
                                                >
                                                @if($errors->has('name'))
                                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="emails-input" class="col-sm-3 form-control-label">
                                                {{ __('Emails') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <div id="emails-form">
                                                    <input
                                                        type="email"
                                                        name="emails[]"
                                                        class="col-sm-12 form-control"
                                                        placeholder="{{ __('Pharmacy email') }}"
                                                    >
                                                </div>
                                                <div class="form-inline" id="append-emails-form"></div>
                                                <button class="btn btn-primary pull-right mt-2" id="add-new-email">
                                                    +
                                                </button>
                                            </div>
                                            @if($errors->has('emails'))
                                                <small class="text-danger">{{ $errors->first('emails') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone-numbers-input" class="col-sm-3 form-control-label">
                                                {{ __('Phone numbers') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <div id="phone-numbers-form">
                                                    <input
                                                        type="text"
                                                        name="phone_numbers[]"
                                                        class="col-sm-12 form-control"
                                                        placeholder="{{ __('Pharmacy phone numbers') }}"
                                                    >
                                                </div>
                                                <div class="form-inline" id="append-phone-numbers-form"></div>
                                                <button class="btn btn-primary pull-right mt-2" id="add-new-phone-number">
                                                    +
                                                </button>
                                            </div>
                                            @if($errors->has('phone_numbers'))
                                                <small class="text-danger">{{ $errors->first('phone_numbers') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group row">
                                            <label for="fax-numbers-input" class="col-sm-3 form-control-label">
                                                {{ __('Fax numbers') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <div id="fax-numbers-form">
                                                    <input
                                                        type="text"
                                                        name="fax_numbers[]"
                                                        class="col-sm-12 form-control"
                                                        placeholder="{{ __('Pharmacy fax numbers') }}"
                                                    >
                                                </div>
                                                <div class="form-inline" id="append-fax-numbers-form"></div>
                                                <button class="btn btn-primary pull-right mt-2" id="add-new-fax-number">
                                                    +
                                                </button>
                                            </div>
                                            @if($errors->has('fax_numbers'))
                                                <small class="text-danger">{{ $errors->first('fax_numbers') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group row">
                                            <label for="about-us-input" class="col-sm-3 form-control-label">
                                                {{ __('About us') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="about_us"
                                                    id="about-us-input"
                                                    class="form-control {{ $errors->has('about-us') ? 'is-invalid' : '' }}"
                                                    placeholder="{{ __('About pharmacy') }}"
                                                >{{ old('about_us') }}</textarea>
                                                @if($errors->has('about_us'))
                                                    <small class="text-danger">{{ $errors->first('about_us') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="web-page-input" class="col-sm-3 form-control-label">
                                                {{ __('Web Page') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="web_page"
                                                    class="form-control {{ $errors->has('web_page') ? 'is-invalid' : '' }}"
                                                    id="web-page-input"
                                                    placeholder="{{ __('Pharmacy web page') }}"
                                                    value="{{ old('web_page') }}"
                                                >
                                                @if($errors->has('web_page'))
                                                    <small class="text-danger">{{ $errors->first('web_page') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div>
                                            <div class="form-group row">
                                                <label for="documents-input" class="col-sm-3 form-control-label">
                                                    {{ __('Documents') }}<span class="text-danger"> *</span>
                                                </label>
                                                <div class="col-sm-9">
                                                    <label class="btn btn-primary ks-btn-file">
                                                        <span class="la la-cloud-upload ks-icon"></span>
                                                        <span class="ks-text">{{ __('Choose files') }}</span>
                                                        <input
                                                            id="documents-input"
                                                            type="file" 
                                                            name="documents[]"
                                                            multiple
                                                        >
                                                    </label>
                                                </div>
                                            </div>
                                            @if($errors->has('documents'))
                                                <small class="text-danger">{{ $errors->first('documents') }}</small>
                                            @endif
                                        </div>
                                        <hr>
                                        <div>
                                            <div class="preview-div col-md-9 mb-3 ml-auto" hidden>
                                                <img class="preview-image img-fluid" src="#">
                                            </div>
                                            <div class="form-group row">
                                                <label for="cover-image-input" class="col-sm-3 form-control-label">
                                                    {{ __('Cover Image') }}
                                                </label>
                                                <div class="col-sm-9">
                                                    <label class="btn btn-primary ks-btn-file">
                                                        <span class="la la-cloud-upload ks-icon"></span>
                                                        <span class="ks-text">{{ __('Choose file') }}</span>
                                                        <input 
                                                            class="preview-input" 
                                                            id="cover-image-input"
                                                            type="file" 
                                                            name="cover_image"
                                                        >
                                                    </label>
                                                </div>
                                            </div>
                                            @if($errors->has('cover_image'))
                                                <small class="text-danger">{{ $errors->first('cover_image') }}</small>
                                            @endif
                                        </div>
                                        <hr>
                                        <div>
                                            <div class="preview-div col-md-9 mb-3 ml-auto" hidden>
                                                <img class="preview-image img-fluid" src="#">
                                            </div>
                                            <div class="form-group row">
                                                <label for="thumbnail-input" class="col-sm-3 form-control-label">
                                                    {{ __('Thumbnail') }}
                                                </label>
                                                <div class="col-sm-9">
                                                    <label class="btn btn-primary ks-btn-file">
                                                        <span class="la la-cloud-upload ks-icon"></span>
                                                        <span class="ks-text">{{ __('Choose file') }}</span>
                                                        <input 
                                                            class="preview-input" 
                                                            id="thumbnail-input"
                                                            type="file" 
                                                            name="thumbnail"
                                                        >
                                                    </label>
                                                </div>
                                            </div>
                                            @if($errors->has('cover_image'))
                                                <small class="text-danger">{{ $errors->first('cover_image') }}</small>
                                            @endif
                                        </div>
                                        <hr>
                                        <div>
                                            <div class="preview-div col-md-9 mb-3 ml-auto" hidden>
                                                <img class="preview-image img-fluid" src="#">
                                            </div>
                                            <div class="form-group row">
                                                <label for="gallery-input" class="col-sm-3 form-control-label">
                                                    {{ __('Gallery') }}<span class="text-danger"> *</span>
                                                </label>
                                                <div class="col-sm-9">
                                                    <label class="btn btn-primary ks-btn-file">
                                                        <span class="la la-cloud-upload ks-icon"></span>
                                                        <span class="ks-text">{{ __('Choose files') }}</span>
                                                        <input 
                                                            class="preview-input" 
                                                            id="gallery-input"
                                                            type="file" 
                                                            name="gallery[]"
                                                            multiple 
                                                        >
                                                    </label>
                                                </div>
                                            </div>
                                            @if($errors->has('gallery'))
                                                <small class="text-danger">{{ $errors->first('gallery') }}</small>
                                            @endif
                                        </div>
                                        <hr>
                                        <strong>{{ __('Working hours') }}</strong>
                                        <div class="form-inline row mt-3">
                                            <div class="col-lg-3">
                                                <div class="custom-control custom-checkbox">
                                                    <input 
                                                        id="wt-work-week" 
                                                        type="checkbox" 
                                                        class="custom-control-input"
                                                        checked
                                                    >
                                                    <label class="custom-control-label pull-left" for="wt-work-week">
                                                        {{ __('Work week') }}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-9">
                                                <input 
                                                    name="work_week[]"
                                                    class="form-control flatpickr wt-work-week working-time" 
                                                    data-enable-time="true"
                                                    data-time_24hr="true"
                                                    data-no-calendar="true"
                                                    value="08:00:00" 
                                                > 
                                                <span class="mx-2">{{ __('to') }}</span>
                                                <input 
                                                    name="work_week[]"
                                                    class="form-control flatpickr wt-work-week working-time" 
                                                    data-enable-time="true"
                                                    data-time_24hr="true"
                                                    data-no-calendar="true"
                                                    value="16:00:00" 
                                                >
                                            </div>
                                        </div>
                                        <div class="form-inline row">
                                            <div class="col-lg-3">
                                                <div class="custom-control custom-checkbox">
                                                    <input 
                                                        id="wt-saturday" 
                                                        type="checkbox" 
                                                        class="custom-control-input"
                                                        checked
                                                    >
                                                    <label class="custom-control-label pull-left" for="wt-saturday">
                                                        {{ __('Saturday') }}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-9">
                                                <input 
                                                    name="saturday[]"
                                                    class="form-control flatpickr wt-saturday working-time" 
                                                    data-enable-time="true"
                                                    data-time_24hr="true"
                                                    data-no-calendar="true"
                                                    value="08:00:00" 
                                                > 
                                                <span class="mx-2">{{ __('to') }}</span>
                                                <input 
                                                    name="saturday[]"
                                                    class="form-control flatpickr wt-saturday working-time" 
                                                    data-enable-time="true"
                                                    data-time_24hr="true"
                                                    data-no-calendar="true"
                                                    value="16:00:00" 
                                                >
                                            </div>
                                        </div>
                                        <div class="form-inline row">
                                            <div class="col-lg-3">
                                                <div class="custom-control custom-checkbox">
                                                    <input 
                                                        id="wt-sunday" 
                                                        type="checkbox" 
                                                        class="custom-control-input"
                                                        checked
                                                    >
                                                    <label class="custom-control-label pull-left" for="wt-sunday">
                                                        {{ __('Sunday') }}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-9">
                                                <input 
                                                    name="sunday[]"
                                                    class="form-control flatpickr wt-sunday working-time" 
                                                    data-enable-time="true"
                                                    data-time_24hr="true"
                                                    data-no-calendar="true"
                                                    value="08:00:00" 
                                                > 
                                                <span class="mx-2">{{ __('to') }}</span>
                                                <input 
                                                    name="sunday[]"
                                                    class="form-control flatpickr wt-sunday working-time" 
                                                    data-enable-time="true"
                                                    data-time_24hr="true"
                                                    data-no-calendar="true"
                                                    value="16:00:00" 
                                                >
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label for="group-input" class="col-sm-3 form-control-label">
                                                {{ __('Groups') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control multiselect2-groups"
                                                    name="groups[]"
                                                    id="group-input"
                                                    multiple
                                                >
                                                    @foreach ($groups as $group)
                                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('groups'))
                                                    <small class="text-danger">{{ $errors->first('groups') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="user-input" class="col-sm-3 form-control-label">
                                                {{ __('Users') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control multiselect2-users"
                                                    name="users[]"
                                                    id="user-input"
                                                    multiple
                                                >
                                                    @foreach ($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->email }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('users'))
                                                    <small class="text-danger">{{ $errors->first('users') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label for="location-input" class="col-sm-3 form-control-label">
                                                {{ __('Location') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control {{ $errors->has('location_id') ? 'is-invalid' : '' }}"
                                                    name="location_id"
                                                    id="location-input"
                                                >
                                                    <option disabled selected>{{ __('Select location') }}</option>
                                                    @foreach ($locations as $location)
                                                        <option value="{{ $location->id }}">{{ $location->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('location_id'))
                                                    <small class="text-danger">{{ $errors->first('location_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="address-input" class="col-sm-3 form-control-label">
                                                {{ __('Address') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="address"
                                                    class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}"
                                                    id="address-input"
                                                    placeholder="{{ __('Pharmacy address') }}"
                                                    value="{{ old('address') }}"
                                                >
                                                @if($errors->has('address'))
                                                    <small class="text-danger">{{ $errors->first('address') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="longitude-input" class="col-sm-3 form-control-label">
                                                {{ __('Longitude') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="longitude"
                                                    class="form-control {{ $errors->has('longitude') ? 'is-invalid' : '' }}"
                                                    id="longitude-input"
                                                    placeholder="{{ __('Pharmacy longitude') }}"
                                                    value="{{ old('longitude') }}"
                                                >
                                                @if($errors->has('longitude'))
                                                    <small class="text-danger">{{ $errors->first('longitude') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="latitude-input" class="col-sm-3 form-control-label">
                                                {{ __('Latitude') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="latitude"
                                                    class="form-control {{ $errors->has('latitude') ? 'is-invalid' : '' }}"
                                                    id="latitude-input"
                                                    placeholder="{{ __('Pharmacy latitude') }}"
                                                    value="{{ old('latitude') }}"
                                                >
                                                @if($errors->has('latitude'))
                                                    <small class="text-danger">{{ $errors->first('latitude') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @if(Auth::user()->hasRole('super admin'))
                                            <div class="form-group row">
                                                <label for="admin-report-input" class="col-sm-3 form-control-label">
                                                    <strong>{{ __('Admin report') }}</strong><span class="text-danger"> *</span>
                                                </label>
                                                <div class="col-sm-9">
                                                    <textarea
                                                        name="admin_report"
                                                        id="admin-report-input"
                                                        class="form-control {{ $errors->has('admin_report') ? 'is-invalid' : '' }}"
                                                        placeholder="{{ __('Admin report') }}"
                                                    >{{ old('admin_report') }}</textarea>
                                                    @if($errors->has('admin_report'))
                                                        <small class="text-danger">{{ $errors->first('admin_report') }}</small>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                        <button class="float-right btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function() {
    $('.multiselect2-groups').select2({
        placeholder: '{{ __('Select groups') }}'
    });

    $('.multiselect2-users').select2({
        placeholder: '{{ __('Select users') }}'
    });

    $('.flatpickr').flatpickr();

    $('#wt-work-week, #wt-saturday, #wt-sunday').on('change', function () {
        if ($(this).is(':checked')) {
            $(this).parents(':eq(2)')
                .find('.working-time')
                .prop('disabled', false)
                .val('08:00');
        } else if (! $(this).is(':checked')) {
            $(this).parents(':eq(2)')
                .find('.working-time')
                .prop('disabled', true)
                .val('');
        }
    });

    $('#add-new-email').on('click', function (e) {
        e.preventDefault();

        $('#emails-form').children()
            .clone()
            .val('')
            .addClass('mt-2')
            .appendTo('#append-emails-form');
    });

    $('#add-new-phone-number').on('click', function (e) {
        e.preventDefault();

        $('#phone-numbers-form').children()
            .clone()
            .val('')
            .addClass('mt-2')
            .appendTo('#append-phone-numbers-form');
    });

    $('#add-new-fax-number').on('click', function (e) {
        e.preventDefault();

        $('#fax-numbers-form').children()
            .clone()
            .val('')
            .addClass('mt-2')
            .appendTo('#append-fax-numbers-form');
    });

    function readURL(input, imageTag) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                imageTag.attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".preview-input").change(function() {
        let parent = $(this).parents(':eq(3)');
        let imageTag = parent.find('.preview-image');


        parent.find('.preview-div').attr('hidden', false);

        readURL(this, imageTag);
    });

});
</script>
@endsection
