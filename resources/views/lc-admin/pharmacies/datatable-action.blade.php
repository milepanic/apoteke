<div class="ks-controls">
    <a href="{{ route('lc-admin.pharmacies.edit', ['pharmacies' => $pharmacy]) }}">
        <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-pencil ks-icon"></span>
        </button>
    </a>
    <button class="btn btn-danger ks-light ks-no-text delete-pharmacy" data-slug="{{ $pharmacy->slug }}" data-name="{{ $pharmacy->name }}" style="line-height: 38px;">
        <span class="la la-trash-o ks-icon"></span>
    </button>
</div>
