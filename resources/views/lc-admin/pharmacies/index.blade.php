@extends('lc-admin.layouts.master')
@section('title', __('Pharmacies'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Pharmacies') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.pharmacies') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-body-wrap-container">
                <div class="ks-full-table">
                    <div class="ks-full-table-header">
                        <h4 class="ks-full-table-name">{{ __('All pharmacies')}}</h4>
                        <div class="ks-controls">
                            <a href="{{ route('lc-admin.pharmacies.create') }}">
                                <button class="btn btn-primary">{{ __('New pharmacy')}}</button>
                            </a>
                        </div>
                    </div>
                    <table id="pharmacies-table" class="table ks-table-info dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="1">ID </th>
                                <th>{{__('Name') }}</th>
                                <th>{{ __('Groups') }}</th>
                                <th>{{ __('Users') }}</th>
                                <th>{{ __('Location') }}</th>
                                <th>{{ __('Address') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#pharmacies-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax:"/lc-admin/pharmacies", 
            columns: [
                { data: "id" }, 
                { 
                    data: "name",
                    render: function (data, type, row, meta) {
                        return "<a href='pharmacies/" + row.slug + "/edit'>" + data + "</a>";
                    }
                },
                { 
                    data: "groups",
                    orderable: false,
                    searchable: false,
                }, 
                { 
                    data: "users",
                    orderable: false,
                    searchable: false,
                }, 
                { data: "location.name" },
                { data: "address" },
                { data: "action" },
            ],
            columnDefs: [
                {
                    targets: 2,
                    render: function (data) {
                        groups = [];
                        $.each(data, function (i, group) {
                            groups.push("<a href='groups/" + group.id + "/edit'>" + group.name + "</a>");
                        });

                        return groups.join(', ');
                    }
                },
                {
                    targets: 3,
                    render: function (data) {
                        users = [];
                        $.each(data, function (i, user) {
                            users.push("<a href='users/" + user.id + "/edit'>" + user.email + "</a>");
                        });

                        return users.join(', ');
                    }
                }
            ]
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.delete-pharmacy', 'click', function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var slug = $(this).data('slug');
        var name = $(this).data('name');
        var table = $("#pharmacies-table").DataTable();
        var tr = $(this).parents('tr');

        $.confirm({
            title: 'Pažnja!',
            content: 'Da li želite da obrišete apoteku ' + name + '?',
            type: 'danger',
            buttons: {
                confirm: {
                    text: 'OK',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                            url: '/lc-admin/pharmacies/' + slug,
                            type: 'DELETE',
                        })
                        .done(function() {
                            table.row(tr).remove().draw();
                            ajaxNotify(data);
                        })
                        .fail(function() {
                            alert('Dogodila se greška');
                        });
                    }
                },
                cancel: function () {

                }
            }
        });
    });
});

</script>
@endsection
