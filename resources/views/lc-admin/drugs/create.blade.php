@extends('lc-admin.layouts.master')
@section('title', __('New Product'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Product') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.drugs.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New Product') }}</h5>
                                    <form action="{{ route('lc-admin.drugs.store') }}" method="POST">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="substance-input" class="col-sm-3 form-control-label">{{ __('Substance') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="substance" class="form-control" id="substance-input" placeholder="{{ __('Substance') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">{{ __('Name') }}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control" id="name-input" placeholder="{{ __('Name') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="manufacturer-input" class="col-sm-3 form-control-label">{{ __('Manufacturer') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="manufacturer" class="form-control" id="manufacturer-input" placeholder="{{ __('Manufacturer') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="shape-input" class="col-sm-3 form-control-label">{{ __('Shape') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="shape" class="form-control" id="shape-input" placeholder="{{ __('Shape') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="dosage-input" class="col-sm-3 form-control-label">{{ __('Dosage') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="dosage" class="form-control" id="dosage-input" placeholder="{{ __('Dosage') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="package-input" class="col-sm-3 form-control-label">{{ __('Package') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="package" class="form-control" id="package-input" placeholder="{{ __('Package') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 form-control-label p-t-0">{{ __('Issuing') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="issuing" class="custom-control-input" id="issuing-input">
                                                    <label class="custom-control-label" for="issuing-input">{{ __('Issuing') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="description-input" class="col-sm-3 form-control-label">{{ __('Description') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="description" class="form-control" id="description-input" placeholder="{{ __('Description') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="instruction-input" class="col-sm-3 form-control-label">{{ __('Instruction') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="instruction" class="form-control" id="instruction-input" placeholder="{{ __('Instruction') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="thumbnail-input" class="col-sm-3 form-control-label">{{ __('Thumbnail') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <label class="btn btn-primary ks-btn-file">
                                                    <span class="la la-cloud-upload ks-icon"></span>
                                                    <span class="ks-text">{{ __('Choose file') }}</span>
                                                    <input type="file" name="thumbnail">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 form-control-label p-t-0">{{ __('Official') }}</label>
                                            <div class="col-sm-9">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="official" class="custom-control-input" id="official-input">
                                                    <label class="custom-control-label" for="official-input">{{ __('Official') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Create') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
