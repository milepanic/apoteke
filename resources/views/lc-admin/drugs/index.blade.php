@extends('lc-admin.layouts.master')
@section('title', __('Drugs'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Drugs') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.drugs') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-body-wrap-container">
                <div class="ks-full-table">
                    <div class="ks-full-table-header">
                        <h4 class="ks-full-table-name">{{ __('All drugs')}}</h4>
                        
                        <div class="ks-controls">
                            @can('drug import')
                                <form action="{{ route('lc-admin.drugs.import') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="file" name="file">
                                    <button type="submit">submit</button>
                                </form>
                            @endcan
                            @can('drug create')
                                <a href="{{ route('lc-admin.drugs.create') }}">
                                    <button class="btn btn-primary">{{ __('New Product')}}</button>
                                </a>
                            @endcan
                        </div>
                    </div>
                    <table id="drugs-table" class="table ks-table-info dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="1">ID </th>
                                <th>{{ __('Name') }}</th>
                                @if(! Auth::user()->hasPermissionTo('drug read all'))
                                    <th>{{ __('Pharmacies') }}</th>
                                @endif
                                <th>{{ __('Official') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#drugs-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax: "/lc-admin/drugs", 
            columns: [
                { data: "id" }, 
                { data: "name" },
                @if(! Auth::user()->hasPermissionTo('drug read all'))
                    { 
                        data: "drug.pharmacies",
                        orderable: false,
                        searchable: false,
                    }, 
                @endif
                { data: "official" },
                { data: "action" }
            ],
            columnDefs: [
            @if(! Auth::user()->hasPermissionTo('drug read all'))
                {
                    targets: 2,
                    render: function (data) {
                        pharmacies = [];
                        $.each(data, function (i, pharmacy) {
                            pharmacies.push("<a href='pharmacies/" + pharmacy.id + "/edit'>" + pharmacy.name + "</a>");
                        });

                        return pharmacies.join(', ');
                    }
                }
            @endif
            ]
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();
});

</script>
@endsection
