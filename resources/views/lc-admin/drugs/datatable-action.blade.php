<div class="ks-controls">
    @can('drug update')
        <a href="#">
            <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
                <span class="la la-pencil ks-icon"></span>
            </button>
        </a>
    @endcan
    @can('drug delete')
        <button class="btn btn-danger ks-light ks-no-text delete-drug" data-id="#" data-name="#" style="line-height: 38px;">
            <span class="la la-trash-o ks-icon"></span>
        </button>
    @endcan
</div>
