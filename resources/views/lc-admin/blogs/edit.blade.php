@extends('lc-admin.layouts.master')
@section('title', __('New Blog Post'))
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Blog Post') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.blogs.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <form
                        id="form-edit-blog"
                        action="{{ route('lc-admin.blogs.update', $blog) }}" 
                        method="POST"
                        enctype="multipart/form-data" 
                    >
                        @method('PUT')
                        @csrf
                        <div class="form-group row col-md-6">
                            <label for="title-input" class="col-sm-2 form-control-label">{{ __('Title') }}</label>
                            <div class="col-sm-10">
                                <input
                                    type="text"
                                    name="title"
                                    class="form-control"
                                    id="title-input"
                                    value="{{ $blog->title }}"
                                    disabled
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="content">{{ old('content', $blog->content) }}</textarea>
                        </div>
                        <button class="btn-edit-blog btn btn-primary">{{ __('Update') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('external-js')
@include('lc-admin.blogs.components.tinymce-js')
<script type="application/javascript">
$(document).ready(function () {
    $('.btn-edit-blog').on('click', function (e) {
        e.preventDefault();

        $.confirm({
            title: 'Pažnja!',
            content: 'Da li želite da sačuvate izmjene?',
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'OK',
                    btnClass: 'btn-warning',
                    action: function () {
                        $('#form-edit-blog').submit();
                    }
                },
                cancel: function () {

                }
            }
        });
    });
});
</script>
@endsection
