@extends('lc-admin.layouts.master')
@section('title', __('New Blog Post'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/widgets/upload.min.css') }}">
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Blog Post') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.blogs.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <form 
                        action="{{ route('lc-admin.blogs.store') }}" 
                        method="POST"
                        enctype="multipart/form-data" 
                    >
                        @csrf
                        <input type="hidden" name="unique_key" value="{{ $uniqueKey }}">
                        <div class="form-group row col-md-6">
                            <label for="title-input" class="col-sm-2 form-control-label">{{ __('Title') }}</label>
                            <div class="col-sm-10">
                                <input
                                    type="text"
                                    name="title"
                                    class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                    id="title-input"
                                    placeholder="{{ __('Blog title') }}"
                                    value="{{ old('title') }}"
                                >
                                @if($errors->has('title'))
                                    <small class="text-danger">{{ $errors->first('title') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <h4>Upload images only.</h4>
                            <div id="ks-widget-attachments-images-only" class="card panel ks-widget-attachments">
                                <div class="card-block">
                                    <h5 class="ks-header">Add an image</h5>

                                    <div id="ks-file-upload-dropzone-images-only" class="ks-upload-block">
                                        <span class="ks-icon la la-cloud-upload"></span>
                                        <span class="ks-text">Drag &amp; drop files here</span>
                                        <span class="ks-upload-btn">
                                            <span class="ks-btn-separator">or</span>
                                            <button class="btn btn-primary ks-btn-file">
                                                <span class="la la-cloud-upload ks-icon"></span>
                                                <span class="ks-text">Choose file</span>
                                                <input 
                                                    id="ks-file-upload-widget-images-input" 
                                                    type="file" 
                                                    name="gallery[]" 
                                                    data-url="{{ route('lc-admin.images.store', ['path' => $uniqueKey]) }}" 
                                                    multiple
                                                    required
                                                >
                                            </button>
                                        </span>
                                    </div>

                                    <div class="ks-uploading-files-container">
                                        <h5 class="ks-header">Uploading images</h5>

                                        <div class="ks-uploading-files"></div>
                                    </div>

                                    <h5 class="ks-header">Uploaded images</h5>

                                    <div class="ks-files"><ul></ul></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="content">{{ old('content') }}</textarea>
                        </div>
                        <button class="btn btn-primary">{{ __('Submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/load-image.all.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/canvas-to-blob.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/jquery.fileupload-process.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/jquery.fileupload-image.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/jquery.fileupload-audio.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/jquery.fileupload-video.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-file-upload/js/jquery.fileupload-validate.js') }}"></script>

@include('lc-admin.blogs.components.tinymce-js')
<script type="application/javascript">
$(document).ready(function() {
    // Upload images only
    $('#ks-file-upload-widget-images-input').fileupload({
        dropZone: $('#ks-file-upload-dropzone-images-only'),
        autoUpload: true,
        dataType: 'json',
        singleFileUploads: true,
        acceptFileTypes:  /(jpeg)|(png)|(gif)|(jpg)$/i,
        add: function (e, data) {
            $('#ks-widget-attachments-images-only .ks-uploading-files-container').show();
            var jqXHR;

            $.each(data.files, function (index, file) {
                var fileUploadInfo = '<div id="file-uploading-' + file.lastModified + '" class="ks-uploading-progress-block"> \
                        <span class="ks-filename">' + file.name + '</span> \
                        <div class="ks-progress ks-progress-inline"> \
                        <div class="progress ks-progress-sm"> \
                        <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 0" aria-valuenow="00" aria-valuemin="0" aria-valuemax="100"></div> \
                        </div> \
                        <span class="ks-amount">0%</span> \
                        </div> \
                        <div class="ks-cancel-block"> \
                            <a class="ks-icon la la-close"></a> \
                        </div> \
                </div>';
                data.context = $(fileUploadInfo).appendTo($('#ks-widget-attachments-images-only .ks-uploading-files'));

                $(data.context).find(".ks-cancel-block").click(function() {
                    jqXHR.abort();
                    data.context.remove();
                })
            });

            data.process().done(function () {
                jqXHR = data.submit();
            });
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            var size = data.files[0].size;
            var uploadedBytes = (size / 100) * progress;
            var id = 'file-uploading-' + data.files[0].lastModified;

            $('#' + id).find('.progress-bar').css('width', progress + '%');
            $('#' + id).find('.ks-amount').text(progress + '%');
        },
        stop: function (e) {
            $('#ks-widget-attachments-images-only .ks-uploading-files-container').hide();
        },
        done: function (e, data) {
            $.each(data.files, function (index, file) {
                var id = 'file-uploading-' + data.files[0].lastModified;
                $('#' + id).remove();
            });

            var uploadedFileInfo = '<li class="ks-file"> \
                    <img class="ks-thumb" src="' + data.result + '"> \
                        <a class="ks-icon la la-trash ks-remove"></a> \
                    </li>';
            $(uploadedFileInfo).appendTo($('#ks-widget-attachments-images-only .ks-files ul'));
        },
        fail: function (e, data) {
            $.each(data.files, function (index, file) {
                var id = 'file-uploading-' + data.files[0].lastModified;
                $('#' + id).addClass('ks-file-uploading-error');
            });
        }
    });

    $(document).on('click', '#ks-widget-attachments-images-only .ks-files .ks-remove', function () {
        var file = $(this).closest('li');

        $.confirm({
            title: 'Danger!',
            content: 'Are you sure you want to remove this file?',
            type: 'danger',
            buttons: {
                confirm: {
                    text: 'Yes, remove',
                    btnClass: 'btn-danger',
                    action: function() {
                        file.remove();
                    }
                },
                cancel: function () {}
            }
        });
    });
});
</script>
@endsection
