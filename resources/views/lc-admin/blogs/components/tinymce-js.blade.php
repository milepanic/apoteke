<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=dr27a9q8cnbb7bnqgugx91bv9oedi3fkfcrsvg4co3314o39"></script>
<script>
tinymce.init({ 
    selector: 'textarea',
    plugins: 'a11ychecker advcode autosave autoresize advlist formatpainter fullscreen image linkchecker preview pageembed permanentpen powerpaste visualblocks wordcount',
    toolbar: 'a11ycheck code formatpainter fullscreen insertfile image pageembed preview permanentpen restoredraft visualblocks wordcount',
    autoresize_on_init: false,
    autoresize_overflow_padding: 50,
    advlist_bullet_styles: "square",
    automatic_uploads: true,
    file_picker_types: 'image',
    relative_urls: false,
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '{{ route('lc-admin.images.store', ['path' => $uniqueKey]) }}');
        xhr.setRequestHeader("X-CSRF-Token", "{{ csrf_token() }}");

        xhr.onload = function() {
            var json;

            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }

            json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }

            success(json.location);
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
    }
});
</script>
