<div class="ks-controls">
    <button class="btn btn-danger ks-light ks-no-text delete-role" data-id="{{ $role->id }}" data-name="{{ $role->name }}" style="line-height: 38px;">
        <span class="la la-trash-o ks-icon"></span>
    </button>
</div>
