<div class="ks-controls">
    <a href="{{ route('lc-admin.employees.edit', ['employees' => $employee]) }}">
        <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-pencil ks-icon"></span>
        </button>
    </a>
    <button class="btn btn-danger ks-light ks-no-text delete-employee" data-id="{{ $employee->id }}" data-name="{{ $employee->first_name . ' ' . $employee->last_name }}" style="line-height: 38px;">
        <span class="la la-trash-o ks-icon"></span>
    </button>
</div>
