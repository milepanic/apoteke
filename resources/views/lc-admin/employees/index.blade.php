@extends('lc-admin.layouts.master')
@section('title', __('Employees'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Employees') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.employees') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-body-wrap-container">
                <div class="ks-full-table">
                    <div class="ks-full-table-header">
                        <h4 class="ks-full-table-name">{{ __('All employees')}}</h4>
                        <div class="ks-controls">
                            <a href="{{ route('lc-admin.employees.create') }}">
                                <button class="btn btn-primary">{{ __('New Employee')}}</button>
                            </a>
                        </div>
                    </div>
                    <table id="employees-table" class="table ks-table-info dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="1">ID </th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Image') }}</th>
                                <th>{{ __('Pharmacies') }}</th>
                                {{-- <th>{{ __('Description') }}</th> --}}
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#employees-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax:"/lc-admin/employees", 
            columns: [
                { data: "id" }, 
                { 
                    data: "name",
                    render: function (data, type, row, meta) {
                        return "<a href='employees/" + row.id + "/edit'>" + row.first_name + ' ' + row.last_name + "</a>";
                    }
                }, 
                { data: "image" }, 
                { 
                    data: "pharmacies",
                    searchable: false,
                    orderable: false,
                }, 
                // { data: "description" }, 
                { data: "action" }
            ],
            columnDefs: [
                {
                    targets: 3,
                    render: function (data) {
                        pharmacies = [];
                        $.each(data, function (i, pharmacy) {
                            pharmacies.push("<a href='pharmacies/" + pharmacy.slug + "/edit'>" + pharmacy.name + "</a>");
                        });

                        return pharmacies.join(', ');
                    }
                }
            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.delete-employee', 'click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        var name = $(this).data('name');
        var table = $("#employees-table").DataTable();
        var tr = $(this).parents('tr');

        $.confirm({
            title: 'Pažnja!',
            content: 'Da li želite da obrišete ' + name + '?',
            type: 'danger',
            buttons: {
                confirm: {
                    text: 'OK',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                            url: '/lc-admin/employees/' + id,
                            type: 'DELETE',
                        })
                        .done(function(data) {
                            table.row(tr).remove().draw();
                            ajaxNotify(data);
                        })
                        .fail(function() {
                            alert('Dogodila se greška');
                        });
                    }
                },
                cancel: function () {

                }
            }
        });
    });
});

</script>
@endsection
