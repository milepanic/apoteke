@extends('lc-admin.layouts.master')
@section('title', __('New Employee'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Employee') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.employees.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New Employee') }}</h5>
                                    <form action="{{ route('lc-admin.employees.store') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="first-name-input" class="col-sm-3 form-control-label">{{ __('First name') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="first_name"
                                                    class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}"
                                                    id="first-name-input"
                                                    placeholder="{{ __('Employee first name') }}"
                                                    value="{{ old('first_name') }}"
                                                >
                                                @if($errors->has('first_name'))
                                                    <small class="text-danger">{{ $errors->first('first_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="last-name-input" class="col-sm-3 form-control-label">{{ __('Last name') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="last_name"
                                                    class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}"
                                                    id="last-name-input"
                                                    placeholder="{{ __('Employee last name') }}"
                                                    value="{{ old('last_name') }}"
                                                >
                                                @if($errors->has('last_name'))
                                                    <small class="text-danger">{{ $errors->first('last_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div>
                                            <div class="preview-div col-md-9 mb-3 ml-auto" hidden>
                                                <img class="preview-image img-fluid" src="#">
                                            </div>
                                            <div class="form-group row">
                                                <label for="image-input" class="col-sm-3 form-control-label">{{ __('Image') }}</label>
                                                <div class="col-sm-9">
                                                    <label class="btn btn-primary ks-btn-file">
                                                        <span class="la la-cloud-upload ks-icon"></span>
                                                        <span class="ks-text">{{ __('Choose file') }}</span>
                                                        <input
                                                            class="preview-input"
                                                            type="file"
                                                            name="image"
                                                        >
                                                    </label>
                                                    @if($errors->has('image'))
                                                        <small class="text-danger">{{ $errors->first('image') }}</small>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="description-input" class="col-sm-3 form-control-label">{{ __('Description') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="description"
                                                    id="description-input"
                                                    class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                                    placeholder="{{ __('Description') }}"
                                                >{{ old('description') }}</textarea>
                                                @if($errors->has('description'))
                                                    <small class="text-danger">{{ $errors->first('description') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="pharmacies-select" class="col-sm-3 form-control-label">{{ __('Pharmacies') }}</label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control multiselect2-pharmacies {{ $errors->has('pharmacies') ? 'is-invalid' : '' }}"
                                                    name="pharmacies[]"
                                                    id="pharmacies-select"
                                                    multiple
                                                >
                                                    @foreach($pharmacies as $pharmacy)
                                                        <option value="{{ $pharmacy->id }}">
                                                            {{ $pharmacy->name . ' - ' . $pharmacy->address . ', ' . $pharmacy->location->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('pharmacies'))
                                                    <small class="text-danger">{{ $errors->first('pharmacies') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Create') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function() {
    $('.multiselect2-pharmacies').select2({
        placeholder: '{{ __('Select pharmacies') }}'
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.preview-image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".preview-input").change(function() {
        $('.preview-div').attr('hidden', false);
        readURL(this);
    });
});
</script>
@endsection
