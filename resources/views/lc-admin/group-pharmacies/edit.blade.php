@extends('lc-admin.layouts.master')
@section('title', __('Add Pharmacy'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Add Pharmacy') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.groups.edit', $group) }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('Add Pharmacies') }}</h5>
                                    <form action="{{ route('lc-admin.group-pharmacies.update', ['group' => $group]) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group row">
                                            <label for="pharmacy-input" class="col-sm-3 form-control-label">{{ __('Pharmacies') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <select class="form-control multiselect2-pharmacies" name="pharmacies[]" id="pharmacy-input" multiple>
                                                    @foreach($pharmacies as $pharmacy)
                                                        @if(! $group->pharmacies->contains($pharmacy))
                                                            <option value="{{ $pharmacy->id }}">{{ $pharmacy->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Add') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function() {
    $('.multiselect2-pharmacies').select2({
        placeholder: '{{ __('Select pharmacies') }}'
    });
});
</script>
@endsection
