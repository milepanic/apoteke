<div class="ks-controls">
    <button class="btn btn-danger ks-light ks-no-text delete-pharmacy" data-id="{{ $pharmacy->id }}" data-name="{{ $pharmacy->name }}" data-group="{{ $group->id }}" style="line-height: 38px;">
        <span class="la la-trash-o ks-icon"></span>
    </button>
</div>
