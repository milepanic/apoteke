    <!-- BEGIN HEADER -->
<nav class="navbar ks-navbar">
    <!-- BEGIN HEADER INNER -->
    <!-- BEGIN LOGO -->
    <div href="index.html" class="navbar-brand">
        <!-- BEGIN RESPONSIVE SIDEBAR TOGGLER -->
        <a href="#" class="ks-sidebar-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
        <a href="#" class="ks-sidebar-mobile-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
        <!-- END RESPONSIVE SIDEBAR TOGGLER -->

        <div class="ks-navbar-logo">
            <a href="{{ route('lc-admin')}} " class="ks-logo">APOTEKE.BA</a>
        </div>
    </div>
    <!-- END LOGO -->

    <!-- BEGIN MENUS -->
    <div class="ks-wrapper">
        <nav class="nav navbar-nav">
            <!-- BEGIN NAVBAR MENU -->
            <div class="ks-navbar-menu">
                <a class="nav-item nav-link" href="#">BRZI LINK</a>
                <a class="nav-item nav-link" href="#">BRZI LINK</a>
                <a class="nav-item nav-link" href="#">BRZI LINK</a>
            </div>
            <!-- END NAVBAR MENU -->

            <!-- BEGIN NAVBAR ACTIONS -->
            <div class="ks-navbar-actions">
                <!-- BEGIN NAVBAR ACTION BUTTON -->
                <!--
                <div class="nav-item nav-link btn-action-block">
                    <a class="btn btn-danger" href="#">
                        <span class="ks-action">Activate Your Account</span>
                        <span class="ks-description">trial ends in 1 day</span>
                    </a>
                </div>
            -->
                <!-- END NAVBAR ACTION BUTTON -->

                <!-- BEGIN NAVBAR LANGUAGES -->
                <div class="nav-item dropdown ks-languages">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        {{ strtoupper(\App::getLocale() ?? 'EN') }} <span class="ks-text">Languages</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right ks-scrollable" aria-labelledby="Preview">
                        <div class="ks-wrapper">
                            @foreach(config('app.admin_locales') as $language)
                                <a href="{{ route('lc-admin.set-language', $language) }}" class="ks-language ks-selected">
                                    {{-- <span class="flag-icon flag-icon-{{$language}} ks-icon"></span> --}}
                                    <img class="flag-icon" src="{{ asset('theme/lc-admin/custom/flags/' . $language . '.png') }}" alt="">
                                    <span class="ks-text">{{ ucfirst(__($language)) }}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- END NAVBAR LANGUAGES -->

                <!-- BEGIN NAVBAR USER -->
                <div class="nav-item dropdown ks-user">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-avatar">
                            <img src="{{ asset('theme/lc-admin/assets/img/avatars/avatar-13.jpg') }}" width="36" height="36">
                        </span>
                        <span class="ks-info">
                            <span class="ks-name">{{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</span>
                            <span class="ks-description">neki opis</span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
                        <a class="dropdown-item" href="{{ route('lc-admin.users.show', ['id' => Auth::id()]) }}">
                            <span class="la la-user ks-icon"></span>
                            <span>{{ __('My Profile') }}</span>
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <span class="la la-sign-out ks-icon" aria-hidden="true"></span>
                            <span>{{__('Logout')}}</span>
                            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;"> @csrf </form>
                        </a>
                    </div>
                </div>
                <!-- END NAVBAR USER -->
            </div>
            <!-- END NAVBAR ACTIONS -->
        </nav>

        <!-- BEGIN NAVBAR ACTIONS TOGGLER -->
        <nav class="nav navbar-nav ks-navbar-actions-toggle">
            <a class="nav-item nav-link" href="#">
                <span class="la la-ellipsis-h ks-icon ks-open"></span>
                <span class="la la-close ks-icon ks-close"></span>
            </a>
        </nav>
        <!-- END NAVBAR ACTIONS TOGGLER -->

        <!-- BEGIN NAVBAR MENU TOGGLER -->
        <nav class="nav navbar-nav ks-navbar-menu-toggle">
            <a class="nav-item nav-link" href="#">
                <span class="la la-th ks-icon ks-open"></span>
                <span class="la la-close ks-icon ks-close"></span>
            </a>
        </nav>
        <!-- END NAVBAR MENU TOGGLER -->
    </div>
    <!-- END MENUS -->
    <!-- END HEADER INNER -->
</nav>
<!-- END HEADER -->
