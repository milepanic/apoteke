    <!-- BEGIN DEFAULT SIDEBAR -->
<div class="ks-column ks-sidebar ks-info">
    <div class="ks-wrapper ks-sidebar-wrapper">
        <ul class="nav nav-pills nav-stacked">

            <li class="nav-item">
                <a class="nav-link"  href="{{ route('lc-admin.dashboard') }}" >
                    <span class="ks-icon la la-dashboard"></span>
                    <span>{{ __('Dashboard') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('lc-admin.groups.index') }}">
                    <span class="ks-icon la la-hospital-o"></span>
                    <span>{{ __('Groups') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('lc-admin.pharmacies.index') }}">
                    <span class="ks-icon la la la-plus-square"></span>
                    <span>{{ __('Pharmacies') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('lc-admin.employees.index') }}">
                    <span class="ks-icon la la la-users"></span>
                    <span>{{ __('Employees') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('lc-admin.job-board.index') }}">
                    <span class="ks-icon la la la-list-ul"></span>
                    <span>{{ __('Job Board') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('lc-admin.drugs.index') }}">
                    <span class="ks-icon la la-medkit"></span>
                    <span>{{ __('Products') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('lc-admin.sliders.index') }}">
                    <span class="ks-icon la la-medkit"></span>
                    <span>{{ __('Sliders') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('lc-admin.blogs.index') }}">
                    <span class="ks-icon la la-file-o"></span>
                    <span>{{ __('Blogs') }}</span>
                </a>
            </li>
            <li class="nav-item dropdown {{ strpos(Route::currentRouteName(), 'lc-admin.users') === 0 || strpos(Route::currentRouteName(), 'lc-admin.roles') === 0 || strpos(Route::currentRouteName(), 'lc-admin.permissions') === 0 ? 'open' : '' }}">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-users"></span>
                    <span>{{ __('Users') }}</span>
                </a>
                <div class="dropdown-menu ">
                    <a href="{{ route('lc-admin.users.index') }}" class="dropdown-item">{{ __('Users') }}</a>
                    <a href="{{ route('lc-admin.roles.index') }}" class="dropdown-item">{{ __('Roles') }}</a>
                    <a href="{{ route('lc-admin.permissions.index') }}" class="dropdown-item">{{ __('Premissions') }}</a>
                </div>
            </li>



            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-sliders"></span>
                    <span>{{ __('Tools & Settings') }}</span>
                </a>
                <div class="dropdown-menu">
                    <a href="{{ route('lc-admin.languages') }}" class="dropdown-item">{{ __('Languages')}}</a>
                    <a href="{{ route('lc-admin.activity-log') }}" class="dropdown-item">{{ __('Activity log') }}</a>
                </div>
            </li>
        </ul>
        <div class="ks-sidebar-extras-block">
            <div class="ks-sidebar-copyright">2018 &copy; Admin panel  by <a href="https://laracode.net" class="m-link">Laracode.net</a></div>
        </div>
    </div>
</div>
<!-- END DEFAULT SIDEBAR -->
