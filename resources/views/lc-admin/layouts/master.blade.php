<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield ('title', 'Administration') | Laracode Panel</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('external-meta')
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/fonts/line-awesome/css/line-awesome.min.css') }}">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/fonts/montserrat/styles.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/tether/css/tether.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/jscrollpane/jquery.jscrollpane.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/common.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/lc-admin/assets/select2.min.css') }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('theme/lc-admin/libs/jquery-confirm/jquery-confirm.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/lc-admin/libs/flatpickr/flatpickr.min.css') }}">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/themes/primary.min.css') }}">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/themes/sidebar-black.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/lc-admin/libs/notify/bootstrap-notify.min.css') }}">
    <!-- END THEME STYLES -->
    @yield('external-css')
</head>
<!-- END HEAD -->

<body class="ks-navbar-fixed ks-sidebar-default ks-sidebar-position-fixed ks-page-header-fixed ks-theme-primary ks-page-loading"> <!-- remove ks-page-header-fixed to unfix header -->

@include('lc-admin.layouts.header')






<div class="ks-page-container">

@include('lc-admin.layouts.sidebar')

@yield('content')
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('theme/lc-admin/libs/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/popper/popper.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/responsejs/response.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/loading-overlay/loadingoverlay.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/tether/js/tether.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jscrollpane/jquery.jscrollpane.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jscrollpane/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/flexibility/flexibility.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/velocity/velocity.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/jquery-confirm/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/flatpickr/flatpickr.min.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('theme/lc-admin/assets/scripts/common.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('theme/lc-admin/assets/scripts/select2.full.min.js') }}"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>

@include ('admin.layouts.notifications')

@yield('external-js')

<div class="ks-mobile-overlay"></div>

<!-- BEGIN SETTINGS BLOCK -->
<div class="ks-settings-slide-block">
    <a class="ks-settings-slide-control">
        <span class="ks-icon la la-cog"></span>
    </a>

    <div class="ks-header">
        <span class="ks-text">Layout Options</span>
        <a class="ks-settings-slide-close-control">
            <span class="ks-icon la la-close"></span>
        </a>
    </div>

    <ul class="ks-settings-list">
        <li>
            <span class="ks-text">Collapsed Sidebar</span>
            <label class="ks-checkbox-slider ks-on-off ks-primary ks-sidebar-checkbox-toggle">
                <input type="checkbox" value="1">
                <span class="ks-indicator"></span>
                <span class="ks-on">On</span>
                <span class="ks-off">Off</span>
            </label>
        </li>
        <li>
            <span class="ks-text">Fixed page header</span>
            <label class="ks-checkbox-slider ks-on-off ks-primary ks-page-header-checkbox-toggle">
                <input type="checkbox" value="0" checked>
                <span class="ks-indicator"></span>
                <span class="ks-on">On</span>
                <span class="ks-off">Off</span>
            </label>
        </li>
        <li>
            <span class="ks-text">Dark/Light Sidebar</span>
            <label class="ks-checkbox-slider ks-on-off ks-primary ks-sidebar-style-checkbox-toggle">
                <input type="checkbox" value="0" checked>
                <span class="ks-indicator"></span>
                <span class="ks-on">On</span>
                <span class="ks-off">Off</span>
            </label>
        </li>
        <li>
            <span class="ks-text">White/Gray Content Background</span>
            <label class="ks-checkbox-slider ks-on-off ks-primary ks-content-bg-checkbox-toggle">
                <input type="checkbox" value="0" checked>
                <span class="ks-indicator"></span>
                <span class="ks-on">On</span>
                <span class="ks-off">Off</span>
            </label>
        </li>
    </ul>
</div>
<!-- END SETTINGS BLOCK -->
</body>
</html>
