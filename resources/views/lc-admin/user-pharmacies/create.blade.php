@extends('lc-admin.layouts.master')
@section('title', __('Add Pharmacy'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Add Pharmacy') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.user-pharmacies.create') }}

                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('Add Pharmacy to ' . $user->first_name . ' ' . $user->last_name) }}</h5>
                                    <form action="{{ route('lc-admin.user-pharmacies.update', ['user' => $user->id]) }}" method="POST">
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group row">
                                            <label for="pharmacy-select" class="col-sm-2 form-control-label">{{ __('Adress') }}</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="pharmacyId" id="pharmacy-select" multiple>
                                                    @foreach($pharmacies as $pharmacy)
                                                        <option value="{{ $pharmacy->id }}">
                                                            {{ $pharmacy->group->name . ', ' . $pharmacy->address . ', ' . $pharmacy->location->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
