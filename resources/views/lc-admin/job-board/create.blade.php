@extends('lc-admin.layouts.master')
@section('title', __('New Job'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Job') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.job-board.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New Job') }}</h5>
                                    <form 
                                        action="{{ route('lc-admin.job-board.store') }}" 
                                        method="POST" 
                                        enctype="multipart/form-data"
                                    >
                                        @csrf
                                        <div class="form-group row">
                                            <label for="title-input" class="col-sm-3 form-control-label">{{ __('Title') }}</label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="title"
                                                    class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                                    id="title-input"
                                                    placeholder="{{ __('Job title') }}"
                                                >{{ old('title') }}</textarea>
                                                @if($errors->has('title'))
                                                    <small class="text-danger">{{ $errors->first('title') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="content-input" class="col-sm-3 form-control-label">{{ __('Content') }}</label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="content"
                                                    class="form-control {{ $errors->has('content') ? 'is-invalid' : '' }}"
                                                    id="content-input"
                                                    placeholder="{{ __('Job content') }}"
                                                    rows="10"
                                                >{{ old('content') }}</textarea>
                                                @if($errors->has('content'))
                                                    <small class="text-danger">{{ $errors->first('content') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="position-input" class="col-sm-3 form-control-label">{{ __('Position') }}</label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="position"
                                                    class="form-control {{ $errors->has('position') ? 'is-invalid' : '' }}"
                                                    id="position-input"
                                                    placeholder="{{ __('Job position') }}"
                                                >{{ old('position') }}</textarea>
                                                @if($errors->has('position'))
                                                    <small class="text-danger">{{ $errors->first('position') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="position-input" class="col-sm-3 form-control-label">{{ __('Executors Count') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="number"
                                                    min="0"
                                                    name="executor_count"
                                                    class="form-control {{ $errors->has('executor_count') ? 'is-invalid' : '' }}"
                                                    id="executor-count-input"
                                                    value="{{ old('executor_count', 1) }}"
                                                >
                                                @if($errors->has('executor_count'))
                                                    <small class="text-danger">{{ $errors->first('executor_count') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="location-input" class="col-sm-3 form-control-label">
                                                {{ __('Location') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control {{ $errors->has('location_id') ? 'is-invalid' : '' }}"
                                                    name="location_id"
                                                    id="location-input"
                                                >
                                                    <option disabled selected>{{ __('Select location') }}</option>
                                                    @foreach ($locations as $location)
                                                        <option value="{{ $location->id }}">{{ $location->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('location_id'))
                                                    <small class="text-danger">{{ $errors->first('location_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="group-input" class="col-sm-3 form-control-label">
                                                {{ __('Group') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control"
                                                    name="group_id"
                                                    id="group-input"
                                                >
                                                    @foreach ($groups as $group)
                                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('groups'))
                                                    <small class="text-danger">{{ $errors->first('groups') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @if(Auth::user()->hasRole('super admin'))
                                            <div class="form-group row">
                                                <label for="user-input" class="col-sm-3 form-control-label">
                                                    {{ __('User') }}
                                                </label>
                                                <div class="col-sm-9">
                                                    <select
                                                        class="form-control {{ $errors->has('users') ? 'is-invalid' : '' }}"
                                                        name="user_id"
                                                        id="user-input"
                                                    >
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->id }}">{{ $user->first_name . ' ' . $user->last_name . ', ' . $user->email }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('users'))
                                                        <small class="text-danger">{{ $errors->first('users') }}</small>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif

                                        <button class="float-right btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function() {
    $('.multiselect2-groups').select2({
        placeholder: '{{ __('Select groups') }}'
    });

    $('.multiselect2-users').select2({
        placeholder: '{{ __('Select users') }}'
    });
});
</script>
@endsection
