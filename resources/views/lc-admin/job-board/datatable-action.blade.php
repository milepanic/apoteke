<div class="ks-controls">
    <a href="{{ route('lc-admin.job-board.edit', ['job_board' => $jobBoard]) }}">
        <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-pencil ks-icon"></span>
        </button>
    </a>
    <button class="btn btn-danger ks-light ks-no-text delete-job" data-id="{{ $jobBoard->id }}" data-title="{{ $jobBoard->title }}" style="line-height: 38px;">
        <span class="la la-trash-o ks-icon"></span>
    </button>
</div>
