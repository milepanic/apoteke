@extends('lc-admin.layouts.master')
@section('title', __('Edit Job'))
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Edit Job') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.job-board.edit', $jobBoard) }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('Edit Job') }}</h5>
                                    <form 
                                        id="form-edit-job" 
                                        action="{{ route('lc-admin.job-board.update', ['job_board' => $jobBoard]) }}" 
                                        method="POST" 
                                    >
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group row">
                                            <label for="title-input" class="col-sm-3 form-control-label">{{ __('Title') }}</label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="title"
                                                    class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                                    id="title-input"
                                                    placeholder="{{ __('Job title') }}"
                                                >{{ old('title', $jobBoard->title) }}</textarea>
                                                @if($errors->has('title'))
                                                    <small class="text-danger">{{ $errors->first('title') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="content-input" class="col-sm-3 form-control-label">{{ __('Content') }}</label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="content"
                                                    class="form-control {{ $errors->has('content') ? 'is-invalid' : '' }}"
                                                    id="content-input"
                                                    placeholder="{{ __('Job content') }}"
                                                    rows="10"
                                                >{{ old('content', $jobBoard->content) }}</textarea>
                                                @if($errors->has('content'))
                                                    <small class="text-danger">{{ $errors->first('content') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="position-input" class="col-sm-3 form-control-label">{{ __('Position') }}</label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="position"
                                                    class="form-control {{ $errors->has('position') ? 'is-invalid' : '' }}"
                                                    id="position-input"
                                                    placeholder="{{ __('Job position') }}"
                                                >{{ old('position', $jobBoard->position) }}</textarea>
                                                @if($errors->has('position'))
                                                    <small class="text-danger">{{ $errors->first('position') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="position-input" class="col-sm-3 form-control-label">{{ __('Executors Count') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="number"
                                                    min="0"
                                                    name="executor_count"
                                                    class="form-control {{ $errors->has('executor_count') ? 'is-invalid' : '' }}"
                                                    id="executor-count-input"
                                                    value="{{ old('executor_count', $jobBoard->executor_count) }}"
                                                >
                                                @if($errors->has('executor_count'))
                                                    <small class="text-danger">{{ $errors->first('executor_count') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="location-input" class="col-sm-3 form-control-label">
                                                {{ __('Location') }}<span class="text-danger"> *</span>
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control {{ $errors->has('location_id') ? 'is-invalid' : '' }}"
                                                    name="location_id"
                                                    id="location-input"
                                                >
                                                    <option disabled selected>{{ __('Select location') }}</option>
                                                    @foreach ($locations as $location)
                                                        <option 
                                                            value="{{ $location->id }}"
                                                            @if($jobBoard->location == $location) selected @endif 
                                                        >{{ $location->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('location_id'))
                                                    <small class="text-danger">{{ $errors->first('location_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="group-input" class="col-sm-3 form-control-label">
                                                {{ __('Group') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control"
                                                    name="group_id"
                                                    id="group-input"
                                                >
                                                    @foreach ($groups as $group)
                                                        <option 
                                                            value="{{ $group->id }}"
                                                            @if($jobBoard->group == $group) selected @endif 
                                                        >{{ $group->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('groups'))
                                                    <small class="text-danger">{{ $errors->first('groups') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @if(Auth::user()->hasRole('super admin'))
                                            <div class="form-group row">
                                                <label for="user-input" class="col-sm-3 form-control-label">
                                                    {{ __('User') }}
                                                </label>
                                                <div class="col-sm-9">
                                                    <select
                                                        class="form-control {{ $errors->has('users') ? 'is-invalid' : '' }}"
                                                        name="user_id"
                                                        id="user-input"
                                                    >
                                                        @foreach($users as $user)
                                                            <option 
                                                                value="{{ $user->id }}"
                                                                @if($jobBoard->user == $user) selected @endif 
                                                            >{{ $user->first_name . ' ' . $user->last_name . ', ' . $user->email }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('users'))
                                                        <small class="text-danger">{{ $errors->first('users') }}</small>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                        <button class="float-right btn btn-primary btn-edit-job">{{ __('Edit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="application/javascript">
jQuery(document).ready(function () {
    $('.multiselect2-users').select2({
        placeholder: '{{ __('Select users') }}'
    });

    $('.btn-edit-job').on('click', function (e) {
        e.preventDefault();

        $.confirm({
            title: 'Pažnja!',
            content: 'Da li želite da sačuvate izmjene?',
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'OK',
                    btnClass: 'btn-warning',
                    action: function () {
                        $('#form-edit-job').submit();
                    }
                },
                cancel: function () {

                }
            }
        });
    });
});

</script>
@endsection
