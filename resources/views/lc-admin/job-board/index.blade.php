@extends('lc-admin.layouts.master')
@section('title', __('Job Board'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Job Board') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.job-board') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-body-wrap-container">
                <div class="ks-full-table">
                    <div class="ks-full-table-header">
                        <h4 class="ks-full-table-name">{{ __('All Jobs')}}</h4>
                        <div class="ks-controls">
                            <a href="{{ route('lc-admin.job-board.create') }}">
                                <button class="btn btn-primary">{{ __('New Job')}}</button>
                            </a>
                        </div>
                    </div>
                    <table id="job-board-table" class="table ks-table-info dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="1">ID </th>
                                <th>{{ __('Title') }}</th>
                                <th>{{ __('User') }}</th>
                                <th>{{ __('Group') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#job-board-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax:"/lc-admin/job-board", 
            columns: [
                { data: "id" }, 
                { 
                    data: "title",
                    render: function (data, type, row, meta) {
                        return "<a href='job-board/" + row.id + "/edit'>" + data + "</a>";
                    }
                },
                { data: "user.email" },
                { data: "group.name" },
                { data: "action" }
            ],
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.delete-job', 'click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        var name = $(this).data('title');
        var table = $("#job-board-table").DataTable();
        var tr = $(this).parents('tr');

        $.confirm({
            title: 'Pažnja!',
            content: 'Da li želite da obrišete ' + name + '?',
            type: 'danger',
            buttons: {
                confirm: {
                    text: 'OK',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                            url: '/lc-admin/job-board/' + id,
                            type: 'DELETE',
                        })
                        .done(function(data) {
                            table.row(tr).remove().draw();
                            ajaxNotify(data);
                        })
                        .fail(function() {
                            alert('Dogodila se greška');
                        });
                    }
                },
                cancel: function () {

                }
            }
        });
    });
});

</script>
@endsection
