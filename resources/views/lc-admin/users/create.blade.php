@extends('lc-admin.layouts.master')
@section('title', __('New User'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New User') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.users.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New User') }}</h5>
                                    <form action="{{ route('lc-admin.users.store') }}" method="POST">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="first-name-input" class="col-sm-3 form-control-label">{{ __('First name') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="first_name"
                                                    class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}"
                                                    id="first-name-input"
                                                    placeholder="{{ __('User first name') }}"
                                                    value="{{ old('first_name') }}"
                                                >
                                                @if($errors->has('first_name'))
                                                    <small class="text-danger">{{ $errors->first('first_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="last-name-input" class="col-sm-3 form-control-label">{{ __('Last name') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="last_name"
                                                    class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}"
                                                    id="last-name-input"
                                                    placeholder="{{ __('User last name') }}"
                                                    value="{{ old('last_name') }}"
                                                >
                                                @if($errors->has('last_name'))
                                                    <small class="text-danger">{{ $errors->first('last_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="contact-input" class="col-sm-3 form-control-label">{{ __('Contact') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="contact"
                                                    class="form-control {{ $errors->has('contact') ? 'is-invalid' : '' }}"
                                                    id="contact-input"
                                                    placeholder="{{ __('User contact') }}"
                                                    value="{{ old('contact') }}"
                                                >
                                                @if($errors->has('contact'))
                                                    <small class="text-danger">{{ $errors->first('contact') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email-input" class="col-sm-3 form-control-label">{{ __('Email') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="email"
                                                    name="email"
                                                    class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                                    id="email-input"
                                                    placeholder="{{ __('User email') }}"
                                                    value="{{ old('email') }}"
                                                >
                                                @if($errors->has('email'))
                                                    <small class="text-danger">{{ $errors->first('email') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="password-input" class="col-sm-3 form-control-label">{{ __('Password') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="password"
                                                    name="password"
                                                    class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                                    id="password-input"
                                                    placeholder="{{ __('User password') }}"
                                                >
                                                @if($errors->has('password'))
                                                    <small class="text-danger">{{ $errors->first('password') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="role-select" class="col-sm-3 form-control-label">{{ __('Role') }}</label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control multiselect2-roles {{ $errors->has('roles') ? 'is-invalid' : '' }}"
                                                    name="roles[]"
                                                    id="role-select"
                                                    multiple
                                                >
                                                    @foreach($roles as $role)
                                                        <option value="{{ $role->id }}">
                                                            {{ $role->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('roles'))
                                                    <small class="text-danger">{{ $errors->first('roles') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="permission-select" class="col-sm-3 form-control-label">{{ __('Permissions') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control multiselect2-permissions {{ $errors->has('permissions') ? 'is-invalid' : '' }}"
                                                    name="permissions[]"
                                                    id="permission-select"
                                                    multiple
                                                >
                                                    @foreach($permissions as $permission)
                                                        <option value="{{ $permission->id }}">
                                                            {{ $permission->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('permissions'))
                                                    <small class="text-danger">{{ $errors->first('permissions') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Register') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function () {
    $('.multiselect2-roles').select2({
        placeholder: '{{ __('Select roles') }}'
    });

    $('.multiselect2-permissions').select2({
        placeholder: '{{ __('Special permissions') }}'
    });
});
</script>
@endsection
