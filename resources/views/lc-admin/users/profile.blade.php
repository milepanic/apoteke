@extends('lc-admin.layouts.master')
@section('title', __('Profile Page'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/fonts/line-awesome/css/line-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/widgets/tables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/profile/customer.min.css') }}">
@endsection
@section('content')

<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('User Profile') }}</h3>
        </section>
    </div>
    <div class="ks-page-content">
            <div class="ks-page-content-body ks-profile">
                <div class="ks-header">
                    <div class="ks-user">
                        <img src="assets/img/avatars/ava-1.png" class="ks-avatar" width="100" height="100">
                        <div class="ks-info">
                            <div class="ks-name">{{ $user->first_name . ' ' . $user->last_name }}</div>
                            <div class="ks-description">New York, USA</div>
                            <div class="ks-rating">
                                <i class="la la-star ks-star" aria-hidden="true"></i>
                                <i class="la la-star ks-star" aria-hidden="true"></i>
                                <i class="la la-star ks-star" aria-hidden="true"></i>
                                <i class="la la-star ks-star" aria-hidden="true"></i>
                                <i class="la la-star-half-o ks-star" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="ks-statistics">
                        <div class="ks-item">
                            <div class="ks-amount">869</div>
                            <div class="ks-text">orders</div>
                        </div>
                        <div class="ks-item">
                            <div class="ks-amount">131</div>
                            <div class="ks-text">reviews</div>
                        </div>
                        <div class="ks-item">
                            <div class="ks-amount">$3,004</div>
                            <div class="ks-text">rewand points</div>
                        </div>
                    </div>
                </div>
                <div class="ks-tabs-container ks-tabs-default ks-tabs-no-separator ks-full ks-light">
                    <ul class="nav ks-nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#" data-toggle="tab" data-target="#overview" aria-expanded="true">Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="tab" data-target="#contacts" aria-expanded="false">Contacts</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="tab" data-target="#orders" aria-expanded="false">Orders</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="tab" data-target="#wish-list" aria-expanded="false">Wish list</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="tab" data-target="#storecredit" aria-expanded="false">Store credit</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="tab" data-target="#returns" aria-expanded="false">Returns</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="tab" data-target="#reward-points" aria-expanded="false">Reward points</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="tab" data-target="#settings" aria-expanded="false">Settings</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="false">
                            <div class="ks-overview-tab">
                                <div class="row">

                                    <div class="col-xl-9 ks-tables-container">
                                        <div class="card panel panel-default ks-information ks-light">
                                            <h5 class="card-header">
                                                <span class="ks-text">{{ __('Pharmacies') }}</span>
                                                {{-- @if(! $user->ownedPharmacies->isEmpty()) --}}
                                                    {{-- TODO: Poslati grupu na stranicu kreiranja apoteke --}}
                                                    {{-- <a href="{{ route('lc-admin.pharmacies.create') }}" class="btn btn-outline-primary ks-light">{{ __('Add New Pharmacy') }}</a> --}}
                                                {{-- @else --}}
                                                    {{-- <a href="{{ route('lc-admin.user-pharmacies.edit', ['user' => $user->id]) }}" class="btn btn-outline-primary ks-light">{{ __('Attach Pharmacy') }}</a> --}}
                                                {{-- @endif --}}
                                            </h5>
                                            <div class="card-block ks-datatable">
                                                <table id="pharmacies-table" class="table table-bordered" style="width:100%" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>{{ __('ID') }}</th>
                                                        <th>{{ __('Name') }}</th>
                                                        <th>{{ __('Adress') }}</th>
                                                        <th>{{ __('Contact') }}</th>
                                                        <th>{{ __('Working Time') }}</th>
                                                        <th>{{ __('Groups') }}</th>
                                                        <th>{{ __('Location') }}</th>
                                                        <th>{{ __('Action') }}</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-3">
                                        <div class="card panel panel-default ks-information ks-light">
                                            <h5 class="card-header">
                                                <span class="ks-text">{{ __('Contact') }}</span>
                                                <a href="#" class="btn btn-outline-primary ks-light ks-no-text"><span class="la la-pencil ks-icon"></span></a>
                                            </h5>
                                            <div class="card-block">
                                                <table class="ks-table-description">
                                                    <tr>
                                                        <td class="ks-icon">
                                                            <span class="la la-map-marker"></span>
                                                        </td>
                                                        <td class="ks-text">
                                                            Ulica Srpske Vojske 111
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ks-icon ks-fs-16">
                                                            <span class="la la-phone"></span>
                                                        </td>
                                                        <td class="ks-text">
                                                            055 478 471
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ks-icon">
                                                            <span class="la la-mobile-phone"></span>
                                                        </td>
                                                        <td class="ks-text">
                                                            065 164 350
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ks-icon ks-fs-14">
                                                            <span class="la la-envelope"></span>
                                                        </td>
                                                        <td class="ks-text">
                                                            {{ $user->email }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="card panel panel-default ks-information ks-light">
                                            <h5 class="card-header">
                                                <span class="ks-text">{{ __('Group info') }}</span>
                                                <a href="#" class="btn btn-outline-primary ks-light ks-no-text"><span class="la la-pencil ks-icon"></span></a>
                                            </h5>
                                            <div class="card-block">
                                                <table class="ks-table-description">
                                                    <tr>
                                                        <td class="ks-icon">
                                                            <span class="la la-hospital-o"></span>
                                                        </td>
                                                        <td class="ks-text">
                                                            3 Apoteke
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ks-icon ks-fs-16">
                                                            <span class="la la-users"></span>
                                                        </td>
                                                        <td class="ks-text">
                                                            4 Zaposlena
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ks-icon ks-fs-14">
                                                            <span class="la la-info-circle"></span>
                                                        </td>
                                                        <td class="ks-text">
                                                            Opis
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="contacts" role="tabpanel" aria-expanded="false">
                            Content 2
                        </div>
                        <div class="tab-pane" id="orders" role="tabpanel" aria-expanded="true">
                            Content 3
                        </div>
                        <div class="tab-pane" id="wish-list" role="tabpanel" aria-expanded="false">
                            Content 1
                        </div>
                        <div class="tab-pane" id="storecredit" role="tabpanel" aria-expanded="false">
                            Content 2
                        </div>
                        <div class="tab-pane" id="returns" role="tabpanel" aria-expanded="true">
                            Content 3
                        </div>
                        <div class="tab-pane" id="reward-points" role="tabpanel" aria-expanded="false">
                            Content 1
                        </div>
                        <div class="tab-pane" id="settings" role="tabpanel" aria-expanded="false">
                            Content 1
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection

@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">
    var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#pharmacies-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax: "/lc-admin/users/1", // FIX: ispraviti id 
            columns: [
                { data: "id" }, 
                { data: "name" },
                { data: "address" }, 
                { data: "contact" }, 
                { data: "working_time" }, 
                { 
                    data: "groups",
                    orderable: false,
                    searchable: false,
                }, 
                { data: "location.name" },
                { data: "action" },
            ],
            columnDefs: [
                {
                    targets: 5,
                    render: function (data) {
                        groups = [];
                        $.each(data, function (i, group) {
                            groups.push(group.name);
                        });

                        return groups.join(', ');
                    }
                }
            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();
});

</script>
@endsection
