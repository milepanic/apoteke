<div class="ks-controls">
    <a href="{{ route('lc-admin.users.edit', ['users' => $user]) }}">
        <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-pencil ks-icon"></span>
        </button>
    </a>
    <button class="btn btn-danger ks-light ks-no-text delete-user" data-id="{{ $user->id }}" data-name="{{ $user->first_name . ' ' . $user->last_name }}" style="line-height: 38px;">
        <span class="la la-trash-o ks-icon"></span>
    </button>
</div>
