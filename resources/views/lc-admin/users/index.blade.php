@extends('lc-admin.layouts.master')
@section('title', __('Users'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Users') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.users') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-body-wrap-container">
                <div class="ks-full-table">
                    <div class="ks-full-table-header">
                        <h4 class="ks-full-table-name">{{ __('All users')}}</h4>
                        <div class="ks-controls">
                            <a href="{{ route('lc-admin.users.create') }}">
                                <button class="btn btn-primary">{{ __('New user')}}</button>
                            </a>
                        </div>
                    </div>
                    <table id="users-table" class="table ks-table-info dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="1">ID </th>
                                <th>{{ __('First name') }}</th>
                                <th>{{ __('Last name') }}</th>
                                <th>{{ __('Contact') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Roles') }}</th>
                                <th>{{ __('Special Permissions') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#users-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax:"/lc-admin/users", 
            columns: [
            { data: "id" }, 
            { data: "first_name" }, 
            { data: "last_name" }, 
            { data: "contact" }, 
            { data: "email" }, 
            { 
                data: "roles", 
                searchable: false,
                orderable: false
            }, 
            { 
                data: "permissions", 
                searchable: false,
                orderable: false
            },
            { data: "action" }
            ],
            columnDefs:[
            {
                targets: -1, 
                orderable: false,
                searchable: false,
                autoWidth: false,
                responsivePriority: -1,
            },
            {
                targets: 6,
                render: function (data) {
                    permissions = [];
                    $.each(data, function (i, permission) {
                        permissions.push(permission.name);
                    });

                    return permissions.join(', ');
                }
            }, 
            {
                targets: 5,
                title: "Roles",
                render: function (data) {
                    roles = [];
                    $.each(data, function (i, role) {
                        roles.push(role.name);
                    });

                    return roles.join(', ');
                }
            }],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.delete-user', 'click', function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var id = $(this).data('id');
        var name = $(this).data('name');
        var table = $("#users-table").DataTable();
        var tr = $(this).parents('tr');

        $.confirm({
            title: 'Pažnja!',
            content: 'Da li želite da obrišete korisnika ' + name + '?',
            type: 'danger',
            buttons: {
                confirm: {
                    text: 'OK',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                            url: '/lc-admin/users/' + id,
                            type: 'DELETE',
                        })
                        .done(function(data) {
                            table.row(tr).remove().draw();
                            ajaxNotify(data);
                        })
                        .fail(function() {
                            alert('Dogodila se greška');
                        });
                    }
                },
                cancel: function () {

                }
            }
        });
    });
});

</script>
@endsection
