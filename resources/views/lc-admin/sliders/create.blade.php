@extends('lc-admin.layouts.master')
@section('title', __('New Slider'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Slider') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.sliders.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New Slider') }}</h5>
                                    <form action="{{ route('lc-admin.sliders.store') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="published-input" class="col-sm-3 form-control-label">{{ __('Published') }}</label>
                                            <div class="col-sm-9">
                                                <label class="ks-checkbox-slider ks-on-off ks-solid ks-primary">
                                                    <input
                                                        type="checkbox"
                                                        name="published"
                                                        id="published-input"
                                                        value="1"
                                                    >
                                                    <span class="ks-indicator"></span>
                                                    <span class="ks-on">On</span>
                                                    <span class="ks-off">Off</span>
                                            </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="type-select" class="col-sm-3 form-control-label">{{ __('Type/Location') }}</label>
                                            <div class="col-sm-9">
                                                <select
                                                    class="form-control multiselect2-types {{ $errors->has('types') ? 'is-invalid' : '' }}"
                                                    name="types[]"
                                                    id="type-select"
                                                    multiple
                                                >
                                                    <option value="1">Type 1</option>
                                                    <option value="2">Type 2</option>
                                                    <option value="3">Type 3</option>
                                                </select>
                                                @if($errors->has('types'))
                                                    <small class="text-danger">{{ $errors->first('types') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="priority-input" class="col-sm-3 form-control-label">{{ __('Priority') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="number"
                                                    name="priority"
                                                    class="form-control {{ $errors->has('priority') ? 'is-invalid' : '' }}"
                                                    id="priority-input"
                                                    min="1"
                                                    max="100"
                                                    value="1"
                                                >
                                                @if($errors->has('priority'))
                                                    <small class="text-danger">{{ $errors->first('priority') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div>
                                            <div class="preview-div col-md-9 mb-3 ml-auto" hidden>
                                                <img class="preview-image img-fluid" src="#">
                                            </div>
                                            <div class="form-group row">
                                                <label for="background-input" class="col-sm-3 form-control-label">{{ __('Background') }}<span class="text-danger"> *</span></label>
                                                <div class="col-sm-9">
                                                    <label class="btn btn-primary ks-btn-file">
                                                        <span class="la la-cloud-upload ks-icon"></span>
                                                        <span class="ks-text">{{ __('Choose file') }}</span>
                                                        <input class="preview-input" type="file" name="background">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">{{ __('Name') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="name"
                                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                    id="name-input"
                                                    placeholder="{{ __('Name') }}"
                                                >
                                                @if($errors->has('name'))
                                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="text-input" class="col-sm-3 form-control-label">{{ __('Text') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="text"
                                                    id="text-input"
                                                    class="form-control {{ $errors->has('text') ? 'is-invalid' : '' }}"
                                                    placeholder="{{ __('Text') }}"
                                                ></textarea>
                                                @if($errors->has('text'))
                                                    <small class="text-danger">{{ $errors->first('text') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="url-input" class="col-sm-3 form-control-label">{{ __('Url') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="url"
                                                    class="form-control {{ $errors->has('url') ? 'is-invalid' : '' }}"
                                                    id="url-input"
                                                    placeholder="{{ __('Url') }}"
                                                >
                                                @if($errors->has('url'))
                                                    <small class="text-danger">{{ $errors->first('url') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 form-control-label">{{ __('Button') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <div id="btn-forms">
                                                    <div class="form-inline">
                                                        <input
                                                            type="text"
                                                            name="button_names[]"
                                                            class="col-sm-6 form-control"
                                                            placeholder="{{ __('Button Name') }}"
                                                        >
                                                        <input
                                                            type="text"
                                                            name="button_styles[]"
                                                            class="col-sm-6 form-control"
                                                            placeholder="{{ __('Button Style') }}"
                                                        >
                                                    </div>
                                                    <div>
                                                        <input
                                                            type="text"
                                                            name="button_urls[]"
                                                            class="col-md-12 mb-2 form-control"
                                                            placeholder="{{ __('Button URL') }}"
                                                        >
                                                    </div>
                                                </div>
                                                <div class="form-inline" id="append-forms"></div>
                                                <button class="btn btn-primary pull-right col-sm-12 mt-2" id="add-new-group">+</button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="style-name-input" class="col-sm-3 form-control-label">{{ __('Style Name') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="style_name"
                                                    class="form-control {{ $errors->has('style_name') ? 'is-invalid' : '' }}"
                                                    id="style-name-input"
                                                    placeholder="{{ __('Style Name') }}"
                                                >
                                                @if($errors->has('style_name'))
                                                    <small class="text-danger">{{ $errors->first('style_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="style-value-input" class="col-sm-3 form-control-label">{{ __('Style Value') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="style_value"
                                                    class="form-control {{ $errors->has('style_value') ? 'is-invalid' : '' }}"
                                                    id="style-value-input"
                                                    placeholder="{{ __('Style Value') }}"
                                                >
                                                @if($errors->has('style_value'))
                                                    <small class="text-danger">{{ $errors->first('style_value') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function() {
    $('.multiselect2-types').select2({
        placeholder: '{{ __('Select Types') }}'
    });

    $('#add-new-group').on('click', function (e) {
        e.preventDefault();

        $('#btn-forms').children().children().clone().val('').appendTo('#append-forms');
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.preview-image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".preview-input").change(function() {
        $('.preview-div').attr('hidden', false);
        readURL(this);
    });
});
</script>
@endsection
