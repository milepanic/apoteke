@extends('lc-admin.layouts.master')
@section('title', __('Edit Slider'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Edit Slider') }}</h3>

            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.sliders.edit', $slider) }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('Edit Slider') }}</h5>
                                    <form id="form-edit-slider" action="{{ route('lc-admin.sliders.update', ['slider' => $slider->id]) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group row">
                                            <label for="published-input" class="col-sm-3 form-control-label">{{ __('Published') }}</label>
                                            <div class="col-sm-9">

                                                <label class="ks-checkbox-slider ks-on-off ks-solid ks-primary">
                                                    <input
                                                        type="checkbox"
                                                        name="published"
                                                        id="published-input"
                                                        value="1"
                                                        @if($slider->published) checked @endif
                                                    >
                                                    <span class="ks-indicator"></span>
                                                    <span class="ks-on">On</span>
                                                    <span class="ks-off">Off</span>
                                                }
                                            </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="type-select" class="col-sm-3 form-control-label">{{ __('Type/Location') }}</label>
                                            <div class="col-sm-9">
                                                <select
                                                    name="types[]"
                                                    class="form-control multiselect2-types {{ $errors->has('types') ? 'is-invalid' : '' }}"
                                                    id="type-select"
                                                    multiple
                                                >
                                                    <option 
                                                        value="1"
                                                        @if(in_array('1', json_decode($slider->types))) selected @endif 
                                                    >Type 1</option>
                                                    <option 
                                                        value="2"
                                                        @if(in_array('2', json_decode($slider->types))) selected @endif 
                                                    >Type 2</option>
                                                    <option 
                                                        value="3"
                                                        @if(in_array('3', json_decode($slider->types))) selected @endif 
                                                    >Type 3</option>
                                                </select>
                                                @if($errors->has('types'))
                                                    <small class="text-danger">{{ $errors->first('types') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="priority-input" class="col-sm-3 form-control-label">{{ __('Priority') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="number"
                                                    name="priority"
                                                    class="form-control {{ $errors->has('priority') ? 'is-invalid' : '' }}"
                                                    id="priority-input"
                                                    min="1"
                                                    max="100"
                                                    value="{{ old('priority', $slider->priority) }}"
                                                >
                                                @if($errors->has('priority'))
                                                    <small class="text-danger">{{ $errors->first('priority') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div>
                                            @if($slider->background)
                                                <div class="col-md-9 mb-3 ml-auto">
                                                    <img class="preview-image img-fluid" src="{{ file_path($slider->background) }}">
                                                </div>
                                            @endif
                                            <div class="form-group row">
                                                <label for="background-input" class="col-sm-3 form-control-label">{{ __('Background') }}<span class="text-danger"> *</span></label>
                                                <div class="col-sm-9">
                                                    <label class="btn btn-primary ks-btn-file">
                                                        <span class="la la-cloud-upload ks-icon"></span>
                                                        <span class="ks-text">{{ __('Choose file') }}</span>
                                                        <input
                                                            class="preview-input"
                                                            type="file"
                                                            name="background"
                                                        >
                                                    </label>
                                                    @if($errors->has('background'))
                                                        <small class="text-danger">{{ $errors->first('background') }}</small>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">{{ __('Name') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="name"
                                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                    id="name-input"
                                                    placeholder="{{ __('Name') }}"
                                                    value="{{ old('name', $slider->name) }}"
                                                >
                                                @if($errors->has('name'))
                                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="text-input" class="col-sm-3 form-control-label">{{ __('Text') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="text"
                                                    class="form-control {{ $errors->has('text') ? 'is-invalid' : '' }}"
                                                    id="text-input"
                                                    placeholder="{{ __('Text') }}"
                                                >{{ old('text', $slider->text) }}</textarea>
                                                @if($errors->has('text'))
                                                    <small class="text-danger">{{ $errors->first('text') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="url-input" class="col-sm-3 form-control-label">{{ __('Url') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="url"
                                                    class="form-control {{ $errors->has('url') ? 'is-invalid' : '' }}"
                                                    id="url-input"
                                                    placeholder="{{ __('Url') }}"
                                                    value="{{ old('url', $slider->url) }}"
                                                >
                                                @if($errors->has('url'))
                                                    <small class="text-danger">{{ $errors->first('url') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 form-control-label">{{ __('Button') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <div id="button-forms">
                                                    @foreach (json_decode($slider->buttons) as $button)
                                                        <div class="form-inline">
                                                            <input
                                                                type="text"
                                                                name="button_names[]"
                                                                class="col-sm-6 form-control"
                                                                placeholder="{{ __('Button Name') }}"
                                                                value="{{ $button->name }}"
                                                            >
                                                            <input
                                                                type="text"
                                                                name="button_styles[]"
                                                                class="col-sm-6 form-control"
                                                                placeholder="{{ __('Button Style') }}"
                                                                value="{{ $button->style }}"
                                                            >
                                                        </div>
                                                        <div>
                                                            <input
                                                                type="text"
                                                                name="button_urls[]"
                                                                class="col-md-12 mb-2 form-control"
                                                                placeholder="{{ __('Button URL') }}"
                                                                value="{{ $button->url }}"
                                                            >
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="form-inline" id="append-forms"></div>
                                                <button class="btn btn-primary pull-right col-sm-12 mt-2" id="add-new-group">+</button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="style-name-input" class="col-sm-3 form-control-label">{{ __('Style Name') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="style_name"
                                                    class="form-control {{ $errors->has('style_name') ? 'is-invalid' : '' }}"
                                                    id="style-name-input"
                                                    placeholder="{{ __('Style Name') }}"
                                                    value="{{ old('style_name', $slider->style_name) }}"
                                                >
                                                @if($errors->has('style_name'))
                                                    <small class="text-danger">{{ $errors->first('style_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="style-value-input" class="col-sm-3 form-control-label">{{ __('Style Value') }}<span class="text-danger"> *</span></label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="style_value"
                                                    class="form-control {{ $errors->has('style_value') ? 'is-invalid' : '' }}"
                                                    id="style-value-input"
                                                    placeholder="{{ __('Style Value') }}"
                                                    value="{{ old('style_value', $slider->style_value) }}"
                                                >
                                                @if($errors->has('style_value'))
                                                    <small class="text-danger">{{ $errors->first('style_value') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary btn-edit-slider">{{ __('Edit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function () {
    $('.multiselect2-types').select2({
        placeholder: '{{ __('Select Types') }}'
    });

    $('#add-new-group').on('click', function (e) {
        e.preventDefault();

        $('#button-forms').children().children().slice(0, 3).clone().val('').appendTo('#append-forms');
    });

    $('.btn-edit-slider').on('click', function (e) {
        e.preventDefault();

        $.confirm({
            title: 'Pažnja!',
            content: 'Da li želite da sačuvate izmjene?',
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'OK',
                    btnClass: 'btn-warning',
                    action: function () {
                        $('#form-edit-slider').submit();
                    }
                },
                cancel: function () {

                }
            }
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.preview-image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".preview-input").change(function() {
        // $('.preview-div').attr('hidden', false);
        readURL(this);
    });
});
</script>
@endsection
