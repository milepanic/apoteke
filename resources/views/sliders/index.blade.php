<!--slider-->
<div class="home-slider clearfix">
    <div class="flexslider">
        <ul class="slides">
            <!--slide start-->
            <li class="flex-active-slide">
                <img src="https://cdn.biotecnika.org/wp-content/uploads/2017/05/labib-demian-youssef-slide02-laboratory-equipments-analytical-balances-1.png" alt="Medical Services That You Can Trust" draggable="false">
                <div class="content-wrapper clearfix">
                    <div class="container">
                        <div class="slide-content clearfix">
                            <h1>Dobro dosli na  <span>apoteke.ba</span></h1>
                            <p>Apoteke.ba je prvi portal u Bosni i Hercegovini sa bazom svih apoteka i njihovim proizvoda. <br> Pored baze apoteka i lijekova na portalu mozete pronaci i blog koji vode strucna i certifikovana lica za medicinu.</p>
                            <a class="slider-button" href="doctors-three-columns.html">APOTEKE</a>
                            <a class="slider-button" href="doctors-three-columns.html">PROIZVODI</a>
                            <a class="slider-button" href="doctors-three-columns.html">BLOG</a>
                        </div>
                    </div>
                </div>
            </li>
            <!--slide end-->

            <!--slide start-->
            <li>
                <img src="https://blgba.com.au/wp-content/uploads/2017/04/Benchmarking-Blog-image.jpg" alt="We Care for You" draggable="false">
<!--                 <div class="content-wrapper clearfix">
                    <div class="container">
                        <div class="slide-content clearfix ">
                            <h1>We <span>Care</span> for <span>You</span></h1>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            <a class="slider-button" href="doctors-three-columns.html">Read More</a>
                        </div>
                    </div>
                </div> -->
            </li>
            <!--slide end-->
        </ul>
        <!--directional nav-->
        <ul class="flex-direction-nav">
            <li><a class="flex-prev" href="#">Previous</a></li>
            <li><a class="flex-next" href="#">Next</a></li></ul>
        </div>
    </div>