@extends('layouts.master')
@section('title', 'Naziv apoteke | APOTEKE.BA')

@section('content')
<div class="home-slider clearfix">
    <div class="flexslider loading">
        <ul class="slides">
            <!--slide start-->
            <li>
                <img src="{{ file_path($pharmacy->cover_image) }}" alt="Medical Services <span>That You Can Trust</span>"/>
            </li>
            <!--slide end-->

        </ul>
    </div>
    <!--info medics-->
    <div class="container-fluid pharmacy-header-wrap">
        <div class="container">
            <div class="row">
                <div class="pharmacy-logo-wrap col-md-3 col-sm-3 col-xs-12">
                    <div class="pharmacy-logo">
                        <img src="{{ file_path($pharmacy->thumbnail) }}" class="img-fluid" alt="{{ $pharmacy->name }}">
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 pharmacy-info">
                    <h1>{{ $pharmacy->name }}</h1>
                    <div class="row">
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <i class="far fa-clock"></i>
                            <span class="pharmacy-block">
                                Radno vrijeme: 09:00h - 21:00h (OTVORENO jos 3h)  
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <i class="fas fa-map-marker-alt"></i>
                            <span class="pharmacy-block pharmacy-address">
                                Majevicka 175,
                                76300 Bijeljina
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <i class="fas fa-phone"></i>
                            <span class="pharmacy-block">
                                +387 55 215 251
                                +387 55 215 252
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--appointment form end-->
</div>
<!--general services-->
<div class="features-var-two clearfix">
    <div class="container">
        <div class="row">
            <section class="single-feature text-center clearfix col-sm-4">
                <img src="{{ asset('theme/apoteke/images/temp-images/pharmacy-icon.svg') }}" class="pharmacy-icon" alt="Special Care Unit">
                <h3>O nama</h3>
                <div class="feature-border"></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                <a class="read-more" href="#aboutModal" data-toggle="modal" data-target="#aboutModal">Read More</a>
            </section>
            <section class="single-feature text-center clearfix col-sm-4">
                <img src="{{ asset('theme/apoteke/images/temp-images/services.svg') }}" alt="Caring Nursing Staff">
                <h3>Usluge</h3>
                <div class="feature-border"></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                <a class="read-more" href="#servicesModal" data-toggle="modal" data-target="#servicesModal">Read More</a>
            </section>
            <section class="single-feature text-center clearfix col-sm-4">
                <img src="{{ asset('theme/apoteke/images/temp-images/placeholder.svg') }}" alt="Location">
                <h3>Lokacija</h3>
                <div class="feature-border"></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                <a class="read-more" href="#map-header">Read More</a>
            </section>
        </div>
    </div>
</div>
<!--general services end-->
<!--Woorking time -->
<div class="container">
    <div class="visible-xs">
        <table class="table">
            <tbody>
                <tr>
                    <td class="season" align="center" colspan="2"><i class="fas fa-snowflake"></i> ZIMI</td>
                </tr>
                <tr>
                    <td class="day">Ponedeljak</td>
                    <td>07:30-20:00</td>
                </tr>
                <tr>
                    <td class="day">Utorak</td>
                    <td>07:30-20:00</td>
                </tr>
                <tr>
                    <td class="day">Sreda</td>
                    <td>07:30-20:00</td>
                </tr>
                <tr>
                    <td class="day">Četvrtak</td>
                    <td>07:30-20:00</td>
                </tr>
                <tr>
                    <td class="day">Petak</td>
                    <td>07:30-20:00</td>
                </tr>
                <tr>
                    <td class="day">Subota</td>
                    <td>07:30-20:00</td>
                </tr>
                <tr>
                    <td class="day">Nedelja</td>
                    <td>07:30-20:00</td>
                </tr>
            </tbody>
        </table>
        <table class="table">
            <tbody>
                <tr>
                    <td class="season" colspan="2" align="center"><i class="fas fa-sun"></i> LJETI</td>
                </tr>
                <tr>
                    <td class="day">Ponedeljak</td>
                    <td>07:00-00:00</td>
                </tr>
                <tr>
                    <td class="day">Utorak</td>
                    <td>07:00-00:00</td>
                </tr>
                <tr>
                    <td class="day">Sreda</td>
                    <td>07:00-00:00</td>
                </tr>
                <tr>
                    <td class="day">Četvrtak</td>
                    <td>07:00-00:00</td>
                </tr>
                <tr>
                    <td class="day">Petak</td>
                    <td>07:00-00:00 </td>
                </tr>
                <tr>
                    <td class="day">Subota</td>
                    <td>07:00-00:00</td>
                </tr>
                <tr>
                    <td class="day">Nedelja</td>
                    <td>07:00-00:00</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="hidden-xs">
        <table class="table hidden-xs">
            <tbody>
                <tr>
                    <td class="day"></td>
                    <td class="day"><span>Ponedeljak</span></td>
                    <td class="day"><span>Utorak</span></td>
                    <td class="day"><span>Srijeda</span></td>
                    <td class="day"><span>Četvrtak</span></td>
                    <td class="day"><span>Petak</span></td>
                    <td class="day"><span>Subota</span></td>
                    <td class="day"><span>Nedelja</span></td>
                </tr>
                <tr>
                    <td class="season" align="center"><i class="fas fa-snowflake"></i> ZIMI</td>
                    <td>07:30 -20:00</td>
                    <td>07:30 -20:00</td>
                    <td> 07:30 -20:00</td>
                    <td>07:30 -20:00</td>
                    <td>07:30 -20:00</td>
                    <td>07:30 -20:00</td>
                    <td>07:30 -20:00</td>
                </tr>
                <tr>
                    <td class="season" align="center"><i class="fas fa-sun"></i> LJETI</td>
                    <td>07:00 -00:00</td>
                    <td>07:00 -00:00</td>
                    <td>07:00 -00:00</td>
                    <td>07:00 -00:00</td>
                    <td>07:00 -00:00</td>
                    <td>07:00 -00:00</td>
                    <td>07:00 -00:00</td>
                </tr>
            </tbody>
        </table>
    </div>


</div>
</div>

<!--Working time end-->
<!--products section-->
<div class="home-doctors  clearfix">
    <div class="container">
        <div class="slogan-section animated fadeInUp clearfix">
            <h2><span>Proizvodi</span></h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
        <div class="row mb-2">
            <div class="col-md-3 col-sm-6 text-center">
                <div class="common-doctor animated fadeInUp clearfix">
                    <figure>
                        <a href="teliska.html" title="teliska ">
                            <img src="{{ asset('theme/apoteke/images/products/teliska.jpg') }}" alt="teliska-1" />
                        </a>
                    </figure>
                    <div class="text-content">
                        <h5><a href="doctor-alex.html">Teliska</a></h5>
                        <div class="for-border"></div>
                        <p>
                            This text represents a brief introduction of doctor and this text will be displayed on homepage and all the places where multiple doctors are listed.
                        </p>
                    </div>
                </div>
                <a class="read-more" href="doctor-alex.html">Read More</a>
            </div>
            <div class="col-md-3 col-sm-6 text-center">
                <div class="common-doctor animated fadeInUp clearfix">
                    <figure>
                        <a href="metamorfin1.html" title="metamorfin1">
                            <img src="{{ asset('theme/apoteke/images/products/metamorfin1.jpg') }}" alt="metamorfin1" />
                        </a>
                    </figure>
                    <div class="text-content">
                        <h5><a href="doctor-becka.html">Metamorfin</a></h5>
                        <div class="for-border"></div>
                        <p>
                            This text represents a brief introduction of doctor and this text will be displayed on homepage and all the places where multiple doctors are listed.
                        </p>
                    </div>
                </div>
                <a class="read-more" href="doctor-becka.html">Read More</a>
            </div>

            <div class="visible-sm clearfix margin-gap"></div>

            <div class="col-md-3 col-sm-6 text-center">
                <div class="common-doctor animated fadeInUp clearfix">
                    <figure>
                        <a href="spirulina.html" title="spirulina">
                            <img src="{{ asset('theme/apoteke/images/products/spirulina.jpg') }}"  alt="spirulina" />
                        </a>
                    </figure>
                    <div class="text-content">
                        <h5><a href="doctor-bert.html">Spirulina</a></h5>
                        <div class="for-border"></div>
                        <p>
                            This text represents a brief introduction of doctor and this text will be displayed on homepage and all the places where multiple doctors are listed.
                        </p>
                    </div>
                </div>
                <a class="read-more" href="doctor-bert.html">Read More</a>
            </div>

            <div class="col-md-3 col-sm-6 text-center">
                <div class="common-doctor animated fadeInUp clearfix">
                    <figure>
                        <a href="inlife-Calcium.html" title="inlife-Calcium">
                            <img src="{{ asset('theme/apoteke/images/products/inlife-Calcium(2).jpg') }}"  alt="inlife-Calcium(2)" />
                        </a>
                    </figure>
                    <div class="text-content">
                        <h5><a href="inlife-Calcium.html">Inlife-Calcium</a></h5>
                        <div class="for-border"></div>
                        <p>
                            This text represents a brief introduction of doctor and this text will be displayed on homepage and all the places where multiple doctors are listed.
                        </p>
                    </div>
                </div>
                <a class="read-more" href="doctor-taleebin.html">Read More</a>
            </div>
            <div class="visible-sm clearfix margin-gap"></div>
        </div>

                <a class="read-more" href="#">Pogledaj sve proizvode iz ove apoteke</a>
    </div>
</div>
<!--doctors section end-->
<!--blog posts section-->
<div class="home-blog blog-var-two clearfix">
    <div class="container">
        <div class="slogan-section animated fadeInUp clearfix">
            <h2><span>Akcija</span></h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 mb-3">
                <article class="common-blog-post animated fadeInRight clearfix">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <figure>
                                <a href="image-post-format.html" title="Image Post Format">
                                    <div class="ribbon red"><span>Popust 18%</span></div>
                                    <img src="{{ asset('theme/apoteke/images/products/metamorfin1.jpg') }}" alt="Product Image">
                                </a>
                            </figure>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="text-content clearfix">
                                <h5><a href="image-post-format.html">Metamorfin</a></h5>
                                <div class="entry-meta">
                                    <time class="entry-time updated" datetime="2014-05-10T11:07:36+00:00">May <strong>10</strong></time>
                                    , by <a href="#" title="Posts by John Doe">Apoteka Rosić 1</a>
                                </div>
                                <div class="for-border"></div>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat&hellip; </p>
                                <a class="read-more" href="image-post-format.html">Read More</a>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <article class="common-blog-post animated fadeInRight clearfix">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="gallery gallery-slider clearfix loading">
                                <div class="ribbon red"><span>Popust 15%</span></div>
                                <ul class="slides">
                                    <li>
                                        <a href="{{ asset('theme/apoteke/images/temp-images/gallery-1.jpg') }}" title="" >
                                            <img src="{{ asset('theme/apoteke/images/products/flupara.jpg') }}" alt="Product Image">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ asset('theme/apoteke/images/temp-images/gallery-2.jpg') }}" title="" >
                                            <img src="{{ asset('theme/apoteke/images/products/inlife-Calcium(2).jpg') }}" alt="Product Image">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ asset('theme/apoteke/images/temp-images/gallery-3.jpg') }}" title="" >
                                            <img src="{{ asset('theme/apoteke/images/products/citicoline.jpg') }}" alt="Product Image">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="text-content clearfix">
                                <h5><a href="gallery-post-format.html">Gallery Post Format</a></h5>
                                <div class="entry-meta">
                                    <time class="entry-time updated" datetime="2014-05-10T11:07:36+00:00">May <strong>10</strong></time>
                                    , by <a href="#" title="Posts by John Doe">John Doe</a>
                                </div>
                                <div class="for-border"></div>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat&hellip; </p>
                                <a class="read-more" href="gallery-post-format.html">Read More</a>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
    <!--blog posts section end-->

    <!--testimonials section-->
    <div class="home-testimonial  clearfix">
        <div class="container">
            <div class="text-center">
                <div class="slogan-section animated fadeInUp clearfix">
                    <h2><span>Zaposleni</span></h2>
                </div>
            </div>
            <div class="row small-worker-profile">
                <div class="col-sm-10 col-sm-offset-1 text-center">
                    <div class="flexslider-three animated fadeInUp">
                        <ul class="slides">
                            <li>
                                <img class="img-circle" src="{{ asset('theme/apoteke/images/temp-images/pharmacy-profile.png') }}" alt="author-22" />
                                <blockquote>
                                    <p>
                                        Marina Maric je diplomirani farmaceut. Farmaceutski fakultet je završila 2015. godine u Banja Luci, nakon čega je položila državni i stručni ispit. Od juna 2016. godine, zaposlena je u apoteci Rosić, gdje obavlja funkciju farmaceuta.
                                    </p>
                                </blockquote>
                                <div class="testimonial-footer clearfix">
                                    <h3> <a href="_blank"> Marina Marić </a></h3>
                                    <div class="for-border"></div>
                                    <p>Diplomirani farmaceut</p>
                                </div>
                            </li>
                            <li>
                                <img class="img-circle" src="{{ asset('theme/apoteke/images/temp-images/worker.jpg') }}"  alt="author-11" />
                                <blockquote>
                                    <p>
                                        Jelena Stević radi na poziciji farmaceutskog tehničara.
                                        Srednju medicinsku školu, smjer: farmacija, završila je u Bijeljini. Nakon završene srednje škole, i odradjenog pripravničkog staža, položila je stručni i državni ispit.
                                        Od juna 2016. godine zaposlena je na poziciji farmaceutskog tehnicara u apoteci Rosić.
                                    </p>
                                </blockquote>
                                <div class="testimonial-footer clearfix">
                                    <h3><a href="_blank">Jelena Stević</a></h3>
                                    <div class="for-border"></div>
                                    <p>Farmaceutski tehničar</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--google map -->
    <div class="map-location contact-page">
        <div class="map-wrapper" id="map-header">
            <div id="map-canvas"></div>
        </div>
    </div>

</div>
<!-- Modals -->
<div class="modal fade" id="aboutModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">O nama</h4>
      </div>
      <div class="modal-body">
          <p>Some text in the modal Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam repellat vero magni sequi dolor voluptatum officia quae minus amet maxime perferendis alias, rerum, voluptate saepe dignissimos eum pariatur, reprehenderit esse..</p>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default read-more" data-dismiss="modal">Close</button>
      </div>
  </div>

</div>
</div>
<div class="modal fade" id="servicesModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Usluge</h4>
      </div>
      <div class="modal-body">
          <p>Some text in the modal Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi doloribus iure totam tenetur assumenda harum vel magnam possimus ex, error illum a itaque, dignissimos quia sapiente minus rem? Eos, perspiciatis..</p>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default read-more" data-dismiss="modal">Close</button>
      </div>
  </div>

</div>
</div>
<!-- End modals -->
@endsection

@section('scripts')
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<!--google map API-->
<script>
    function initializeContactMap() {
        var officeLocation = new google.maps.LatLng(-37.817314, 144.955431);
        var contactMapOptions = {
            zoom:  14,
            center: officeLocation,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        }
        var contactMap = new google.maps.Map(document.getElementById('map-canvas'), contactMapOptions);

        var contactMarker = new google.maps.Marker({
            position: officeLocation,
            map: contactMap
        });

    }
    window.onload = initializeContactMap();

</script>
@endsection