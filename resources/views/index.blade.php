@extends('layouts.master')

@section('title', 'APOTEKE.BA | Lista svih apoteka u BiH')

@section('content')

@include('sliders.index')

    <!--general services-->
    <div class="home-features clearfix">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <div class="features-intro clearfix">
                        <h2>LISTA SVI APOTEKA U BIH - <span>APOTEKE.BA</span> </h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                        <a class="read-more" href="services-two-columns.html">VIDI SVE APOTEKE</a>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="row">
                        <div class="col-sm-6 single-feature">
                            <div class="row">
                                <div class="col-sm-3 icon-wrapper">
                                    <i class="fa fa-medkit fa-custom"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h3>LISTA PROIZVODA</h3>
                                    <p>Najveca baza proizvoda i lijekova sa cijenama po apotekama.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 single-feature">
                            <div class="row">
                                <div class="col-sm-3 icon-wrapper">
                                    <i class="fa fa-rss fa-custom"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h3>BLOG</h3>
                                    <p>Korisni savjeti za zdrav zivot i resavanje raznih tjegoba i bolesti.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 single-feature" style="height: 100px">
                            <img src="https://dummyimage.com/768x96/ccc/fff" alt="BANNER" class="img-fluid">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--general services end-->

    <!--doctors section-->
    <div class="home-doctors  clearfix">
        <div class="container">
            <div class="slogan-section animated fadeInUp clearfix">
                <h2><span>ISTAKNUTE APOTEKE</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6  text-center">
                    <div class="common-doctor animated fadeInUp clearfix">
                        <figure>
                            <a href="doctor-alex.html" title="Dr.Addison Alexander">
                                <img src="{{ asset('theme/apoteke/images/temp-images/doctor-2.jpg') }}" alt="doctor-2">
                            </a>
                        </figure>
                        <div class="text-content">
                            <h5><a href="doctor-alex.html">Apoteka Rosic - Bijeljina 1</a></h5>
                            <div class="for-border"></div>
                            <p>
                                This text represents a brief introduction of doctor and this text will be displayed on homepage and all the places where multiple doctors are listed.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6  text-center">
                    <div class="common-doctor animated fadeInUp clearfix">
                        <figure>
                            <a href="doctor-becka.html" title="Dr.Adaline Becka">
                                <img src="{{ asset('theme/apoteke/images/temp-images/doctor-1.jpg') }}" alt="doctor-1">
                            </a>
                        </figure>
                        <div class="text-content">
                            <h5><a href="doctor-becka.html">Moja Apoteka - Centar</a></h5>
                            <div class="for-border"></div>
                            <p>
                                This text represents a brief introduction of doctor and this text will be displayed on homepage and all the places where multiple doctors are listed.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="visible-sm clearfix margin-gap"></div>

                <div class="col-md-3 col-sm-6  text-center">
                    <div class="common-doctor animated fadeInUp clearfix">
                        <figure>
                            <a href="doctor-bert.html" title="Dr.Andrew Bert">
                                <img src="{{ asset('theme/apoteke/images/temp-images/doctor-4.jpg') }}" alt="doctor-4">
                            </a>
                        </figure>
                        <div class="text-content">
                            <h5><a href="doctor-bert.html">Parmacy Bijeljina</a></h5>
                            <div class="for-border"></div>
                            <p>
                                This text represents a brief introduction of doctor and this text will be displayed on homepage and all the places where multiple doctors are listed.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6  text-center">
                    <div class="common-doctor animated fadeInUp clearfix">
                        <figure>
                            <a href="doctor-taleebin.html" title="Dr.Orana Taleebin">
                                <img src="{{ asset('theme/apoteke/images/temp-images/doctor-3.jpg') }}" alt="doctor-3">
                            </a>
                        </figure>
                        <div class="text-content">
                            <h5><a href="doctor-taleebin.html">Apoteka Rosić - Ugljevik</a></h5>
                            <div class="for-border"></div>
                            <p>
                                This text represents a brief introduction of doctor and this text will be displayed on homepage and all the places where multiple doctors are listed.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="visible-sm clearfix margin-gap"></div>

            </div>
            <a class="read-more" href="doctor-becka.html" style="margin-top: 20px">POGLEDAJ SVE APOTEKE</a>
        </div>

    </div>
    <!--doctors section end-->


    <!--blog posts section-->
    <div class="home-blog text-center clearfix">
        <div class="container">
            <div class="slogan-section animated fadeInUp clearfix">
                <h2>AKCIJE <span>LIJEKOVA I PROIZVODA</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <article class="common-blog-post animated fadeInRight clearfix">
                        <figure>
                            <a href="image-post-format.html" title="Naziv Akcije">
                                <img src="https://healthjade.com/wp-content/uploads/2018/07/paracetamol.jpg"  alt="news-2" />
                            </a>
                        </figure>
                        <div class="text-content clearfix">
                            <h5><a href="image-post-format.html">Paractamog 20mg</a></h5>
                            <div class="entry-meta">
                                akcija traje do 10.5.2018.

                            </div>
                            <div class="for-border"></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat&hellip; </p>
                        </div>

                    </article>
                </div>
                <div class="col-md-4">
                    <article class="common-blog-post animated fadeInRight clearfix">
                        <div class="gallery gallery-slider clearfix loading">
                            <ul class="slides">
                                <li>
                                    <a href="{{ asset('theme/apoteke/images/temp-images/gallery-1.jpg') }}" title="" >
                                        <img src="{{ asset('theme/apoteke/images/temp-images/gallery-1.jpg') }}" alt="gallery-1" />
                                    </a>
                                </li>
                                <li class="flex-active-slide">
                                    <a href="{{ asset('theme/apoteke/images/temp-images/gallery-2.jpg') }}" title="" >
                                        <img src="{{ asset('theme/apoteke/images/temp-images/gallery-2.jpg') }}" alt="gallery-2" />
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ asset('theme/apoteke/images/temp-images/gallery-3.jpg') }}" title="" >
                                        <img src="{{ asset('theme/apoteke/images/temp-images/gallery-3.jpg') }}" alt="gallery-3" />
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ asset('theme/apoteke/images/temp-images/gallery-2.jpg') }}" title="" >
                                        <img src="{{ asset('theme/apoteke/images/temp-images/gallery-2.jpg') }}" alt="gallery-4" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="text-content clearfix">
                            <h5><a href="gallery-post-format.html">Gallery Post Format</a></h5>
                            <div class="entry-meta">
                                <span>10 May, 2014</span>, by <a href="#" title="Posts by John Doe">John Doe</a>
                            </div>
                            <div class="for-border"></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat&hellip; </p>
                        </div>
                    </article>

                </div>

                <div class="col-md-4">
                    <article class="common-blog-post animated fadeInRight clearfix">
                        <div class="video clearfix">
                            <div class="video-wrapper clearfix">
                                <iframe src="//player.vimeo.com/video/75594702?title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="text-content clearfix">
                            <h5><a href="video-post-format.html">The Hero In All Of Us</a></h5>
                            <div class="entry-meta">
                                <span>10 May, 2014</span>, by <a href="#" title="Posts by John Doe">John Doe</a>
                            </div>
                            <div class="for-border"></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat&hellip; </p>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
    <!--blog posts section end-->


    <!--testimonials section-->
    <div class="home-testimonial  clearfix">
        <div class="container">
            <div class="text-center">
                <div class="slogan-section animated fadeInUp clearfix">
                    <h2>KORISNI SAVJETI - <span>BLOG</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                </div>
            </div>
            <div class="tab-main animated fadeInDown clearfix ae-animation-fadeInDown">
            <div class="row">
                <div class="col-sm-4">
                    <div class="tab-title active">
                        <h6>Dental Implants</h6>
                    </div>
                    <div class="tab-title">
                        <h6>Blood Bank</h6>
                    </div>
                    <div class="tab-title">
                        <h6>Medicine Research</h6>
                    </div>
                    <div class="tab-title">
                        <h6>Pharmaceutical Advice</h6>
                    </div>
                    <div class="tab-title">
                        <h6>Medical Counseling</h6>
                    </div>
                </div>

                <div class="col-sm-8">
                    <div class="tab-content">
                        <article class="content clearfix hentry">
                            <div class="row">
                                <div class="col-md-6">
                                    <figure>
                                        <a href="services-dental.html">
                                            <img  src="{{ asset('theme/apoteke/images/temp-images/service-3-670x500.jpg') }}" alt="service-3">
                                        </a>
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h5><a href="services-dental.html" rel="bookmark">Dental Implants</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip…</p>
                                    <a class="read-more" href="services-dental.html">Read More</a>
                                </div>
                            </div>
                        </article>
                        <article class="content clearfix hentry">
                            <div class="row">
                                <div class="col-md-6">
                                    <figure>
                                        <a href="services-blood.html">
                                            <img src="{{ asset('theme/apoteke/images/temp-images/service-2-670x500.jpg') }}" alt="service-2">
                                        </a>
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h5><a href="services-blood.html" rel="bookmark">Blood Bank</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip…</p>
                                    <a class="read-more" href="services-blood.html">Read More</a>
                                </div>
                            </div>
                        </article>
                        <article class="content clearfix hentry">
                            <div class="row">
                                <div class="col-md-6">
                                    <figure>
                                        <a href="services-research.html">
                                            <img src="{{ asset('theme/apoteke/images/temp-images/service-5-670x500.jpg') }}"  alt="service-5">
                                        </a>
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h5><a href="services-research.html">Medicine Research</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip…</p>
                                    <a class="read-more" href="services-research.html">Read More</a>
                                </div>
                            </div>
                        </article>
                        <article class="content clearfix hentry">
                            <div class="row">
                                <div class="col-md-6">
                                    <figure>
                                        <a href="services-advice.html">
                                            <img src="{{ asset('theme/apoteke/images/temp-images/service-4-670x500.jpg') }}"  alt="service-4">
                                        </a>
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h5><a href="services-advice.html">Pharmaceutical Advice</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip…</p>
                                    <a class="read-more" href="services-advice.html">Read More</a>
                                </div>
                            </div>
                        </article>
                        <article class="content clearfix hentry">
                            <div class="row">
                                <div class="col-md-6">
                                    <figure>
                                        <a href="services-counseling.html">
                                            <img src="{{ asset('theme/apoteke/images/temp-images/service-1-670x500.jpg') }}" alt="service-1">
                                        </a>
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h5><a href="services-counseling.html">Medical Counseling</a></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip…</p>
                                    <a class="read-more" href="services-counseling.html">Read More</a>
                                </div>
                            </div>
                        </article>
                    </div><!-- end of tab-content -->
                </div>

            </div><!-- tab-main -->
        </div>
        </div>
    </div>
    <!--testimonials section end-->

@endsection