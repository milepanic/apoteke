@extends('layouts.master')
@section('title', 'O nama | APOTEKE.BA')

@section('content')
    <div class="banner clearfix"></div>
    
    <!--main part-->
    <section class="aboutUs">
        <div class="container-fluid">
            <div class="container about ">
                <div class="f-title">
                    <h2>NEKOLIKO RIJEČI O NAMA</h2>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="about-excerpt">
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde, molestiae quis.
                                Beatae error possimus nam porro, reprehenderit nihil maiores esse. 
                                Porro ducimus odit reiciendis sequi et ratione voluptas consequatur quasi. 
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Repellat voluptatum iste magni porro, blanditiis, neque sapiente eaque rem asperiores ipsa aliquam, sint facere.
                                Ratione, amet voluptates aperiam ad incidunt aliquam? 
                            </p>  
                            <hr>                      
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-12 col-md-4 block-item">
                        <div class="info-img"><img src="http://www.hausmajstor.me/theme/images/misija.png" alt="icon"></div>
                        <div class="info-title text-center">
                            <h3>MISIJA</h3>
                        </div>
                        <div class="info-excerpt text-center">
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Velit esse quae odio corrupti quos placeat possimus cupiditate delectus nihil ullam magni,
                                aliquam doloribus obcaecati natus inventore modi magnam explicabo laboriosam?&nbsp;
                            </p>                       
                        </div>
                    </div> <!--end fact-item-->
                    
                    <div class="col-12 col-md-4 block-item">
                        <div class="info-img"><img src="http://www.hausmajstor.me/theme/images/vizija.png" alt="icon"></div>
                        
                        <div class="info-title text-center">
                            <h3>VIZIJA</h3>
                        </div>
                        <div class="info-excerpt text-center">
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                Sunt modi molestias corporis porro ut vitae atque mollitia asperiores voluptatibus,
                                veritatis itaque quo accusamus est similique tempora ex deserunt! Quidem, facere?
                            </p>                        
                        </div>
                    </div> <!--end fact-item-->
                    
                    <div class="col-12 col-md-4 block-item">
                        <div class="info-img"><img src="http://www.hausmajstor.me/theme/images/sta-radimo.png" alt="icon"></div>
                        
                        <div class="info-title text-center">
                            <h3>RAZVOJ</h3>
                        </div>
                        <div class="info-excerpt text-center">
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                Ullam adipisci repudiandae dolorem dicta officia necessitatibus, perspiciatis repellat, eaque dignissimos odit esse exercitationem!
                                Temporibus harum a beatae repellendus quidem vero blanditiis!
                            </p>                
                        </div>
                    </div> <!--end fact-item-->
                </div>
            </div> <!--end about-->
            <div class="container-fluid">
                <div class="container forCustomers">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="f-title">
                                <h2>ZA KORISNIKE</h2>
                            </div>
                            <div class="about-excerpt">
                                <p>
                                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis saepe similique accusantium
                                    quasi placeat quas reprehenderit doloremque nam? Rem inventore temporibus tenetur, 
                                    fugiat consectetur sed cupiditate facilis deserunt similique consequatur.
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam tempore tempora blanditiis,
                                    recusandae similique accusantium natus laborum iure harum, amet commodi consectetur.
                                    Voluptatem, beatae soluta! Ad saepe inventore tenetur quia?
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                    Facere nostrum aperiam eum similique, illo commodi, esse ratione cum magnam nobis
                                    et rerum omnis id voluptatibus sapiente repellendus, velit blanditiis inventore?
                                </p>                        
                            </div>
                        </div>
                        <div class="hidden-xs-down col-md-6 image-center text-center">
                            <img src="{{ asset('theme/apoteke/images/aboutUs/medical.jpg') }}" alt="Medical" class="col-xs-4 medical img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="container forClients">
                    <div class="row">
                         <div class="hidden-xs-12 col-md-6 image-center text-center">
                            <img src="{{ asset('theme/apoteke/images/aboutUs/shakeHands.jpg') }}" alt="Medical" class="shakeHands">
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="f-title">
                                <h2>ZA KLIJENTE</h2>
                            </div>
                            <div class="about-excerpt">
                                <p>
                                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis saepe similique accusantium
                                    quasi placeat quas reprehenderit doloremque nam? Rem inventore temporibus tenetur, 
                                    fugiat consectetur sed cupiditate facilis deserunt similique consequatur.
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam tempore tempora blanditiis,
                                    recusandae similique accusantium natus laborum iure harum, amet commodi consectetur.
                                    Voluptatem, beatae soluta! Ad saepe inventore tenetur quia?
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                    Facere nostrum aperiam eum similique, illo commodi, esse ratione cum magnam nobis
                                    et rerum omnis id voluptatibus sapiente repellendus, velit blanditiis inventore?
                                </p>                        
                            </div>
                        </div>
                    </div>
                </div> <!--end about-->
            </div>
        </div>
    </section>
   
@endsection