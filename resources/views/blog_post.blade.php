@extends('layouts.master')
@section('title', 'Naziv bloga | APOTEKE.BA')

@section('content')

    <div class="banner clearfix"></div>
    <!--blog post section-->
    
    
    <div class="blog-page clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="blog-post-single clearfix"> 
                        <div class="row">
                            <!--post date-->
                            <div class="col-sm-2">
                                <div class="left_meta clearfix entry-meta">
                                    <time class="entry-time" datetime="2014-05-10T11:07:36+00:00">May <strong>10</strong></time>
                                    <span class="comments_count clearfix entry-comments-link"><a href="#comments" title="Comment on Gallery Post Format">0</a></span>
                                </div>
                            </div>
                            <!--post contents-->
                            <div class="col-sm-10">
                                <article class="post format-gallery hentry clearfix">
                                    <div class="right-contents">
                                        <header class="entry-header">
                                            <!--post slider-->
                                            <div class="gallery gallery-slider clearfix">
                                                <ul class="slides">
                                                    <!--slide start-->
                                                    <li>
                                                        <a href="images/blog/Webp.net-resizeimage.png" title="gallery" >
                                                        <img src="images/blog/Webp.net-resizeimage.png" alt="gallery-1" /></a>
                                                        
                                                    </li>
                                                    <!--slide end-->
                                                    
                                                    <!--slide start-->
                                                    <li>
                                                        <a href="images/temp-images/gallery-2.jpg" title="" >
                                                            <img src="images/temp-images/gallery-2.jpg" alt="gallery-2" />
                                                        </a>
                                                    </li>
                                                    <!--slide end-->
                                                    
                                                    <!--slide start-->
                                                    <li>
                                                        <a href="images/temp-images/gallery-3.jpg" title="" >
                                                            <img src="images/temp-images/gallery-3.jpg" alt="gallery-3" />
                                                        </a>
                                                    </li>
                                                    <!--slide end-->
                                                    
                                                    <!--slide start-->
                                                    <li>
                                                        <a href="images/temp-images/gallery-4.jpg" title="" >
                                                            <img src="images/temp-images/gallery-4.jpg" alt="gallery-4" />
                                                        </a>
                                                    </li>
                                                    <!--slide end-->
                                                </ul>
                                            </div>
                                            <!--post title-->
                                            <h1 class="entry-title">Gallery Post Format</h1>
                                            <!--post author-->
                                            <span class="entry-author">Posted by  <span class="entry-author-link"><a href="#" title="Posts by John Doe">John Doe</a></span></span>
                                        </header>
                                        <!--post detail-->
                                        <div class="entry-content">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                                Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                                <span id="more-86"></span>
                                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
                                                vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent 
                                                luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                                                Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.
                                            </p>
                                            <h3>
                                                Importance of Health
                                            </h3>
                                            <p>
                                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
                                                vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio
                                                dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla
                                                facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming
                                                id quod mazim placerat facer possim assum.
                                            </p>
                                            <ol>
                                                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
                                                <li>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</li>
                                                <li>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</li>
                                                <li>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</li>
                                                <li>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</li>
                                            </ol>
                                            <h3>Living a Healthier Life</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                                            <blockquote><p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p></blockquote>
                                            <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                                            <p class="message bg-info text-info"><strong>Information:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco quat.<button type="button" class="close" aria-hidden="true">&times;</button></p>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                                            <p class="message bg-warning text-warning"><strong>Tip:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco quat. <button type="button" class="close" aria-hidden="true">&times;</button></p>
                                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                                        </div>
                                        
                                        <footer class="entry-footer">
                                            <p class="entry-meta">
                                                <span class="entry-categories">
                                                    <i class="fa fa-folder-o"></i>&nbsp; <a href="#" title="View all posts in Environment" >Environment</a>
                                                </span>
                                                <span class="entry-tags">
                                                    <i class="fa fa-tags"></i>&nbsp; <a href="#">blood pressure</a>,
                                                    <a href="#">clean environment</a>, <a href="#">friendly staff</a>,
                                                    <a href="#">kids health</a>
                                                </span>
                                            </p>
                                        </footer>
                                    </div>
                                </article>   
                                <div class="comments-wrapper">  
                                    <!-- start of comments section -->
                                    <div id="comments-section">
                                        <h5 id="comments-title">3 Comments</h5>
                                        <ol id="comments" class="commentlist" name="comments">
                                            <li class="comment">
                                                <!--comment start-->
                                                <article >
                                                    <div class="comment-wrap clearfix">
                                                        <a class="avatar" href=""><img alt="avatar" src="images/temp-images/person1-1.jpeg"></a>
                                                        <div class="comment-detail-wrap">
                                                            <div class="comment-meta">
                                                                <h5 class="author"><a href="#" class="url">Lloyd Budd</a></h5>
                                                                <time datetime="2014-05-10T11:07:36+00:00">
                                                                    May 12, 2014
                                                                </time>
                                                            </div>
                                                            <div class="comment-body entry-content">
                                                                <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                                <!-- end of comment -->
                                            </li>
                                            <li class="comment">
                                                <!--comment start-->
                                                <article >
                                                    <div class="comment-wrap clearfix">
                                                        <a class="avatar" href=""><img alt="avatar" src="images/temp-images/person3-3.jpeg"></a>
                                                        <div class="comment-detail-wrap">
                                                            <div class="comment-meta">
                                                                <h5 class="author"><a href="#" class="url">John Doe</a></h5>
                                                                <time datetime="2014-05-10T11:07:36+00:00">
                                                                    May 12, 2014
                                                                </time>
                                                            </div>
                                                            <div class="comment-body entry-content">
                                                                <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                                <!-- end of comment -->
                                            </li>
                                            <li class="comment">
                                                <!--comment start-->
                                                <article >
                                                    <div class="comment-wrap clearfix">
                                                        <a class="avatar" href=""><img alt="avatar" src="images/temp-images/person-2-2.jpeg"></a>
                                                        <div class="comment-detail-wrap">
                                                            <div class="comment-meta">
                                                                <h5 class="author"><a href="#" class="url">Alex Shiels</a></h5>
                                                                <time datetime="2014-05-10T11:07:36+00:00">
                                                                    May 12, 2014
                                                                </time>
                                                            </div>
                                                            
                                                            <div class="comment-body entry-content">
                                                                <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                                <!-- end of comment -->
                                                
                                            </li>
                                        </ol>
                                        <!--comment form-->
                                        <div id="respond" class="comment-respond">
                                            <h3 id="reply-title" class="comment-reply-title">Leave a Reply</h3>
                                            <form action="#" method="post" id="commentform" class="comment-form">
                                                <p class="comment-notes">Your email address will not be published. Required fields are marked <span class="required">*</span></p>
                                                <p class="comment-form-author">
                                                    <label for="author">Name <span class="required">*</span></label>
                                                    <input id="author" name="author" type="text" value="" size="30" aria-required='true' />
                                                </p>
                                                <p class="comment-form-email">
                                                    <label for="email">Email <span class="required">*</span></label>
                                                    <input id="email" name="email" type="text" value="" size="30" aria-required='true' />
                                                </p>
                                                <p class="comment-form-url">
                                                    <label for="url">Website</label>
                                                    <input id="url" name="url" type="text" value="" size="30" />
                                                </p>
                                                <p class="comment-form-comment">
                                                    <label for="comment">Comment</label>
                                                    <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
                                                </p>
                                                <p class="form-allowed-tags">You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes:  <code>&lt;a href=&quot;&quot; title=&quot;&quot;&gt; &lt;abbr title=&quot;&quot;&gt; &lt;acronym title=&quot;&quot;&gt; &lt;b&gt; &lt;blockquote cite=&quot;&quot;&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=&quot;&quot;&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=&quot;&quot;&gt; &lt;strike&gt; &lt;strong&gt; </code>
                                                </p>
                                                <p class="form-submit">
                                                    <input name="submit" type="submit" id="submit" value="Post Comment" />
                                                    <input type='hidden' name='comment_post_ID' value='86' id='comment_post_ID' />
                                                    <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                                </p>
                                            </form>
                                        </div><!-- #respond -->
                                    </div>
                                    <!-- end of comments -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--sidebar-->
                <div class="col-lg-3 col-md-4">
                    <aside class="sidebar clearfix">
                        <!--tab widget-->
                        <section  class="widget tabs-widget">
                            <div class="tabs clearfix">
                                <div class = "tab-head active">
                                    <h6>Popular</h6>
                                </div>
                                <div class = "tab-head ">
                                    <h6>Latest</h6>
                                </div>
                                <div class="tabs-content clearfix">
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img src="images/temp-images/news-2-150x150.jpg" class="tabs-thumb wp-post-image" alt="news-2" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Image Post Format</a></h6>
                                            <span>May 10, 2014</span>
                                        </div>
                                    </div>
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img src="images/temp-images/gallery-1-150x150.jpg" class="tabs-thumb wp-post-image" alt="gallery-1" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Gallery Post Format</a></h6>
                                            <span>May 10, 2014</span>
                                        </div>
                                    </div>
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img src="images/temp-images/news-1-150x150.jpg" class="tabs-thumb wp-post-image" alt="news-1" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Standard Post Format with Featured Image</a></h6>
                                            <span>May 06, 2014</span>
                                        </div>
                                    </div>
                                </div><div class="tabs-content clearfix">
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img src="images/temp-images/news-2-150x150.jpg" class="tabs-thumb wp-post-image" alt="news-2" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Image Post Format</a></h6>
                                            <span>May 10, 2014</span>
                                        </div>
                                    </div>
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img src="images/temp-images/gallery-1-150x150.jpg" class="tabs-thumb wp-post-image" alt="gallery-1" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Gallery Post Format</a></h6>
                                            <span>May 10, 2014</span>
                                        </div>
                                    </div>
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img src="images/temp-images/news-1-150x150.jpg" class="tabs-thumb wp-post-image" alt="news-1" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Standard Post Format with Featured Image</a></h6>
                                            <span>May 06, 2014</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs-content clearfix">
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img alt='person' src="images/temp-images/person1.jpeg" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Image Post Format</a></h6>
                                            <span>Ut wisi enim ad&hellip;</span>
                                        </div>
                                    </div>
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img alt='person' src="images/temp-images/person2.jpeg" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Image Post Format</a></h6>
                                            <span>Lorem ipsum dolor sit&hellip;</span>
                                        </div>
                                    </div>
                                    <div class="tab-post-listing clearfix">
                                        <figure>
                                            <a href="#">
                                                <img alt='person' src="images/temp-images/person3.jpeg" />
                                            </a>
                                        </figure>
                                        <div class="post-content">
                                            <h6><a href="#">Image Post Format</a></h6>
                                            <span>Typi non habent claritatem&hellip;</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        
                        <!--categories-->
                        <section class="widget widget_categories">
                            <h3 class="title">Kategorije</h3>
                            <ul>
                                <li><a href="#" title="View all posts filed under Environment">Zdravlje</a></li>
                                <li><a href="#" title="View all posts filed under Health Basics">Prehrana</a></li>
                                <li><a href="#" title="View all posts filed under Lifestyle">Ljepota</a></li>
                                <li><a href="#" title="View all posts filed under Motivation">Mame i bebe</a></li>
                            </ul>
                        </section>
                        <!--tags-->
                        <section class="widget widget_tag_cloud">
                            <h3 class="title">Tags</h3>
                            <div class="tagcloud">
                                <a href='#'  title='3 topics'>benefits</a>
                                <a href='#'  title='2 topics'>blood pressure</a>
                                <a href='#'  title='2 topics'>body</a>
                                <a href='#'  title='1 topic'>caring others</a>
                                <a href='#'  title='2 topics'>clean environment</a>
                                <a href='#'  title='3 topics'>doing good</a>
                                <a href='#'  title='2 topics'>friendly staff</a>
                                <a href='#'  title='2 topics'>habit</a>
                                <a href='#'  title='4 topics'>health</a>
                                <a href='#'  title='2 topics'>inspiring</a>
                                <a href='#'  title='2 topics'>kids health</a>
                                <a href='#'  title='3 topics'>learning</a>
                                <a href='#'  title='2 topics'>tips</a>
                                <a href='#'  title='3 topics'>willpower</a>
                            </div>
                        </section>
                        <!--Reklame-->
                        <section class="widget ads-item-wrap">
                            <h3 class="title">Reklame</h3>
                            <div class="ads">
                                <a href='https://www.prirodnolecenje.com/sremus-kapi-ramslin/'  title='sremus-kapi'>
                                    <img src="images/blog/images.jpg" alt="kapiSremus">
                                </a>    
                            </div>
                        </section>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    
  

@endsection