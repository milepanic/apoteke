@extends('layouts.master')
@section('title', 'Politika privatnosti | APOTEKE.BA')

@section('content')
    <div class="banner clearfix"></div>
    
    <section class="privacy">
        <div class="container-fluid">
            <div class="container politics-privacy ">
                <div class="f-title">
                    <h1>POLITIKA PRIVATNOSTI</h1>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="privacy-terms">
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde, molestiae quis.
                                Beatae error possimus nam porro, reprehenderit nihil maiores esse. 
                                Porro ducimus odit reiciendis sequi et ratione voluptas consequatur quasi. 
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Repellat voluptatum iste magni porro, blanditiis, neque sapiente eaque rem asperiores ipsa aliquam, sint facere.
                                Ratione, amet voluptates aperiam ad incidunt aliquam? 
                            </p>  
                            <hr>                      
                        </div>
                    </div>
                </div> 
            </div>
        </div> <!--end about-->
        <div class="container-fluid">
            <div class="container info-privacy">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="f-title">
                            <h2>OTKRIVANJE INFORMACIJA</h2>
                        </div>
                        <div class="privacy-terms">
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis saepe similique accusantium
                                quasi placeat quas reprehenderit doloremque nam? Rem inventore temporibus tenetur, 
                                fugiat consectetur sed cupiditate facilis deserunt similique consequatur.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam tempore tempora blanditiis,
                                recusandae similique accusantium natus laborum iure harum, amet commodi consectetur.
                                Voluptatem, beatae soluta! Ad saepe inventore tenetur quia?
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                Facere nostrum aperiam eum similique, illo commodi, esse ratione cum magnam nobis
                                et rerum omnis id voluptatibus sapiente repellendus, velit blanditiis inventore?
                            </p>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="container others-privacy">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="f-title">
                            <h2>DRUGI INTERNET SAJTOVI</h2>
                        </div>
                        <div class="privacy-terms">
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis saepe similique accusantium
                                quasi placeat quas reprehenderit doloremque nam? Rem inventore temporibus tenetur, 
                                fugiat consectetur sed cupiditate facilis deserunt similique consequatur.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam tempore tempora blanditiis,
                                recusandae similique accusantium natus laborum iure harum, amet commodi consectetur.
                                Voluptatem, beatae soluta! Ad saepe inventore tenetur quia?
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                Facere nostrum aperiam eum similique, illo commodi, esse ratione cum magnam nobis
                                et rerum omnis id voluptatibus sapiente repellendus, velit blanditiis inventore?
                            </p>                        
                        </div>
                    </div>
                </div>
            </div> <!--end about-->
        </div>
        <div class="container-fluid">
            <div class="container info-privacy">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="f-title">
                            <h2>UPOTREBA KOLAČIĆA (COOKIES) </h2>
                        </div>
                        <div class="privacy-terms">
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis saepe similique accusantium
                                quasi placeat quas reprehenderit doloremque nam? Rem inventore temporibus tenetur, 
                                fugiat consectetur sed cupiditate facilis deserunt similique consequatur.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam tempore tempora blanditiis,
                                recusandae similique accusantium natus laborum iure harum, amet commodi consectetur.
                                Voluptatem, beatae soluta! Ad saepe inventore tenetur quia?
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                Facere nostrum aperiam eum similique, illo commodi, esse ratione cum magnam nobis
                                et rerum omnis id voluptatibus sapiente repellendus, velit blanditiis inventore?
                            </p>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection