<?php

namespace App\Providers;

use App\Blog;
use App\Drug;
use App\Employee;
use App\Group;
use App\Pharmacy;
use App\Policies\BlogPolicy;
use App\Policies\DrugPolicy;
use App\Policies\EmployeePolicy;
use App\Policies\GroupPolicy;
use App\Policies\PharmacyPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Group::class => GroupPolicy::class,
        Pharmacy::class => PharmacyPolicy::class,
        Drug::class => DrugPolicy::class,
        Employee::class => EmployeePolicy::class,
        Blog::class => BlogPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Implicitly grant "super admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            if ($user->hasRole('super admin')) {
                return true;
            }
        });
    }
}
