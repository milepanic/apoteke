<?php

namespace App\Providers;

use App\Observers\PharmacyObserver;
use App\Pharmacy;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        \App::setLocale(\Illuminate\Support\Facades\Cookie::get('admin-language'));
        Pharmacy::observe(PharmacyObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
