<?php

namespace App;

use App\BusinessHour;
use App\Category;
use App\Drug;
use App\Employee;
use App\Group;
use App\Location;
use App\Service;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pharmacy extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $guarded = [];

    protected $casts = [
        'emails' => 'array',
        'phone_numbers' => 'array',
        'fax_numbers' => 'array',
        'documents' => 'array',
        'gallery' => 'array',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    public function drugs()
    {
        return $this->belongsToMany(Drug::class)
            ->withPivot('price', 'sale', 'promoted');
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class)
            ->withPivot('role');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }

    public function businessHours() 
    {
        return $this->hasOne(BusinessHour::class);
    }

}
