<?php

namespace App;

use App\Group;
use App\Location;
use App\User;
use Illuminate\Database\Eloquent\Model;

class JobBoard extends Model
{
    protected $guarded = [];

    public function location() 
    {
        return $this->belongsTo(Location::class);
    }

    public function group() 
    {
        return $this->belongsTo(Group::class);
    }

    public function user() 
    {
        return $this->belongsTo(User::class);
    }
}
