<?php

namespace App;

use App\Pharmacy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $guarded = [];
    
    public function pharmacies()
    {
        return $this->belongsToMany(Pharmacy::class);
    }
    
}
