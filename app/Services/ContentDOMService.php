<?php

namespace App\Services;

/**
 * Gets specific data from content
 * in html
 */
class ContentDOMService
{
    public $dom;

    public function __construct()
    {
        $this->dom = new \DOMDocument();
    }

    public function loadHTML($content)
    {
        $this->dom->loadHTML($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        return $this;
    }

    public function replaceImagePaths($oldSegment, $newSegment)
    {
        foreach ($this->dom->getElementsByTagName('img') as $img) {
            $path = $img->getAttribute('src');

            $img->setAttribute('src', str_replace($oldSegment, $newSegment, $path));
        }

        return $this;
    }

    public function getImages()
    {       
        $images = [];

        foreach ($this->dom->getElementsByTagName('img') as $img) {
           $images[] = $img->getAttribute('src');
        }

        return $images;
    }    
}
