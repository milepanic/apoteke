<?php

namespace App\Policies;

use App\User;
use App\Pharmacy;
use Illuminate\Auth\Access\HandlesAuthorization;

class PharmacyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the pharmacy.
     *
     * @param  \App\User  $user
     * @param  \App\Pharmacy  $pharmacy
     * @return mixed
     */
    public function view(User $user, Pharmacy $pharmacy)
    {
        if ($pharmacy->users()->where('user_id', $user->id)->exists() &&
            $user->hasPermissionTo('pharmacy view')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create pharmacies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('pharmacy create');
    }

    /**
     * Determine whether the user can update the pharmacy.
     *
     * @param  \App\User  $user
     * @param  \App\Pharmacy  $pharmacy
     * @return mixed
     */
    public function update(User $user, Pharmacy $pharmacy)
    {
        if ($pharmacy->users()->where('user_id', $user->id)->exists() &&
            $user->hasPermissionTo('pharmacy update')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the pharmacy.
     *
     * @param  \App\User  $user
     * @param  \App\Pharmacy  $pharmacy
     * @return mixed
     */
    public function delete(User $user, Pharmacy $pharmacy)
    {
        return $user->hasPermissionTo('pharmacy delete');
    }

    /**
     * Determine whether the user can restore the pharmacy.
     *
     * @param  \App\User  $user
     * @param  \App\Pharmacy  $pharmacy
     * @return mixed
     */
    public function restore(User $user, Pharmacy $pharmacy)
    {
        return $user->hasPermissionTo('pharmacy restore');
    }

    /**
     * Determine whether the user can permanently delete the pharmacy.
     *
     * @param  \App\User  $user
     * @param  \App\Pharmacy  $pharmacy
     * @return mixed
     */
    public function forceDelete(User $user, Pharmacy $pharmacy)
    {
        return $user->hasPermissionTo('pharmacy force delete');
    }
}
