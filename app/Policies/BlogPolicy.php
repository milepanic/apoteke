<?php

namespace App\Policies;

use App\User;
use App\=Blog;
use Illuminate\Auth\Access\HandlesAuthorization;

class BlogPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the = blog.
     *
     * @param  \App\User  $user
     * @param  \App\Blog  $Blog
     * @return mixed
     */
    public function view(User $user, Blog $Blog)
    {
        return $user->hasPermissionTo('drug view');
    }

    /**
     * Determine whether the user can create = blogs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('drug create');
    }

    /**
     * Determine whether the user can update the = blog.
     *
     * @param  \App\User  $user
     * @param  \App\Blog  $Blog
     * @return mixed
     */
    public function update(User $user, Blog $Blog)
    {
        return $user->hasPermissionTo('drug update');
    }

    /**
     * Determine whether the user can delete the = blog.
     *
     * @param  \App\User  $user
     * @param  \App\Blog  $Blog
     * @return mixed
     */
    public function delete(User $user, Blog $Blog)
    {
        return $user->hasPermissionTo('drug delete');
    }

    /**
     * Determine whether the user can restore the = blog.
     *
     * @param  \App\User  $user
     * @param  \App\Blog  $Blog
     * @return mixed
     */
    public function restore(User $user, Blog $Blog)
    {
        return $user->hasPermissionTo('drug restore');
    }

    /**
     * Determine whether the user can permanently delete the = blog.
     *
     * @param  \App\User  $user
     * @param  \App\Blog  $Blog
     * @return mixed
     */
    public function forceDelete(User $user, Blog $Blog)
    {
        return $user->hasPermissionTo('drug forceDelete');
    }
}
