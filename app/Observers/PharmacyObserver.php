<?php

namespace App\Observers;

use App\Pharmacy;
use Illuminate\Support\Str;

class PharmacyObserver
{
    /**
     * Handle the pharmacy "creating" event.
     *
     * @param  \App\Pharmacy  $pharmacy
     * @return void
     */
    public function creating(Pharmacy $pharmacy)
    {
        // produce a slug based on the model title
        $slug = Str::slug($pharmacy->name);

        // check to see if any other slugs exist that are the same & count them
        $count = Pharmacy::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

        // if other slugs exist that are the same, append the count to the slug
        $pharmacy->slug = $count ? "{$slug}-{$count}" : $slug;
    }

    /**
     * Handle the pharmacy "created" event.
     *
     * @param  \App\Pharmacy  $pharmacy
     * @return void
     */
    public function created(Pharmacy $pharmacy)
    {
        //
    }

    /**
     * Handle the pharmacy "updated" event.
     *
     * @param  \App\Pharmacy  $pharmacy
     * @return void
     */
    public function updated(Pharmacy $pharmacy)
    {
        //
    }

    /**
     * Handle the pharmacy "deleted" event.
     *
     * @param  \App\Pharmacy  $pharmacy
     * @return void
     */
    public function deleted(Pharmacy $pharmacy)
    {
        //
    }

    /**
     * Handle the pharmacy "restored" event.
     *
     * @param  \App\Pharmacy  $pharmacy
     * @return void
     */
    public function restored(Pharmacy $pharmacy)
    {
        //
    }

    /**
     * Handle the pharmacy "force deleted" event.
     *
     * @param  \App\Pharmacy  $pharmacy
     * @return void
     */
    public function forceDeleted(Pharmacy $pharmacy)
    {
        //
    }
}
