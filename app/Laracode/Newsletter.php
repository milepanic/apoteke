<?php

namespace App\Laracode;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\User');
    }

}
