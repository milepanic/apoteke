<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;

    protected $casts = [
        'images' => 'array',
        'gallery' => 'array',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected $dates = ['deleted_at'];
    
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getStoragePathAttribute()
    {
        return "blogs/{$this->slug}";
    }
}
