<?php

namespace App\Imports;

use App\Drug;
use Maatwebsite\Excel\Concerns\ToModel;

class DrugsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Drug([
            'substance' => $row[0],
            'name' => $row[1],
            'manufacturer' => $row[2],
            'shape' => $row[3],
            'dosage' => $row[4],
            'package' => $row[5],
            'official' => false,
        ]);
    }
}
