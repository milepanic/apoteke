<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    
    protected $dates = ['deleted_at'];
    
    public function photoable()
    {
        return $this->morphTo();
    }
}
