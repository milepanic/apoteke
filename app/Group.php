<?php

namespace App;

use App\Pharmacy;
use App\Photo;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $guarded = [];
    
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function pharmacies()
    {
        return $this->belongsToMany(Pharmacy::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }

}
