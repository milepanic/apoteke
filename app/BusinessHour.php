<?php

namespace App;

use App\Pharmacy;
use Illuminate\Database\Eloquent\Model;

class BusinessHour extends Model
{
    protected $guarded = [];

    protected $casts = [
        'monday' => 'array',
        'tuesday' => 'array',
        'wednesday' => 'array',
        'thursday' => 'array',
        'friday' => 'array',
        'saturday' => 'array',
        'sunday' => 'array',
        'exceptions' => 'array',
    ];

    public function pharmacy() 
    {
        return $this->belongsTo(Pharmacy::class);
    }
}
