<?php

namespace App\Http\Controllers;

use App\Drug;
use App\Jobs\ImportDrugs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class DrugController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Drug::class);
    }

    public function index()
    {
        $userPharmacies = Auth::user()->pharmacies()->pluck('id')->toArray();

        if (request()->ajax()) {
            $drugs = null;
            if (Auth::user()->hasPermissionTo('drug read all')) {
                $drugs = Drug::query()->latest();
            } else {
                $drugs = Drug::whereHas('pharmacies', function ($query) use ($userPharmacies) {
                    $query->whereIn('pharmacy_id', $userPharmacies);
                })->with('pharmacies')
                ->get();
            }

            return DataTables::of($drugs)
                ->addColumn('action', function ($drug) {
                   return view('lc-admin.drugs.datatable-action', compact('drug'));
                })->make(true);
        }

        return view('lc-admin.drugs.index');
    }

    public function create()
    {
        return view('lc-admin.drugs.create');
    }

    public function store(Request $request)
    {
        Drug::create($request->all());

        return redirect()->route('lc-admin.drugs.index');
    }

    public function edit(Drug $drug)
    {
        return view('lc-admin.drugs.edit')->with(
            'drug', $drug
        );
    }

    public function update(Drug $drug, Request $request)
    {
        $drug->update($request->all());

        return redirect()->route('lc-admin.drugs.index');
    }

    public function import(Request $request)
    {
        $file = Storage::putFileAs(
            'drugs-excel', $request->file, time() . '-' . $request->file->getClientOriginalName()
        );

        ImportDrugs::dispatch($file);

        return redirect()->route('lc-admin.drugs.index');
    }

}
