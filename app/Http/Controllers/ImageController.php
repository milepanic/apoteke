<?php

namespace App\Http\Controllers;

use App\Services\FileService;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    private $fileService;

    function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function store(Request $request, $path)
    {
        app('debugbar')->disable();

        if ($request->has('gallery')) {
            $gallery = $this->fileService->upload($request->gallery, "temp/{$path}/gallery");

            return response()->json(file_path($gallery[0]));
        }

        $file = $this->fileService
            ->upload($request->file, 'temp/' . $path);

        return response()
            ->json(['location' => file_path($file)]);
    }

    public function storeGallery(Request $request, $path, $dir)
    {
        app('debugbar')->disable();

        logger($request-all());

        $file = $this->fileService
            ->upload($request->file, 'temp/' . $path);

        return response()
            ->json(['location' => file_path($file)]);
    }
}
