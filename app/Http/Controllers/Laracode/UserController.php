<?php

namespace App\Http\Controllers\Laracode;

use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax())
            return DataTables::eloquent(
                User::query()->with(['roles', 'permissions']))
                    ->addColumn('action', function ($user) {
                           return view('lc-admin.users.datatable-action', compact('user'));
                       })
                    ->make(true);

        return view('lc-admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        $permissions = Permission::get();

        return view('lc-admin.users.create', compact('roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = User::create(
            $request->except(['roles', 'permissions'])
        );

        $roles = array_flatten(
            Role::whereIn('id', [$request->roles])
                ->get(['name'])
                ->toArray()
        );

        $permissions = array_flatten(
            Permission::whereIn('id', [$request->permissions])
                ->get(['name'])
                ->toArray()
        );

        activity()
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->withProperties([
                'attributes' => [
                    'roles' => implode(', ', $roles)
                ]
            ])
            ->log('Synced roles');

        activity()
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->withProperties([
                'attributes' => [
                    'permissions' => implode(', ', $permissions)
                ]
            ])
            ->log('Synced permissions');

        $user->syncRoles($request->roles);
        $user->syncPermissions($request->permissions);

        // switch ($request->action) {

        //     case 'save':
        //         return redirect()->route('lc-admin.users.edit', ['user' => $user->id]);

        //     case 'save-and-new':
        //         return redirect()->route('lc-admin.users.create');

        //     case 'save-and-back':
        //         return redirect()->route('lc-admin.users.index');
        // }
        return view('lc-admin.users.index');
    }

    public function edit(User $user)
    {
        $roles = Role::get();
        $permissions = Permission::get();

        return view('lc-admin.users.edit', compact('roles', 'permissions', 'user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // if the user is the owner of the group
        // if (request()->ajax() && ! $user->ownedPharmacies->isEmpty()) {
        //     return DataTables::eloquent($user->ownedPharmacies()->with('groups'))
        //         ->addColumn('action', function ($pharmacy) {
        //                return view('lc-admin.pharmacies.datatable-action', compact('pharmacy'));
        //            })
        //         ->make(true);
        // // if the user is the owner of pharmacies
        // } else if (request()->ajax() && ! $user->pharmacies->isEmpty()) {
        if (request()->ajax()) {
            return DataTables::eloquent($user->pharmacies()->with(['groups', 'location']))
                ->addColumn('action', function ($pharmacy) {
                   return view('lc-admin.pharmacies.datatable-action', compact('pharmacy'));
               })
                ->make(true);
        }
        // }

        return view('lc-admin.users.profile', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $roles = array_flatten(
            Role::whereIn('id', [$request->roles])
                ->get(['name'])
                ->toArray()
        );

        $permissions = array_flatten(
            Permission::whereIn('id', [$request->permissions])
                ->get(['name'])
                ->toArray()
        );

        // activity()
        //     ->performedOn($user)
        //     ->causedBy(Auth::user())
        //     ->withProperties([
        //         'attributes' => '{' . implode(', ', $roles) . '}',
        //         'old' => '{' . implode(', ', $user->roles->pluck('name')->toArray()) . '}'
        //     ])
        //     ->log('Synced roles');

        // activity()
        //     ->performedOn($user)
        //     ->causedBy(Auth::user())
        //     ->withProperties([
        //         'attributes' => '{' . implode(', ', $permissions) . '}',
        //         'old' => '{' . implode(', ', $user->permissions->pluck('name')->toArray()) . '}'
        //     ])
        //     ->log('Synced permissions');

        $user->syncRoles($request->roles);
        $user->syncPermissions($request->permissions);

        $user->update($request->except(['roles', 'permissions']));

        // switch ($request->action) {

        //     case 'save':
        //         return redirect()->route('lc-admin.users.edit', ['user' => $user->id]);

        //     case 'save-and-new':
        //         return redirect()->route('lc-admin.users.create');

        //     case 'save-and-back':
        //         return redirect()->route('lc-admin.users.index');
        // }
        return redirect()->route('lc-admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return 200;
    }
}
