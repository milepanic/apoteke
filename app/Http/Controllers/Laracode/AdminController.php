<?php

namespace App\Http\Controllers\Laracode;

use App\Http\Controllers\Controller;
use App\Laracode\Activity;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function index()
    {
        if (Auth::check())
            return redirect()->route('lc-admin.dashboard');

        return view('lc-admin.login');
    }

    public function dashboard()
    {
        return view('lc-admin.dashboard');
    }

    public function activityLog()
    {
        if (request()->ajax())
            return DataTables::eloquent(
                Activity::query()->with(['subject', 'causer']))
                    ->addColumn('attributes', function ($activity) {
                        return view('lc-admin.components.datatables.activity-log-child', compact('activity'))
                            ->render();
                    })
                    ->rawColumns(['attributes'])
                    ->make(true);

        return view('lc-admin.activity-log');  
    }

}
