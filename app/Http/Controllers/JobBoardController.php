<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\JobBoardRequest;
use App\JobBoard;
use App\Location;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class JobBoardController extends Controller
{
    /**
     * Creates new controller instance
     */
    function __construct()
    {
        $this->authorizeResource(JobBoard::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                JobBoard::query()
                    ->with(['user', 'group'])
                    ->latest()
                )->addColumn('action', function ($jobBoard) {
                   return view('lc-admin.job-board.datatable-action', compact('jobBoard'));
               })->make(true);
        }

        return view('lc-admin.job-board.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lc-admin.job-board.create')->with([
            'locations' => Location::all(),
            'groups' => Group::all(),
            'users' => User::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobBoardRequest $request)
    {
        if (Auth::user()->hasRole('super admin')) {
            JobBoard::create($request->all());
        } else {
            Auth::user()->jobBoards()
                ->create($request->except('user_id'));
        }

        return redirect()->route('lc-admin.job-board.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobBoard  $jobBoard
     * @return \Illuminate\Http\Response
     */
    public function show(JobBoard $jobBoard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobBoard  $jobBoard
     * @return \Illuminate\Http\Response
     */
    public function edit(JobBoard $jobBoard)
    {
        return view('lc-admin.job-board.edit')->with([
            'jobBoard' => $jobBoard,
            'users' => User::all(),
            'locations' => Location::all(),
            'groups' => Group::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobBoard  $jobBoard
     * @return \Illuminate\Http\Response
     */
    public function update(JobBoardRequest $request, JobBoard $jobBoard)
    {
        if (Auth::user()->hasRole('super admin')) {
            $jobBoard->update($request->all());
        } else {
            Auth::user()->jobBoards()
                ->update($request->except('user_id'));
        }

        return redirect()->route('lc-admin.job-board.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobBoard  $jobBoard
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobBoard $jobBoard)
    {
        $jobBoard->delete();

        return notify('success-solid-active', __('Uspešno je obrisan oglas za posao'));
    }
}
