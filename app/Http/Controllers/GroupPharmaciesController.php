<?php

namespace App\Http\Controllers;

use App\Group;
use App\Pharmacy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class GroupPharmaciesController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {        
        return DataTables::eloquent(
                $group->pharmacies()->with('location')
            )->addColumn('action', function ($pharmacy) use ($group) {
               return view('lc-admin.group-pharmacies.datatable-action', compact('pharmacy', 'group'));
            })
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('lc-admin.group-pharmacies.edit')->with([
            'group' => $group,
            'pharmacies' => Pharmacy::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $group->pharmacies()
            ->attach($request->pharmacies);

        return redirect()
            ->route('lc-admin.groups.edit', ['group' => $group]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pharmacy  $pharmacy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group, Request $request)
    {
        $group->pharmacies()
            ->detach($request->pharmacy);

        return notify('success-solid-active', 'Uspešno je izbačena apoteka');
    }
}
