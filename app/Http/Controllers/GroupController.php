<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\GroupRequest;
use App\Pharmacy;
use App\Services\FileService;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class GroupController extends Controller
{
    /**
     * @var \App\Services\FileService
     */
    private $fileService;

    /**
     * Creates new controller instance
     *
     * @param  \App\Services\FileService  $fileService
     */
    public function __construct(FileService $fileService)
    {
        $this->authorizeResource(Group::class);

        $this->fileService = $fileService;
    }

    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                Group::query()
                    ->with(['users', 'pharmacies'])
                    ->latest()
                )->addColumn('action', function ($group) {
                   return view('lc-admin.groups.datatable-action', compact('group'));
               })->make(true);
        }

        return view('lc-admin.groups.index');
    }

    public function create()
    {
        return view('lc-admin.groups.create')->with([
            'pharmacies' => Pharmacy::all(),
            'users' => User::all(),
        ]);
    }

    public function store(GroupRequest $request)
    {
        $fileName = $this->fileService->upload($request->logo, 'groups', $request->name);

        $group = Group::create(
            array_merge(
                $request->except(['pharmacies', 'users', 'logo']),
                ['logo' => $fileName]
            )
        );

        $group->pharmacies()
            ->sync($request->pharmacies);

        $group->users()
            ->sync($request->users);

        return redirect()->route('lc-admin.groups.index');
    }

    public function show(Group $group)
    {

    }

    public function edit(Group $group)
    {
        return view('lc-admin.groups.edit')->with([
            'group' => $group,
            'users' => User::all(),
        ]);
    }

    public function update(GroupRequest $request, Group $group)
    {
        $logoArray = [];

        if ($request->has('logo')) {
            $fileName = $this->fileService->uploadAndDelete($group->logo, $request->logo, 'groups', $request->name);
            $logoArray = ['logo' => $fileName];
        }

        $group->update(
            array_merge(
                $request->except(['users', 'logo']),
                $logoArray
            )
        );

        $group->users()
            ->sync($request->users);

        return redirect()->route('lc-admin.groups.index');
    }

    public function destroy(Group $group)
    {
        $group->delete();

        $this->fileService->delete($group->logo);

        return notify('success-solid-active', __('Uspešno je obrisana grupa'));
    }
}
