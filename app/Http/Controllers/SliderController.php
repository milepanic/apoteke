<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderRequest;
use App\Services\FileService;
use App\Slider;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SliderController extends Controller
{
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(Slider::query())
                ->addColumn('action', function ($slider) {
                   return view('lc-admin.sliders.datatable-action', compact('slider'));
               })
                ->make(true);
        }

        return view('lc-admin.sliders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("lc-admin.sliders.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SliderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {
        $types = [];

        foreach ($request->types as $type) {
            array_push($types, $type);
        }

        $buttons = [];

        for ($i = 0; $i < sizeof($request->button_names); $i++) {
            $button = new \stdClass;
            $button->name = $request->button_names[$i];
            $button->style = $request->button_styles[$i];
            $button->url = $request->button_urls[$i];

            array_push($buttons, $button);
        }

        $backgroundArray = [];
        if ($request->has('background')) {
            $fileName = $this->fileService->upload($request->background, 'sliders');
            $backgroundArray = ['background' => $fileName];
        }

        Slider::create(
            array_merge(
                $request->except(['background', 'button_names', 'button_styles', 'button_urls']),
                $backgroundArray,
                ['buttons' => json_encode($buttons)],
                ['types' => json_encode($types)]
            )
        );

        return redirect()->route('lc-admin.sliders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('lc-admin.sliders.edit')->with(
            'slider', $slider
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SliderRequest  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(SliderRequest $request, Slider $slider)
    {
        $types = [];

        foreach ($request->types as $type) {
            array_push($types, $type);
        }

        $buttons = [];

        for ($i = 0; $i < sizeof($request->button_names); $i++) {
            $button = new \stdClass;
            $button->name = $request->button_names[$i];
            $button->style = $request->button_styles[$i];
            $button->url = $request->button_urls[$i];

            array_push($buttons, $button);
        }

        $backgroundArray = [];

        if ($request->has('background')) {
            $fileName = $this->fileService->uploadAndDelete($slider->background, $request->background, 'sliders');
            $backgroundArray = ['background' => $fileName];
        }

        $slider->update(
            array_merge(
                $request->except(['background', 'button_names', 'button_styles', 'button_urls']),
                $backgroundArray,
                ['buttons' => json_encode($buttons)],
                ['types' => json_encode($types)],
            )
        );

        return redirect()->route('lc-admin.sliders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->delete();

        $this->fileService->delete($slider->background);

        return notify('success-solid-active', 'Uspešno je obrisan slajder');
    }
}
