<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\PharmacyRequest;
use App\Location;
use App\Pharmacy;
use App\Services\FileService;
use App\User;
use Yajra\DataTables\Facades\DataTables;

class PharmacyController extends Controller
{
    /**
     * [__construct description]
     */
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;

        $this->authorizeResource(Pharmacy::class);
    }

    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                Pharmacy::query()
                    ->with(['location', 'users'])
                    ->latest()
                )->addColumn('action', function ($pharmacy) {
                   return view('lc-admin.pharmacies.datatable-action', compact('pharmacy'));
               })->make(true);
        }

        return view('lc-admin.pharmacies.index');
    }

    public function create()
    {
        return view('lc-admin.pharmacies.create')->with([
            'groups' => Group::all(),
            'users' => User::all(),
            'locations' => Location::all(),
        ]);
    }

    public function store(PharmacyRequest $request)
    {
        $emails = null;
        foreach ($request->emails as $email) {
            $emails[] = $email;
        }

        $phone_numbers = null;
        foreach ($request->phone_numbers as $number) {
            $phone_numbers[] = $number;
        }

        $fax_numbers = null;
        foreach ($request->fax_numbers as $number) {
            $fax_numbers[] = $number;
        }

        $documents = null;
        if ($request->has('documents')) {
            $documents[] = $this->fileService->upload($request->documents, 'pharmacies/documents');
        }

        $cover_image = null;
        if ($request->has('cover_image')) {
            $cover_image = $this->fileService->upload($request->cover_image, 'pharmacies/cover_images');
        }

        $thumbnail = null;
        if ($request->has('thumbnail')) {
            $thumbnail = $this->fileService->upload($request->thumbnail, 'pharmacies/thumbnails');
        }

        $gallery = null;
        if (! empty($request->gallery)) {
            $gallery[] = $this->fileService->upload($request->gallery, 'pharmacies/gallery');
        }

        $pharmacy = Pharmacy::create(
            array_merge(
                $request->only([
                    'name', 'about_us', 'web_page', 'address', 'location_id', 
                    'longitude', 'latitude', 'admin_report',
                ]),
                [
                    'visible' => $request->has('visible'),
                    'emails' => $emails,
                    'phone_numbers' => $phone_numbers,
                    'fax_numbers' => $fax_numbers,
                    'documents' => $documents,
                    'cover_image' => $cover_image,
                    'thumbnail' => $thumbnail,
                    'gallery' => $gallery,
                ]
            )
        );

        $pharmacy->groups()
            ->sync($request->groups);

        $pharmacy->users()
            ->sync($request->users);

        $pharmacy->businessHours()
            ->create([
                // 'monday' => $request->monday,
                // 'tuesday' => $request->tuesday,
                // 'wednesday' => $request->wednesday,
                // 'thursday' => $request->thursday,
                // 'friday' => $request->friday,
                // 'saturday' => $request->saturday,
                'monday' => $request->work_week,
                'tuesday' => $request->work_week,
                'wednesday' => $request->work_week,
                'thursday' => $request->work_week,
                'friday' => $request->work_week,
                'saturday' => $request->saturday,
                'sunday' => $request->sunday,
                'exceptions' => $request->exceptions
            ]);

        return redirect()->route('lc-admin.pharmacies.index');
    }

    public function edit(Pharmacy $pharmacy)
    {
        return view('lc-admin.pharmacies.edit')->with([
            'pharmacy' => $pharmacy->load('businessHours'),
            'groups' => Group::all(),
            'users' => User::all(),
            'locations' => Location::all(),
        ]);
    }

    public function update(Pharmacy $pharmacy, PharmacyRequest $request)
    {
        $emails = null;
        foreach ($request->emails as $email) {
            $emails[] = $email;
        }

        $phone_numbers = null;
        foreach ($request->phone_numbers as $number) {
            $phone_numbers[] = $number;
        }

        $fax_numbers = null;
        foreach ($request->fax_numbers as $number) {
            $fax_numbers[] = $number;
        }

        $documents = null;
        if ($request->has('documents')) {
            $documents = [
                'documents' => $this->fileService
                    ->upload($request->documents, 'pharmacies/documents')
                ];
        }

        $cover_image = null;
        if ($request->has('cover_image')) {
            $cover_image = [
                'cover_image' => $this->fileService
                    ->uploadAndDelete($pharmacy->cover_image, $request->cover_image, 'pharmacies/cover_images')
            ];
        }

        $thumbnail = null;
        if ($request->has('thumbnail')) {
            $thumbnail = [
                'thumbnail' => $this->fileService
                    ->uploadAndDelete($pharmacy->thumbnail, $request->thumbnail, 'pharmacies/thumbnails')
                ];
        }

        $gallery = null;
        if (! empty($request->gallery)) {
            $gallery = [
                'gallery' => $this->fileService
                    ->upload($request->gallery, 'pharmacies/gallery')
                ];
        }

        $pharmacy->update(
            array_merge(
                $request->only([
                    'name', 'about_us', 'web_page', 'address', 'location_id', 
                    'longitude', 'latitude', 'admin_report',
                ]),
                [
                    'visible' => $request->has('visible'),
                    'emails' => $emails,
                    'phone_numbers' => $phone_numbers,
                    'fax_numbers' => $fax_numbers,
                ],
                $cover_image ? $cover_image : [],
                $thumbnail ? $thumbnail : [],
                $documents ? $documents : [],
                $gallery ? $gallery : []
            )
        );

        $pharmacy->groups()
            ->sync($request->groups);

        $pharmacy->users()
            ->sync($request->users);

        $pharmacy->businessHours
            ->update([
                'monday' => $request->work_week,
                'tuesday' => $request->work_week,
                'wednesday' => $request->work_week,
                'thursday' => $request->work_week,
                'friday' => $request->work_week,
                'saturday' => $request->saturday,
                'sunday' => $request->sunday,
                'exceptions' => $request->exceptions
            ]);

        return redirect()->route('lc-admin.pharmacies.index');
    }

    public function destroy(Pharmacy $pharmacy)
    {
        $pharmacy->delete();

        $pharmacy->businessHours->delete();

        return notify('success-solid-active', 'Uspešno je obrisana apoteka');
    }

}
