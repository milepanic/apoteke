<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pharmacy;

class PageController extends Controller
{
    //
    public function index()
    {
    	return view('index');
    }

    public function pharmacies()
    {
        $pharmacies = Pharmacy::paginate(6);
        return view('pharmacies', compact('pharmacies'));
    }

    public function pharmacy($slug)
    {
        $pharmacy = Pharmacy::where('slug', $slug)->firstOrFail();
        return view('pharmacy', compact('pharmacy'));
    }

    public function products()
    {
        return view('products');
    }

    public function blogs()
    {
        return view('blogs');
    }

    public function blogPost($slug)
    {

        return view('blog_post');
    }

    public function contact()
    {
    	return view('contact');
    }


    public function aboutUs()
    {
    	return view('about_us');
    }

    public function advertising()
    {
    	return view('advertising');
    }

    public function faq()
    {
        return view('faq');
    }

    public function privacy()
    {
        return view('privacy');
    }

}
