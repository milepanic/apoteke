<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Pharmacy;
use App\Services\FileService;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends Controller
{
    public function __construct(FileService $fileService)
    {
        $this->authorizeResource(Employee::class);

        $this->fileService = $fileService;
    }

    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                Employee::query()
                    ->with(['pharmacies', 'pharmacies.location'])
                    ->latest()
                )->addColumn('action', function ($employee) {
                   return view('lc-admin.employees.datatable-action', compact('employee'));
                })->make(true);
        }

        return view('lc-admin.employees.index');
    }

    public function create()
    {
        return view('lc-admin.employees.create')->with(
            'pharmacies', Pharmacy::with('location')->get()
        );
    }

    /**
     * @param  \App\Http\Requests\EmployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $fileName = $this->fileService->upload($request->image, 'employees');

        $employee = Employee::create(
            array_merge(
                $request->except(['pharmacies', 'image']),
                ['image' => $fileName]
            )
        );

        $employee->pharmacies()
            ->sync($request->pharmacies);

        return redirect()->route('lc-admin.employees.index');
    }

    public function show(Employee $employee)
    {

    }

    public function edit(Employee $employee)
    {
        return view('lc-admin.employees.edit')->with([
            'employee' => $employee,
            'pharmacies' => Pharmacy::all(),
        ]);
    }

    /**
     * @param  \App\Employee  $employee
     * @param  \App\Http\Requests\EmployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Employee $employee, EmployeeRequest $request)
    {
        $imageArray = [];

        if ($request->has('image')) {
            $fileName = $this->fileService->uploadAndDelete($employee->image, $request->image, 'employees');
            $imageArray = ['image' => $fileName];
        }

        $employee->update(
            array_merge(
                $request->except(['pharmacies', 'image']),
                $imageArray
            )
        );

        $employee->pharmacies()
            ->sync($request->pharmacies);

        return redirect()->route('lc-admin.employees.index');
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();

        $this->fileService->delete($employee->image);

        return notify('success-solid-active', 'Uspešno je obrisan zaposleni');
    }

}
