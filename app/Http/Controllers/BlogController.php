<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests\BlogRequest;
use App\Services\FileService;
use App\Services\ContentDOMService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class BlogController extends Controller
{
    /**
     * Service for geting content out of DOM
     * @var \App\Services\ContentDOMService
     */
    private $contentDOMService;

    private $fileService;

    /**
     * New controller instance
     * 
     * @param ContentDOMService $contentDOMService
     */
    public function __construct(
        ContentDOMService $contentDOMService,
        FileService $fileService
    )
    {
        $this->contentDOMService = $contentDOMService;
        $this->fileService = $fileService;

        $this->authorizeResource(Blog::class);
    }

    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Blog::query()
                        ->with('user')
                )
                ->addColumn('action', function ($blog) {
                   return view('lc-admin.blogs.datatable-action', compact('blog'));
               })
                ->make(true);
        }

        return view('lc-admin.blogs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("lc-admin.blogs.create")->with(
            'uniqueKey', uniqid()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $blog = Blog::create($request->except('unique_key'));

        $content = $this->contentDOMService->loadHTML($request->content);

        $imagePaths = $content->getImages();

        foreach ($imagePaths as $imagePath) {
            $this->fileService->move($imagePath, $blog->storagePath);
        }

        $content = $content->replaceImagePaths("temp/{$request->unique_key}", $blog->storagePath);

        $blog->update([
            'content' => $content->dom->saveHTML(),
            'images' => $content->getImages()
        ]);

        return redirect()->route('lc-admin.blogs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        $uniqueKey = uniqid();

        return view('lc-admin.blogs.edit')->with([
            'blog' => $blog,
            'uniqueKey' => $uniqueKey
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, Blog $blog)
    {
        $blog->update($request->only('content'));

        return redirect()->route('lc-admin.blogs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();

        return 200;
    }
}
