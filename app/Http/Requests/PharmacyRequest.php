<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PharmacyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'visible' => 'nullable|boolean',
                    'name' => 'required|string|unique:pharmacies',
                    'emails' => 'nullable|array',
                    'emails.*' => 'nullable|email',
                    'phone_numbers' => 'nullable|array',
                    'phone_numbers.*' => 'nullable|string',
                    'fax_numbers' => 'nullable|array',
                    'fax_numbers.*' => 'nullable|string',
                    'about_us' => 'nullable|string',
                    'web_page' => 'nullable|string',
                    'documents' => 'nullable|array',
                    'documents.*' => 'nullable|file',
                    'cover_image' => 'required|image',
                    'thumbnail' => 'required|image',
                    'galery' => 'nullable|array',
                    'galery.*' => 'nullable|image',
                    'address' => 'required|string',
                    'location_id' => 'required|integer',
                    'longitude' => 'required|string',
                    'latitude' => 'required|string',
                    'admin_report' => 'nullable|string',
                ];

            case 'PUT':
                return [
                    'name' => 'required|string|unique:pharmacies,name,' . $this->pharmacy->id,
                    'emails' => 'nullable|array',
                    'emails.*' => 'nullable|email',
                    'phone_numbers' => 'nullable|array',
                    'phone_numbers.*' => 'nullable|string',
                    'fax_numbers' => 'nullable|array',
                    'fax_numbers.*' => 'nullable|string',
                    'about_us' => 'nullable|string',
                    'web_page' => 'nullable|string',
                    'documents' => 'nullable|array',
                    'documents.*' => 'nullable|file',
                    'cover_image' => 'nullable|image',
                    'thumbnail' => 'nullable|image',
                    'galery' => 'nullable|array',
                    'galery.*' => 'nullable|image',
                    'address' => 'required|string',
                    'location_id' => 'required|integer',
                    'longitude' => 'required|string',
                    'latitude' => 'required|string',
                    'admin_report' => 'nullable|string',
                ];
        }
    }
}
