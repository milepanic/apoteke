<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'title' => 'required|string|min:5|max:200',
                    'content' =>  'required|string|min:10',
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'content' =>  'required|string|min:10',
                ];
        }
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    public function prepareForValidation()
    {
        $this->merge([
            'user_id' => auth()->id(),
            'title' => ucfirst($this->input('title')),
            'slug' => str_slug($this->input('title')),
            'content' => str_replace(['<script', '</script>'], '', $this->input('content'))
        ]);
    }
}
