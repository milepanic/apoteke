<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|string|max:255|unique:groups',
                    'logo' => 'required|image',
                    'description' => 'required|string|max:500',
                    'web_page' => 'nullable|url',
                    'facebook' => 'nullable|url',
                    'instagram' => 'nullable|url',
                    'google' => 'nullable|url',
                    'linkedin' => 'nullable|url',
                    'twitter' => 'nullable|url',
                    'youtube' => 'nullable|url',
                    'pinterest' => 'nullable|url',
                ];

            case 'PUT':
                return [
                    'name' => 'required|string|max:255|unique:groups,name,' . $this->group->id,
                    'logo' => 'nullable|image',
                    'description' => 'required|string|max:500',
                    'web_page' => 'nullable|url',
                    'facebook' => 'nullable|url',
                    'instagram' => 'nullable|url',
                    'google' => 'nullable|url',
                    'linkedin' => 'nullable|url',
                    'twitter' => 'nullable|url',
                    'youtube' => 'nullable|url',
                    'pinterest' => 'nullable|url',
                ];
        }
    }
}
