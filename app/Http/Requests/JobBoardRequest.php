<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobBoardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'position' => 'required|string',
            'executor_count' => 'required|integer',
            'location_id' => 'nullable|integer',
            'group_id' => 'required|integer',
        ];
    }
}
