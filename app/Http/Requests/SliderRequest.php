<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'published' => 'nullable|boolean',
            'types' => 'required|array',
            'priority' => 'required|integer',
            'background' => 'nullable|image',
            'name' => 'nullable|string',
            'text' => 'nullable|string',
            'url' => 'nullable|url',
            'style_name' => 'nullable|string',
            'style_value' => 'nullable|string',
        ];
    }
}
