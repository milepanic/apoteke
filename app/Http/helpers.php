<?php

if (! function_exists('notify')) {
	/**
	 * Helper for handling notifications
	 *
	 * @param $param1 string - type (string) or options (array)
	 * @param $param2 string - message (string) or settings (array)
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 *
	 * @see http://bootstrap-notify.remabledesigns.com/
	 */
	function notify($param1, $param2) {

		if (is_array($param1)) {
			$json = json_encode([
				$param1,
				$param2
			]);
		}
		// use short syntax
		else {
			$json = json_encode([
				['message' => $param2],
				['type' => $param1]
			]);
		}

		$notification = str_replace(['[', ']'], ['', ''], $json);

		// redirect back
		return request()->ajax()
			? response()->json($json)
			: back()->withNotification($notification);
	}
}

if (! function_exists('file_path')) {
    /**
     * TODO: da radi za sve fajlove
     * TODO: ako je slika da izbaci 404 sliku
     * TODO: dodati u config i env putanju do 404 slike
     *
     * @param  string $path
     * @return string
     */
	function file_path($path) {
		if ($path) {
			if (filter_var($path, FILTER_VALIDATE_URL)) { 
			  return $path;
			}
			return url(Storage::url($path));
		}

		return asset('theme/images/404.jpg');
	}
}
