<?php

namespace App;

use App\Group;
use App\JobBoard;
use App\Pharmacy;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use LogsActivity;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected static $logAttributes = ['first_name', 'last_name', 'contact', 'email'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function pharmacies()
    {
        return $this->belongsToMany(Pharmacy::class);
    }

    // Owner of the group - his pharmacies
    // public function ownedPharmacies()
    // {
    //     return $this->hasManyThrough(Pharmacy::class, Group::class);
    // }

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    public function jobBoards() 
    {
        return $this->hasMany(JobBoard::class);
    }

}
